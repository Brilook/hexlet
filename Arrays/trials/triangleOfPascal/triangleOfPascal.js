/*
https://ru.hexlet.io/challenges/js_arrays_pascals_triangle/instance

Треугольник Паскаля — бесконечная таблица биномиальных коэффициентов, имеющая треугольную форму.
В этом треугольнике на вершине и по бокам стоят единицы. Каждое число равно сумме двух расположенных над ним чисел.
Строки треугольника симметричны относительно вертикальной оси.

0:      1
1:     1 1
2:    1 2 1
3:   1 3 3 1
4:  1 4 6 4 1
solution.js
Напишите функцию generate, которая возвращает указанную строку треугольника паскаля в виде массива. Экспортируйте функцию по умолчанию.
*/
const addRow = (triangle) => {
    const prevRow = triangle[triangle.length - 1];
    const startEnd = 1;
    const newRow = [startEnd];
    for (let i = 0; i < prevRow.length - 1; i++) {
        const upLeft = prevRow[i];
        const upRight = prevRow[i + 1];
        newRow.push(upLeft + upRight);
    }
    newRow.push(startEnd);
    return newRow;
};

const generate = (column) => {
    const triangle = [
        [1],
        [1, 1],
    ];
    while (triangle.length <= column) {
        triangle.push(addRow(triangle));
    }
    return triangle[column];
};

generate(4);

// teacher`s solution

const factorial = (n) => {
    if (n === 0) {
      return 1;
    }
    return n * factorial(n - 1);
  };
  
  const generate1 = (n) => {
    const numbersCount = n + 1;
    const result = [];
    for (let i = 0; i < numbersCount; i += 1) {
      result[i] = factorial(n) / (factorial(i) * factorial(n - i));
    }
    return result;
  };

generate1(4);

  
  /* Alternative solution
  
  const generateNextRow = (previousRow) => {
    const nextRow = [];
  
    for (let i = 0; i <= previousRow.length; i += 1) {
      const first = previousRow[i - 1] || 0;
      const second = previousRow[i] || 0;
      nextRow[i] = first + second;
    }
  
    return nextRow;
  };
  
  const generate = (rowNumber) => {
    let currentRow = [1];
  
    for (let row = 0; row < rowNumber; row += 1) {
      currentRow = generateNextRow(currentRow);
    }
  
    return currentRow;
  };
  
  */
  