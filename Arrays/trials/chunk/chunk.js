/*
https://ru.hexlet.io/challenges/js_arrays_chunk/instance

Реализуйте и экспортируйте функцию по умолчанию, которая принимает на вход массив и число, которое задает размер чанка (куска).
Функция должна вернуть массив, состоящий из чанков указанной размерности.
*/

const chunk1_0 = (arr, lengthChunk) => {
    const result = [];
    let tmpChunk = [];
    for (let i = 0; i < arr.length;) {
        while (tmpChunk.length < lengthChunk && i < arr.length) {
            tmpChunk.push(arr[i]);
            i++;
        }
        result.push(tmpChunk);
        tmpChunk = [];
    }
    return result;
};

const chunk1_1 = (arr, lengthChunk) => {
    const result = [];
    let tmpChunk = [];
    for (const item of arr) {
        if (tmpChunk.length === lengthChunk) {
            result.push(tmpChunk);
            tmpChunk = [];
        }
        tmpChunk.push(item);
    }
    if (tmpChunk.length > 0) result.push(tmpChunk);
    return result;
};

const chunk = (arr, lengthChunk) => {
    const result = [];
    for (let i = 0; i < arr.length; i += lengthChunk) {
        result.push(arr.slice(i, i + lengthChunk));
    }
    return result;
};

// chunk(['a', 'b', 'c', 'd'], 2);
// [['a', 'b'], ['c', 'd']]

// chunk1(['0', null, 'c', undefined, '0', 0, null], 3);
// [['a', 'b', 'c'], ['d']]
chunk1([], 4);
// expect(result3).toEqual([]);