/*
https://ru.hexlet.io/challenges/js_arrays_matrix_mirroring/instance

Реализуйте и экспортируйте по умолчанию функцию getMirrorMatrix, которая принимает двумерный массив (матрицу) и возвращает массив,
изменённый таким образом, что правая половина матрицы становится зеркальной копией левой половины,
симметричной относительно вертикальной оси матрицы.
Для простоты условимся, что матрица всегда имеет чётное количество столбцов и количество столбцов всегда равно количеству строк.
Постарайтесь решить данное испытание без использования встроенных методов массива.
Ограничение не касается метода push, который добавляет элементы в массив.
*/

const getMirrorMatrix = (matrix) => {
    const mirrorMatrix = [];
    for (const row of matrix) {
        const newRow = [];
        const size = row.length;
        for (let i = 0; i < size / 2; i++) {
            newRow[i] = row[i];
            newRow[size - 1 - i] = newRow[i];
        }
        mirrorMatrix.push(newRow);
    }
    return mirrorMatrix;
};

// teacher`s solution

const getMirrorArray = (array) => {
    const size = array.length;
    const mirrored = [];
  
    for (let i = 0; i < size / 2; i += 1) {
      mirrored[i] = array[i];
      mirrored[size - i - 1] = array[i];
    }
  
    return mirrored;
  };
  
  const getMirrorMatrix1 = (matrix) => {
    const mirroredMatrix = [];
  
    for (const row of matrix) {
      const mirroredRow = getMirrorArray(row);
      mirroredMatrix.push(mirroredRow);
    }
  
    return mirroredMatrix;
  };



getMirrorMatrix([
    [11, 12, 13, 24, 14],
    [21, 22, 23, 24],
    [31, 32, 33, 34],
    [41, 42, 43, 44],
  ]);
  
  //  [
  //     [11, 12, 12, 11],
  //     [21, 22, 22, 21],
  //     [31, 32, 32, 31],
  //     [41, 42, 42, 41],
  //  ]