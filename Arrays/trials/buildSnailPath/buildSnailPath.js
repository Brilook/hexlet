/**
https://ru.hexlet.io/challenges/js_arrays_snail/instance

Реализуйте и экспортируйте по умолчанию функцию buildSnailPath,
которая принимает на вход матрицу и возвращает список элементов матрицы 
по порядку следования от левого верхнего элемента по часовой стрелке к внутреннему.
 */

function rotateLeft(matrix) {
    const result = [];
    for (let i = 0; i <= matrix.length - 1; i += 1) {
      let iter = 0;
      for (let j = matrix[i].length - 1; j >= 0; j -= 1) {
        if (!result[iter]) {
          result[iter] = [];
        }
        result[iter].push(matrix[i][j]);
        iter += 1;
      }
    }
    return result;
  }
  
  const buildSnailPath = (matrix) => {
    let res = [];
    if (matrix.length !== 0) {
      const [firstLine, ...rest] = matrix;
      res = [firstLine, buildSnailPath(rotateLeft(rest))].flat();
    }
    return res;
  };

// rotateLeft([
//     [1, 2, 3, 4],
//     [5, 6, 7, 8],
//     [9, 10, 11, 12],
// ])
// rotateLeft([[5, 6, 7, 8],
//            [9, 10, 11, 12],] )

// buildSnailPath2([
//     [1, 2],
//     [3, 4],
//   ]); // [1, 2, 4, 3]

buildSnailPath2([
    [1, 2, 3, 4],
    [5, 6, 7, 8],
    [9, 10, 11, 12],
]) // [1, 2, 3, 4, 8, 12, 11, 10, 9, 5, 6, 7] 

// buildSnailPath1([
//     [1, 2, 3, 4],
//     [5, 6, 7, 8],
//     [9, 10, 11, 12],
//     [13, 14, 15, 16],
//     [17, 18, 19, 20]
// ]) // [1, 2, 3, 4, 8, 12, 16, 20, 19, 18, 17, 13, 9, 5, 6, 7, 11, 15, 14, 10] 

// buildSnailPath([
//     [1, 2, 3, 4, 5, 6, 7, 8],
//     [9, 10, 11, 12, 13, 14, 15, 16],
//     [17, 18, 19, 20]
// ]) // [1, 2, 3, 4, 5, 6, 7, 8, 16, 20, 19, 18, 17, 9,10,11,12,13,14,15] 

// buildSnailPath([
//     [1],
//     [2],
//     [3],
//     [4]
// ]) //[1, 2, 3, 4]);