/*
https://ru.hexlet.io/challenges/js_arrays_hamming_weight/instance

Вес Хэмминга — это количество единиц в двоичном представлении числа.

solution.js
Реализуйте и экспортируйте по умолчанию функцию hammingWeight, которая считает вес Хэмминга.
*/
const hammingWeight = (num) => {
  const binNum = num.toString(2).split('');
  return binNum.filter(item => item === '1').length;
};

const hammingWeight1 = (num) => {
  let weight = 0;
  const digits = num.toString(2);

  for (let i = 0; i < digits.length; i += 1) {
    weight += Number(digits[i]);
  }

  return weight;
};

// without conversion to string
const hammingWeight2 = (num) => {
  let weight = 0;
  let tmpNum = num;
  while (tmpNum) {
    weight += tmpNum % 2;
    tmpNum = Math.floor(tmpNum / 2);
  }
  return weight;
};
// hammingWeight(0); // 0
// hammingWeight(4); // 1
// hammingWeight2(101); // 4
hammingWeight2(12345); // 6