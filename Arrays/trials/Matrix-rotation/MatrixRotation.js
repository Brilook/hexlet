/*
https://ru.hexlet.io/challenges/js_arrays_matrix_rotation/instance

Реализуйте и экспортируйте функции rotateLeft и rotateRight, которые поворачивают матрицу влево (против часовой стрелки)
и соответственно вправо (по часовой стрелке).

Матрица реализована на массивах.
Функции должны возвращать новую матрицу не изменяя исходную.
Примеры:
const matrix = [
  [1, 2, 3, 4],
  [5, 6, 7, 8],
  [9, 0, 1, 2],
];

rotateLeft(matrix);
// [
//   [4, 8, 2],
//   [3, 7, 1],
//   [2, 6, 0],
//   [1, 5, 9],
// ]

rotateRight(matrix);
// [
//   [9, 5, 1],
//   [0, 6, 2],
//   [1, 7, 3],
//   [2, 8, 4],
// ]
*/

function rotateLeft(matrix) {
    const result = [];
    for (let i = 0; i <= matrix.length - 1; i += 1) {
        let iter = 0;
        for (let j = matrix[i].length - 1; j >= 0; j -= 1) {
            if (!result[iter]) {
                result[iter] = [];
            }
            result[iter].push(matrix[i][j]);
            iter += 1;
        }
    }
    return result;
};

function rotateRight(matrix) {
    const result = [];
    for (let i = matrix.length - 1; i >= 0; i -= 1) {
        for (let j = 0; j < matrix[i].length; j += 1) {
            if (!result[j]) {
                result[j] = [];
            }
            result[j].push(matrix[i][j]);
        }
    }
    return result;
};


// teacher`s solution

const rotate = (matrix, direction) => {
    const rowsCount = matrix.length;
    const [firstRow] = matrix;
    const columnsCount = firstRow.length;
    const rotated = [];
  
    for (let row = 0; row < columnsCount; row += 1) {
      rotated[row] = [];
      for (let column = 0; column < rowsCount; column += 1) {
        rotated[row][column] = direction === 'left'
          ? matrix[column][columnsCount - row - 1]
          : matrix[rowsCount - column - 1][row];
      }
    }
  
    return rotated;
  };
  
  export const rotateLeft = (matrix) => rotate(matrix, 'left');
  
  export const rotateRight = (matrix) => rotate(matrix, 'right');

rotateLeft([
    [1, 2, 3, 4],
    [5, 6, 7, 8],
    [9, 0, 1, 2],
]);
rotateRight([
    [1, 2, 3, 4],
    [5, 6, 7, 8],
    [9, 0, 1, 2],
]);