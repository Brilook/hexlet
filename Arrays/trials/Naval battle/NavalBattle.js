'use strict';
/*
https://ru.hexlet.io/challenges/js_arrays_sea_battle/instance

Реализуйте и экспортируйте функцию calcShipsCount,
которая принимает на вход поле боя в виде квадратного двумерного массива из нулей и единиц.
Ноль — пустая ячейка, единица — часть корабля. Функция должна вернуть количество кораблей на поле боя.

Так как корабли не должны соприкасаться друг с другом,
реализуйте и экспортируйте функцию isValidField, которая проверяет расстановку кораблей на корректность.

Примеры
calcShipsCount([]); // 0
calcShipsCount([
  [0, 1, 0, 0, 0, 0],
  [0, 1, 0, 1, 1, 1],
  [0, 0, 0, 0, 0, 0],
  [0, 1, 1, 1, 0, 1],
  [0, 0, 0, 0, 0, 1],
  [1, 1, 0, 1, 0, 0],
]); // 6
isValidField([
  [0, 1, 0, 0],
  [1, 0, 0, 1],
  [0, 0, 0, 0],
  [0, 1, 1, 1],
]); // false
*/
const isValidField = (matrix) => {
    let result = true;
    loop1:
        for (let i = 0; i < matrix.length; i++) {
            for (let j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] === 1) {
                    if (matrix[i - 1] &&
                        matrix[i - 1][j - 1] === 1 ||
                        matrix[i - 1] &&
                        matrix[i - 1][j + 1] === 1) {
                        result = false;
                        break loop1;
                    }
                    if (matrix[i + 1] &&
                        matrix[i + 1][j - 1] === 1 ||
                        matrix[i + 1] &&
                        matrix[i + 1][j + 1] === 1) {
                        result = false;
                        break loop1;
                    }
                }
            }
        }
    return result;
};


const calcShipsCount = (matrix) => {
    let numberOfShips = 0;
    const length = matrix.length;
    if (length > 0 && isValidField(matrix)) {
        for (let i = 0; i < length; i++) {
            for (let j = 0; j < length; j++) {
                const nextHorizontal = (j < length - 1) ? matrix[i][j + 1] : 0;
                const nextVertical = (i < length - 1) ? matrix[i + 1][j] : 0;
                if (matrix[i][j] === 1 && nextVertical === 0 && nextHorizontal === 0) {
                    numberOfShips++;
                }
            }
        }
    }
    return numberOfShips;
};

// Yars solution

const calcShipsCountY = (matrix) => {
  let count = 0;
  for (let i = 0; i < matrix.length; i++) {
    console.log(matrix[i - 1]);
    const lineUp = matrix[i - 1] ?? []; // при undefined[i] не выбросит исключение а вернет undefined
    if (!matrix[i].includes(1)) continue;
    for (let j = 0; j < matrix[i].length; j++) {
      if (matrix[i][j] === 1) {
        if (!lineUp[j] && !matrix[i][j - 1]) {
          count++;
        }
      }
    }
  }
  return count;
};
const isValidFieldY = (matrix) => {
  let result = true;
  loop1: for (let i = 0; i < matrix.length; i++) {
    const lineUp = matrix[i - 1] ?? [];
    const lineDown = matrix[i + 1] ?? [];
    for (let j = 0; j < matrix[i].length - 1; j++) {
      if (matrix[i][j]) {
        if (lineUp[j + 1] || lineDown[j + 1]) {
          result = false;
          break loop1;
        }
      }
    }
  }
  return result;
};


// teacher`s solution

const calcShipsCountT = (battleField) => {
    let shipsCount = 0;
    const fieldSize = battleField.length;
    for (let row = 0; row < fieldSize; row += 1) {
      for (let col = 0; col < fieldSize; col += 1) {
        if (battleField[row][col] && !battleField[row][col - 1]) {
          if (!battleField[row - 1] || !battleField[row - 1][col]) {
            shipsCount += 1;
          }
        }
      }
    }
  
    return shipsCount;
  };
  
  const isValidFieldT = (battleField) => {
    for (let col = 0; col < battleField.length; col += 1) {
      for (let row = 1; row < battleField.length; row += 1) {
        if (battleField[row][col]) {
          if (battleField[row - 1][col + 1] || battleField[row - 1][col - 1]) {
            return false;
          }
        }
      }
    }
    return true;
  };
  
  // export { calcShipsCount, isValidField };


// calcShipsCount([])

calcShipsCountY([
    [0, 1, 0, 0, 0, 0],
    [0, 1, 0, 1, 1, 1],
    [0, 0, 0, 0, 0, 0],
    [0, 1, 1, 1, 0, 1],
    [0, 0, 0, 0, 0, 1],
    [1, 1, 0, 1, 0, 0],
]); // 6

// isValidField([
//     [0, 1, 0, 0],
//     [1, 0, 0, 1],
//     [0, 0, 0, 0],
//     [0, 1, 1, 1],
// ]); // false