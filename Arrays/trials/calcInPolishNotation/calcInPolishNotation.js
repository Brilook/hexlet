/*
https://ru.hexlet.io/challenges/js_arrays_reverse_polish_notation/instance

В данном упражнении необходимо реализовать стековую машину, то есть алгоритм, проводящий вычисления по обратной польской записи.

Обратная польская нотация или постфиксная нотация — форма записи математических и логических выражений,
в которой операнды расположены перед знаками операций. Выражение читается слева направо. Когда в выражении встречается знак операции,
выполняется соответствующая операция над двумя ближайшими операндами, находящимися слева от знака операции.
Результат операции заменяет в выражении последовательность её операндов и знак, после чего выражение вычисляется дальше по тому же правилу.
Таким образом, результатом вычисления всего выражения становится результат последней вычисленной операции.

Например, выражение (1 + 2) * 4 + 3 в постфиксной нотации будет выглядеть так: 1 2 + 4 * 3 +, а результат вычисления: 15.
Другой пример - выражение: 7 - 2 * 3, в постфиксной нотации: 7 2 3 * -, результат: 1.

solution.js
Реализуйте функцию calcInPolishNotation, которая принимает массив, каждый элемент которого содержит число или знак операции (+, -, *, /).
Функция должна вернуть результат вычисления по обратной польской записи. Экспортируйте функцию по умолчанию.
*/
const calcInPolishNotation = (arr) => {
    const operationSymbol = ['+', '-', '*', '/'];
    const stack = [];

    for (const char of arr) {
        if (!operationSymbol.includes(char)) {
            stack.push(char);
        } else {
            const secondOperand = stack.pop();
            const firstOperand = stack.pop();
            let result = 0;
            switch (char) {
                case '+':
                    result = firstOperand + secondOperand;
                    break;
                case '-':
                    result = firstOperand - secondOperand;
                    break;
                case '*':
                    result = firstOperand * secondOperand;
                    break;
                case '/':
                    result = firstOperand / secondOperand;
                    break;
                default:
                    console.error('Check the correctness of the transmitted data!');
            }
            stack.push(result);
        }
    }
    return stack.pop();
};

const calcInPolishNotation1 = (arr) => {
    const stack = [];
    const superSwich = {
        '+': (x, y) => x + y,
        '-': (x, y) => x - y,
        '*': (x, y) => x * y,
        '/': (x, y) => x / y,
    };
    for (let char of arr) {
        if (Object.keys(superSwich).includes(char)) {
            const secondOperand = stack.pop();
            const firstOperand = stack.pop();
            char = superSwich[char](firstOperand, secondOperand);
        }
        stack.push(char);
    }
    return stack.pop();
};




calcInPolishNotation1([1, 2, '+', 4, '*', 3, '+']); // 15
// calcInPolishNotation1([7, 2, 3, '*', '-']); // 1