/*
https://ru.hexlet.io/challenges/js_arrays_summary_ranges/instance

Реализуйте и экспортируйте по умолчанию функцию summaryRanges,
которая находит в массиве непрерывные возрастающие последовательности чисел и возвращает массив с их перечислением.

Примеры
summaryRanges([]);
// []

summaryRanges([1]);
// []

summaryRanges([1, 2, 3]);
// ['1->3']

summaryRanges([0, 1, 2, 4, 5, 7]);
// ['0->2', '4->5']

summaryRanges([110, 111, 112, 111, -5, -4, -2, -3, -4, -5]);
// ['110->112', '-5->-4']
*/

const summaryRanges = (arr) => {
    const rangeses = [];
    let tmpRange = [arr[0]];
    
    for (let i = 1; i <= arr.length; i++) {
        if (arr[i] - arr[i - 1] === 1) {
            tmpRange[1] = (arr[i]);
        } else if (tmpRange[1]) {
            rangeses.push(tmpRange);
            tmpRange = [arr[i]];
        } else {
            tmpRange = [arr[i]];
        }
    }

    return rangeses.map(item => item.join('->'));
};

// teacher`s solution

const getRangeOfSequence = (sequence) => {
    const first = sequence[0];
    const last = sequence[sequence.length - 1];
    return `${first}->${last}`;
};

const summaryRanges2 = (numbers) => {
    const ranges = [];
    let sequence = [];

    for (let index = 0; index < numbers.length; index += 1) {
        const current = numbers[index];
        const next = numbers[index + 1];
        sequence.push(current);
        if (current + 1 !== next) {
            if (sequence.length > 1) {
                const range = getRangeOfSequence(sequence);
                ranges.push(range);
            }
            sequence = [];
        }
    }

    return ranges;
};


// summaryRanges([0, 1, 2, 4, 5, 7]);
// summaryRanges([1, 1, 3, 4, 5, -6, 8, 9, 10, 12, 14, 14]);
// summaryRanges([1, 2, 3]);
summaryRanges([110, 111, 112, 111, -5, -4, -2, -3, -4, -5]);