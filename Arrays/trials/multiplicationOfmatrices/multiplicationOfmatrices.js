/*
https://ru.hexlet.io/challenges/js_arrays_matrix_multiplication/instance

Операция умножения двух матриц А и В представляет собой вычисление результирующей матрицы С,
где каждый элемент C(ij) равен сумме произведений элементов в соответствующей строке первой матрицы A(ik) и элементов в столбце второй матрицы B(kj).

Две матрицы можно перемножать только в том случае, если количество столбцов в первой матрице совпадает с количеством строк во второй матрице.
Это значит, что первая матрица обязательно должна быть согласованной со второй матрицей.
В результате операции умножения матрицы размера M×N на матрицу размером N×K является матрица размером M×K.

matrix.js
Реализуйте и экспортируйте по умолчанию функцию, которая принимает две матрицы и возвращает новую матрицу — результат их произведения.

Примеры
import multiply from './matrix.js';

const matrixA = [[1, 2], [3, 2]];
const matrixB = [[3, 2], [1, 1]];

multiply(matrixA, matrixB);
// [[5, 4], [11, 8]]

const matrixC = [
  [2, 5],
  [6, 7],
  [1, 8],
];
const matrixD = [
  [1, 2, 1],
  [0, 1, 0],
];

multiply(matrixC, matrixD);
// [
//   [2, 9, 2],
//   [6, 19, 6],
//   [1, 10, 1],
// ]
*/
// const matrixA = [
//     [1, 2],
//     [3, 4]
// ];
// const matrixB = [
//     [5, 6],
//     [7, 8]
// ];

// const matrixB = [
//     [4, 2],
//     [3, 1],
//     [1, 5],
// ];
// const matrixA = [
//     [1, 2, 2],
//     [3, 1, 1],
// ];

// const matrixC = [
//     [2, 5],
//     [6, 7],
//     [1, 8],
// ];
// const matrixD = [
//     [1, 2, 1],
//     [0, 1, 0],
// ];


// test 3
// const matrixA = [
//   [1, 2, 1],
//   [0, 1, 0],
//   [2, 3, 4],
// ];
// const matrixB = [
//   [2, 5],
//   [6, 7],
//   [1, 8],
// ];
// test 3
//   const expected = [
//     [15, 27],
//     [6, 7],
//     [26, 63],
//   ];

// display test "0" 
const matrixA = [
  [0, 0, 0],
  [0, 1, 0],
  [2, 3, 4],
];
const matrixB = [
  [0, 5],
  [0, 7],
  [0, 8],
];

const multiplicationOfmatrices = (matrix1, matrix2) => {
  const columnCountM1 = matrix1.length;
  const result = [];
  for (let j = 0; j < columnCountM1; j++) {
    for (let i = 0; i < columnCountM1; i++) {
      let multiSum = 0;
      for (let k = 0; k < matrix2.length; k++) {
        multiSum += matrix1[j][k] * matrix2[k][i];
      }
      if (!result[j]) result[j] = [];
      if (multiSum || multiSum === 0) result[j].push(multiSum);
    }
  }
  return result;
};

// teacher`s solution 
const multiplicationOfmatrices1 = (matrixA, matrixB) => {
  const rowsCountA = matrixA.length;
  const rowsCountB = matrixB.length;
  const [firstRowB] = matrixB;
  const columnsCountB = firstRowB.length;
  const matrixC = [];

  for (let row = 0; row < rowsCountA; row += 1) {
    matrixC[row] = [];
    for (let column = 0; column < columnsCountB; column += 1) {
      let sum = 0;
      for (let i = 0; i < rowsCountB; i += 1) {
        sum += matrixA[row][i] * matrixB[i][column];
      }
      matrixC[row][column] = sum;
    }
  }
  return matrixC;
};

multiplicationOfmatrices(matrixA, matrixB);