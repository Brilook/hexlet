/*
https://ru.hexlet.io/challenges/js_arrays_ascending_sequence/instance

Реализуйте и экспортируйте по умолчанию функцию isContinuousSequence, которая проверяет,
является ли переданная последовательность целых чисел возрастающей непрерывно (не имеющей пропусков чисел).
Например, последовательность [4, 5, 6, 7] — непрерывная, а [0, 1, 3] — нет.
Последовательность может начинаться с любого числа, главное условие — отсутствие пропусков чисел.
Последовательность из одного числа не может считаться возрастающей.
*/

const isContinuousSequence = (sequence) => {
    let result = false;
    const size = sequence.length;
    if (size > 1) {
        result = true;
        for (let i = 0; i < size - 1; i++) {
            if (sequence[i + 1] - sequence[i] !== 1) {
                result = false;
                break;
            }
        }
    }
    return result;
};

// teacher`s solution

const isContinuousSequence1 = (coll) => {
    const size = coll.length;
    if (size <= 1) {
      return false;
    }
  
    const start = coll[0];
    for (let index = 1; index < size; index += 1) {
      if (start + index !== coll[index]) {
        return false;
      }
    }
  
    return true;
  };
  



isContinuousSequence([10, 11, 12, 13]);     // true
isContinuousSequence([-5, -4, -3]);         // true

isContinuousSequence([10, 11, 12, 14, 15]); // false
isContinuousSequence([1, 2, 2, 3]);         // false
isContinuousSequence([7]);                  // false
isContinuousSequence([]);                   // false