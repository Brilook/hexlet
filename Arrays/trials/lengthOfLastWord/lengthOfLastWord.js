/*
https://ru.hexlet.io/challenges/js_arrays_length_of_last/instance

Реализуйте и экспортируйте по умолчанию функцию lengthOfLastWord, которая возвращает длину последнего слова переданной на вход строки.
Словом считается любая последовательность, не содержащая пробелов.
*/
const lengthOfLastWord = (str) => {
  const strArr = str.trim().split(' ');
  return strArr[strArr.length - 1].length;
};

const lengthOfLastWord1 = (str) => {
  const end = str.length - 1;
  let lastWord = '';
  for (let i = end; i > 0; i--) {
    if (str[i] !== ' ') {
      // как лучше?
      // while (str[i] !== ' ' && i >= 0) { // так?
      while (str[i] && str[i] !== ' ') { // или так?
        lastWord += str[i];
        i--;
      }
      break;
    }
  }
  return lastWord.length
};

const lengthOfLastWord2 = (str) => {
  const end = str.length - 1;
  let indexStartWord = 0;
  let lengthLastWord = 0;
  for (let i = end; i >= 0; i--) {
    if (str[i] !== ' ' && indexStartWord === 0) {
      indexStartWord = i;
    }
    if (i < indexStartWord && str[i] === ' ') {
      lengthLastWord = indexStartWord - i;
      break;
    }
    if (i === 0) {
      lengthLastWord = indexStartWord + 1;
    }
  }
  return lengthLastWord;
};

// lengthOfLastWord1(''); // 0
// lengthOfLastWord1('man in BlacK'); // 5
// lengthOfLastWord1('hello, ld!  '); // 3
// lengthOfLastWord1('ld!'); // 3
lengthOfLastWord1(undefined); // 3
// lengthOfLastWord1('hello, world! dafsdfdgfdfgdfgdfgdfgdfgdfgdfgdfgdf '); // 35 