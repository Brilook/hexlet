/*
https://ru.hexlet.io/challenges/js_arrays_compare_versions/instance

Реализуйте и экспортируйте по умолчанию функцию compareVersion, которая сравнивает переданные версии version1 и version2.
Если version1 > version2, то функция должна вернуть 1,
если version1 < version2, то - -1,
если же version1 = version2, то - 0.

Версия - это строка, в которой два числа (мажорная и минорные версии) разделены точкой, например: 12.11.
Важно понимать, что версия - это не число с плавающей точкой, а несколько чисел не связанных между собой.
Проверка на больше/меньше производится сравнением каждого числа независимо. Поэтому версия 0.12 больше версии 0.2.

Пример порядка версий:

0.1 < 1.1 < 1.2 < 1.11 < 13.37
*/

const compareVersion = (version1, version2) => {
    const [majorV1, minorV1] = version1.split('.');
    const [majorV2, minorV2] = version2.split('.');
    let result = 0;
    if (majorV1 > majorV2) {
        result = 1;
    } else if (majorV1 === majorV2) {
        if (minorV1 - minorV2 > 0) {
            result = 1;
        } else if (minorV1 - minorV2 < 0) {
            result = -1;
        }
    } else {
        result = -1;
    }
    return result;
};

// teachers`s solution

const compareVersion1 = (first, second) => {
    const [a1, b1] = first.split('.');
    const [a2, b2] = second.split('.');
  
    const major = Math.sign(a1 - a2);
    const minor = Math.sign(b1 - b2);
  
    return major === 0 ? minor : major;
  };



// compareVersion("0.1", "0.2"); // -1
// compareVersion("0.2", "0.1"); // 1
// compareVersion("4.2", "4.2"); // 0
// compareVersion('0.2', '0.12'); // -1