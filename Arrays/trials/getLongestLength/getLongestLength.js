/*
https://ru.hexlet.io/challenges/js_arrays_longest_substring/instance

Реализуйте функцию getLongestLength принимающую на вход строку и возвращающую длину максимальной последовательности из неповторяющихся символов.
Подстрока может состоять из одного символа.
Например в строке qweqrty, можно выделить следующие подстроки: qwe, weqrty. Самой длинной будет weqrty.
Экспортируйте функцию по умолчанию.
*/

const getLongestLength1 = (str) => {
  let sequence = new Set();
  let maxLength = 0;
  for (const char of str) {
    if (!sequence.has(char)) {
      sequence.add(char);
    } else {
      maxLength = sequence.size > maxLength ? sequence.size : maxLength;
      let tmp = [...sequence];
      tmp.splice(0, str.indexOf(char) + 1, char);
      sequence = new Set(tmp);
    }
  }
  return Math.max(maxLength, sequence.size);
};

// teacher`s solution

const getLongestLength2 = (str) => {
  let sequence = [];
  let maxLength = 0;

  // Проходимся по всем символам переданной строки.
  for (const char of str) {
    // Ищем в сформированной последовательности
    // позицию первого вхождения текущего символа.
    const index = sequence.indexOf(char);
    // Добавляем в последовательность текущий символ.
    sequence.push(char);
    if (index !== -1) {
      // Если символ в последовательности был найден,
      // значит в неё был добавлен повторяющийся символ.
      // Отсекаем из все символы включая найденный.
      sequence = sequence.slice(index + 1);
    }
    // Получаем длину последовательности.
    const sequenceLength = sequence.length;
    if (sequenceLength > maxLength) {
      // Если длина последовательности больше чем максимальная,
      // устанавливаем новую максимальную длину.
      maxLength = sequenceLength;
    }
  }

  return maxLength;
};


// getLongestLength1('cdeef'); // 3
// getLongestLength1('abcdeef'); // 5
getLongestLength1('jabjcdel'); // 7
getLongestLength1('1234561qweqwer'); // 9
getLongestLength1('jabjcdel'); // 7