/*
https://ru.hexlet.io/challenges/js_arrays_sum_intervals/instance

Реализуйте и экспортируйте по умолчанию функцию sumIntervals,
которая принимает на вход массив интервалов и возвращает сумму всех длин интервалов.
В данной задаче используются только интервалы целых чисел от -100 до 100 , которые представлены в виде массива.
Первое значение интервала всегда будет меньше, чем второе значение. Например, длина интервала [-100, 0] равна 100,
а длина интервала [5, 5] равна 0. Пересекающиеся интервалы должны учитываться только один раз.

Примеры

sumIntervals([
   [5, 5]
]); // 0

sumIntervals([
   [-100, 0]
]); // 100

sumIntervals([
   [1, 2],
   [11, 12]
]); // 2

sumIntervals([
   [2, 7],
   [6, 6]
]); // 5

sumIntervals([
   [1, 9],
   [7, 12],
   [3, 4]
]); // 11

sumIntervals([
   [1, 5],
   [-30, 19],
   [1, 7],
   [16, 19],
   [5, 100]
]); // 130
*/
const sumIntervals3 = (matrix) => {
  let result = 0;
  let [
    [min, max], ...rest
  ] = matrix;
  for (const item of rest) {
    const [start, end] = item;
    if (min < end && max > start) {
      if (min > start) {
        min = start;
      }
      if (max < end) {
        max = end;
      }
    } else {
      result = sumIntervals(rest);
    }
  }
  result += (max - min);
  return result;
};

const sumIntervals1 = (matrix) => {
  let [[min, max], ...rest] = matrix;
  let result = 0;
  for (const item of rest) {
    const [start, end] = item;
    if (min < end && max > start) {
      if (min > start) {
        min = start;
      }
      if (max < end) {
        max = end;
      }
    } else {
      result = max - min;
      [[min, max], ...rest] = rest;
    }
  }
  result += (max - min);
  return result;
};


// teacher`s solution. cool solution! I like it!

const sumIntervals = (intervals) => {
  const values = [];
  for (const [start, end] of intervals) {
    for (let i = start; i < end; i += 1) {
      if (!values.includes(i)) {
        values.push(i);
      }
    }
  }
  return values.length;
};


// upgrade teacher`s solution
const sumIntervals2 = (intervals) => {
  const values = new Set();
  for (const [start, end] of intervals) {
    for (let i = start; i < end; i += 1) {
      values.add(i);
    }
  }
  return values.size;
};
//  sumIntervals1([
//    [7, 10],
//    [-1, 4],
//    [2, 5],
//  ]); // 9

//  sumIntervals1([
//    [-4, 4],
//    [1, 3],
//  ]); // 8

// sumIntervals1([
//   [1, 5],
//   [-30, 19],
//   [1, 7],
//   [16, 19],
//   [5, 100]
// ]); // 130

// sumIntervals1([
//    [1, 2],
//    [11, 12],
// ]); // 2

sumIntervals1([
  [7, 10],
  [-1, 4],
  [2, 5],
]) // 9