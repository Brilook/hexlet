/*
https://ru.hexlet.io/courses/js-arrays/lessons/rest-operator/exercise_unit

Реализуйте и экспортируйте функцию getMax(), которая ищет в массиве максимальное значение и возвращает его.

Примеры
import { getMax } from '../arrays.js';

// Для пустого массив возвращается null
getMax([]); // null
getMax([1, 10, 8]); // 10

Эта функция реализуется просто и мы уже делали подобное ранее. Сейчас же мы учимся использовать rest-оператор. Используйте его вместе с деструктуризацией, для извлечения первого элемента и всех остальных.
Первый элемент становится начальным значением максимального, а остальные перебираются в цикле для определения максимального.
*/

const getMax = (arr) => {
    let [max = null, ...rest] = arr;
    for (const item of rest) {
        if (max < item) {
            max = item;
        }
    }
    return max;
};

// teacher`s sulution (I'm cooler!)))
const getMax = (coll) => {
    if (coll.length === 0) {
        return null;
    }

    let [max, ...rest] = coll;
    for (const value of rest) {
        if (value > max) {
            max = value;
        }
    }

    return max;
};

getMax([1, 10, 8]); // 10