/*
https://ru.hexlet.io/courses/js-arrays/lessons/build-strings/exercise_unit

Реализуйте функцию buildDefinitionList, которая генерирует HTML список определений (теги <dl>, <dt> и <dd>) и возвращает получившуюся строку. При отсутствии элементов в массиве функция возвращает пустую строку. Экспортируйте функцию по умолчанию.

Параметры функции
Список определений следующего формата:

 const definitions = [
  ['definition1', 'description1'],
  ['definition2', 'description2']
];
То есть каждый элемент входного списка сам является массивом, содержащим два элемента: термин и его определение.

Пример использования
const definitions = [
  ['Блямба', 'Выпуклость, утолщения на поверхности чего-либо'],
  ['Бобр', 'Животное из отряда грызунов'],
];

buildDefinitionList(definitions);
// '<dl><dt>Блямба</dt><dd>Выпуклость, утолщение на поверхности чего-либо</dd><dt>Бобр</dt><dd>Животное из отряда грызунов</dd></dl>';
*/

const definitions = [
    ['Блямба', 'Выпуклость, утолщения на поверхности чего-либо'],
    ['Бобр', 'Животное из отряда грызунов'],
];

const buildDefinitionList = (definitions) => {
    const result = [];
    if (definitions.length > 0) {
        const resultStr = '<dl>';
        result.push(resultStr);
        for (const item of definitions) {
            const term = `<dt>${item[0]}</dt>`;
            result.push(term);
            const def = `<dd>${item[1]}</dd>`;
            result.push(def);
        }
        const resultEnd = '</dl>';
        result.push(resultEnd);
    }
    return result.join('\n');
};

// teacher`s solution

const buildDefinitionList = (definitions) => {
    if (definitions.length === 0) {
        return '';
    }

    const parts = [];

    for (const definition of definitions) {
        const name = definition[0];
        const description = definition[1];
        parts.push(`<dt>${name}</dt><dd>${description}</dd>`);
    }

    const innerValue = parts.join('');
    const result = `<dl>${innerValue}</dl>`;

    return result;
};

export default buildDefinitionList;

buildDefinitionList1(definitions)