/*
https://ru.hexlet.io/courses/js-arrays/lessons/strings/exercise_unit

Реализуйте и экспортируйте по умолчанию функцию makeCensored, которая заменяет каждое вхождение указанных слов в предложении
на последовательность $#%! и возвращает полученную строку.

Аргументы:
Текст
Набор стоп слов
Словом считается любая непрерывная последовательность символов, включая любые спецсимволы (без пробелов).

Примеры
import makeCensored from '../strings';

const sentence = 'When you play the game of thrones, you win or you die';
const result = makeCensored(sentence, ['die', 'play']);
// When you $#%! the game of thrones, you win or you $#%!

const sentence2 = 'chicken chicken? chicken! chicken';
const result2 = makeCensored(sentence2, ['?', 'chicken']);
// '$#%! chicken? chicken! $#%!';
*/

const sentence = 'play the die';
const stopWords = ['die', 'play'];

const makeCensored = (sentence, stopWords) => {
    const sentenceArr = sentence.split(' ');
    const sentenceArrLength = sentenceArr.length;
    for (let i = 0; i < sentenceArrLength; i++) {
        for (const stopWord of stopWords) {
            if (sentenceArr[i] === stopWord) {
                sentenceArr[i] = '$#%!';
            }
        }
    }
    return sentenceArr.join(' ');
};
// It doesn't work! Why?
const makeCensored1 = (sentence, stopWords) => {
    const sentenceArr = sentence.split(' ');
    const result = [];
    for (let word of sentenceArr) {
        if (stopWords.includes(word)) {
            result.push('$#%!');
        } else {
            result.push(word);
        }
    }
    return result.join(' ');
};

const makeCensored3 = (sentence, stopWords) => {
    const stopSet = new Set(stopWords);
    const sentenceArr = sentence.split(' ');
    const result = [];
    for (let word of sentenceArr) {
        if (stopSet.has(word)) {
            result.push('$#%!');
        } else {
            result.push(word);
        }
    }
    return result.join(' ');
};


// teacher`s solution;
const makeCensored2 = (sentence, stopWords) => {
    const sentenceArr = sentence.split(' ');
    const result = [];
    for (let word of sentenceArr) {
        const censoredWord = stopWords.includes(word) ? '$#%!' : word;
        result.push(censoredWord);
    }
    return result.join(' ');
};


makeCensored3(sentence, stopWords);