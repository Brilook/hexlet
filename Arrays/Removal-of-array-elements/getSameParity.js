/*
https://ru.hexlet.io/courses/js-arrays/lessons/removing/exercise_unit

Реализуйте функцию getSameParity, которая принимает на вход массив чисел и возвращает новый, состоящий из элементов,
у которых такая же чётность, как и у первого элемента входного массива. Экспортируйте функцию по умолчанию.

Примеры
getSameParity([]);        // []
getSameParity([1, 2, 3]); // [1, 3]
getSameParity([1, 2, 8]); // [1]
getSameParity([2, 2, 8]); // [2, 2, 8]
Подсказки
Проверка чётности - остаток от деления: item % 2 === 0 — чётное число.
Если на вход функции передан пустой массив, то она должна вернуть пустой массив.
Для работы с отрицательными числами может понадобиться функция нахождения модуля Math.abs
*/

const getSameParity = (arr) => {
    let result = [];
    if (arr.length > 0) {
        const remainder = Math.abs(arr[0] % 2);
        for (const item of arr) {
            if (Math.abs(item % 2) === remainder) {
                result.push(item);
            }
        }
    }
    return result;
};

getSameParity([1, -3]);