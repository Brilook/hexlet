/*
https://ru.hexlet.io/courses/js-arrays/lessons/isset/exercise_unit

Реализуйте и экспортируйте по умолчанию функцию get, которая извлекает из массива элемент по указанному индексу,
если индекс существует, либо возвращает значение по умолчанию. Функция принимает на вход три аргумента:

Массив
Индекс
Значение по умолчанию (равно null)
Примеры
const cities = ['moscow', 'london', 'berlin', 'porto'];

get(cities, 1); // 'london'
get(cities, 4); // null
get(cities, 10, 'paris'); // 'paris'
*/
const cities = ['moscow', '', 'berlin', 'porto'];

const get = (arr, index, defaultValue = null) => {
    let res;
    if (arr[index] === '') {
        res = '';
    } else {
        res = arr[index] || defaultValue;
    }
    return res;
};

const get1 = (arr, index, defaultValue = null) => {
    return arr[index] === '' ? '' : arr[index] || defaultValue;
}

const get2 = (arr, index, defaultValue = null) => {
    return arr[index] !== undefined ? arr[index] : defaultValue;
}


get2(cities, 4);