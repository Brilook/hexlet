/*
https://ru.hexlet.io/courses/js-arrays/lessons/stack/exercise_unit

Реализуйте и экспортируйте функцию по умолчанию, которая принимает на вход строку,
состоящую только из открывающих и закрывающих скобок разных типов, и проверяет является ли эта строка сбалансированной.
Пустая строка (отсутствие скобок) считается сбалансированной.

// Пример вложенности
isBracketStructureBalanced('[()]');  // true
isBracketStructureBalanced('{<>}}'); // false
Функция должна поддерживать, минимум, четыре вида скобок: круглые — (), квадратные — [], фигурные — {} и угловые — <>.
*/

const isBracketStructureBalanced = (str) => {
    const stack = [];

    let balance = true;
    for (const bracket of str) {
        const lastCharOfStack = stack[stack.length - 1];
        if (bracket === '(' ||
            bracket === '[' ||
            bracket === '{' ||
            bracket === '<') {
            stack.push(bracket);
        } else if (lastCharOfStack === '(' && bracket === ')') {
            stack.pop();

        } else if (lastCharOfStack === '{' && bracket === '}') {
            stack.pop();

        } else if (lastCharOfStack === '[' && bracket === ']') {
            stack.pop();

        } else if (lastCharOfStack === '<' && bracket === '>') {
            stack.pop();
        } else {
            balance = false;
        }
    }
    return balance && stack.length === 0;
};



const isBracketStructureBalanced1 = (str) => {
    let balance = true;
    const brackets = str.split('');
    const openingBracket = ['(', '[', '{', '<'];
    const closingBracket = [')', ']', '}', '>'];
    const stack = [];

    for (const bracket of brackets) {
        const lastCharOfStack = stack[stack.length - 1];
        const indexLastCharInOpeningBracket = openingBracket.indexOf(lastCharOfStack);

        if (openingBracket.includes(bracket)) {
            stack.push(bracket);
        } else if (closingBracket.indexOf(bracket) === indexLastCharInOpeningBracket) {
            stack.pop();
        } else {
            balance = false;
        }
    }
    return balance && stack.length === 0;
};

// teacher`s solution

const openingSymbols = ['(', '[', '{', '<'];
const closingSymbols = [')', ']', '}', '>'];

const isOpeningSymbol = (symbol) => openingSymbols.includes(symbol);
const getClosingSymbolFor = (symbol) => closingSymbols[openingSymbols.indexOf(symbol)];

const isBracketStructureBalanced2 = (str) => {
  const stack = [];
  for (const symbol of str) {
    if (isOpeningSymbol(symbol)) {
      const closingSymbol = getClosingSymbolFor(symbol);
      stack.push(closingSymbol);
    } else {
      const lastSavedSymbol = stack.pop();
      if (symbol !== lastSavedSymbol) {
        return false;
      }
    }
  }

  return stack.length === 0;
};

// isBracketStructureBalanced('[()]'); // true
// isBracketStructureBalanced1('{<>}}'); // false
// isBracketStructureBalanced1('(('); // false
isBracketStructureBalanced('(<><<{[()]}>>>)'); // false