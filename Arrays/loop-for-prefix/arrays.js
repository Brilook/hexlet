/*
https://ru.hexlet.io/courses/js-arrays/lessons/for/exercise_unit

Реализуйте и экспортируйте по умолчанию функцию, которая принимает на вход массив и строковой префикс,
и возвращает новый массив, в котором к каждому элементу исходного массива добавляется переданный префикс.
Функция предназначена для работы со строковыми элементами. После префикса автоматически добавляется пробел.

import addPrefix from './arrays.js';

const names = ['john', 'smith', 'karl'];

const newNames = addPrefix(names, 'Mr');
console.log(newNames);
// => ['Mr john', 'Mr smith', 'Mr karl'];

console.log(names); // Старый массив не меняется!
// => ['john', 'smith', 'karl'];
*/
const names = ['john', 'smith', 'karl'];

const addPrefix = (arr, prefix) => {
    const lastIndex = arr.length - 1;
    const prefixArr = [];
    for (let i = 0; i <= lastIndex; i++) {
        const newItem = `${prefix} ${arr[i]}`;
        prefixArr.push(newItem);
    }
    return prefixArr;
};

const addPrefix1 = (arr, prefix) => {
    const lastIndex = arr.length;
    const prefixArr = [];
    for (let i = 0; i < lastIndex; i++) {
        prefixArr[i] = `${prefix} ${arr[i]}`;
    }
    return prefixArr;
};

addPrefix1(names, 'Mr')