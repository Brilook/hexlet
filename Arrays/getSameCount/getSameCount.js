/*
https://ru.hexlet.io/courses/js-arrays/lessons/nested-loops/exercise_unit

Реализуйте и экспортируйте по умолчанию функцию getSameCount() принимающую на вход два массива и возвращающую количество общих уникальных значений в обоих массивах.

// Общие повторяющиеся элементы: 1, 3, 2
getSameCount([1, 3, 2, 2], [3, 1, 1, 2, 5]); // 3

// Общие повторяющиеся элементы: 4
getSameCount([1, 4, 4], [4, 8, 4]); // 1

// Общие повторяющиеся элементы: 1, 10
getSameCount([1, 10, 3], [10, 100, 35, 1]); // 2

// Нет элементов
getSameCount([], []); // 0

Подсказки
Для получения массива без повторяющихся элементов, используйте uniq из библиотеки lodash.
В целях обучения и прокачки, решите это упражнение с помощью вложенных циклов.
*/
const unique = (arr) => {
    const result = [];
    for (const item of arr) {
        if (!result.includes(item)) {
            result.push(item);
        }
    }
    return result;
};

const getSameCount = (arr1, arr2) => {
    let count = 0;
    const uniqArr1 = unique(arr1);
    const uniqArr2 = unique(arr2);
    for (const item of uniqArr1) {
        for (const item2 of uniqArr2) {
            if (item === item2) {
                count++;
            }
        }
    }
    return count;
};

const getSameCount1 = (arr1, arr2) => {
    const set1 = new Set(arr1);
    const set2 = new Set(arr2);
    let resCount = 0;
    set1.forEach(value => {
        if (set2.has(value)) {
            resCount++;
        }
    })
    return resCount;
};

getSameCount1([1, 10, 3], [10, 100, 35, 1]); // 2
getSameCount1([1, 3, 2, 2], [3, 1, 1, 2, 5]); // 3