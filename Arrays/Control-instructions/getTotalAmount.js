/*
https://ru.hexlet.io/courses/js-arrays/lessons/control-statements/exercise_unit

Реализуйте и экспортируйте по умолчанию функцию, которая принимает на вход в виде массива кошелёк с деньгами и название валюты
и возвращает сумму денег указанной валюты.

Реализуйте данную функцию используя управляющие инструкции.

Параметры функции:
Массив, содержащий купюры разных валют с различными номиналами
Наименование валюты

Примеры
const money1 = [
  'eur 10', 'usd 1', 'usd 10', 'rub 50', 'usd 5',
];
getTotalAmount(money1, 'usd') // 16

const money2 = [
  'eur 10', 'usd 1', 'eur 5', 'rub 100', 'eur 20', 'eur 100', 'rub 200',
];
getTotalAmount(money2, 'eur') // 135

const money3 = [
  'eur 10', 'rub 50', 'eur 5', 'rub 10', 'rub 10', 'eur 100', 'rub 200',
];
getTotalAmount(money3, 'rub') // 270
*/

const getTotalAmount = (wallet, currency) => {
  let sum = 0;
  for (const item of wallet) {
    if (item.slice(0, 3) === currency) {
      sum += Number(item.slice(4));
    }
  }
  return sum;
};

const getTotalAmount1 = (wallet, currency) => {
  let sum = 0;
  for (const item of wallet) {
    if (item.slice(0, 3) !== currency) {
      continue;
    }
    sum += Number(item.slice(4));
  }
  return sum;
};

const money1 = [
  'eur 10', 'usd 1', 'usd 10', 'rub 50', 'usd 5',
];
getTotalAmount1(money1, 'usd') // 16