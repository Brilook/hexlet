/*
https://ru.hexlet.io/courses/js-arrays/lessons/references/exercise_unit

Реализуйте и экспортируйте функцию reverse(), которая принимает на вход массив и располагает элементы внутри него в обратном порядке.

import { reverse } from './arrays';

const names = ['john', 'smith', 'karl'];

reverse(names);
console.log(names); // => ['karl', 'smith', 'john']

reverse(names);
console.log(names); // => ['john', 'smith', 'karl']
*/

const reverse = (arr) => {
    const reverseArr = [];
    const length = arr.length;
    for (let i = length - 1; i >= 0; i--) {
        reverseArr.push(arr[i])
    }
    return reverseArr;
}

// array mutation
const reverse1 = (arr) => {
    const lastIndx = arr.length - 1;
    const halfLength = arr.length / 2;

    for (let i = 0; i < halfLength; i++) {
        const mirrorIndex = lastIndx - i;
        const tmp = arr[i];
        arr[i] = arr[mirrorIndex];
        arr[mirrorIndex] = tmp;
    }
    return arr;
}
reverse1([1, 2, 3, 4, 5, 6, 7])