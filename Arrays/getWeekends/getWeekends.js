/*
https://ru.hexlet.io/courses/js-arrays/lessons/syntax/exercise_unit


Реализуйте и экспортируйте функцию getWeekends(), которая возвращает массив из двух элементов – названий выходных дней на английском. Функция принимает на вход параметр – формат возврата. Всего есть два возможных значения:

'long' (по умолчанию) – массив содержит значения saturday и sunday
'short' – массив содержит значения sat и sun

import { getWeekends } from '../dates';

// Вывод не показан, так как это равносильно ответу
getWeekends();
// Тоже самое что и вызов выше
getWeekends('long');
getWeekends('short');
*/

const getWeekends = (param) => {
    const listWeekends = [
        ['saturday', 'sunday'],
        ['sat', 'sun']
    ];
    return param === 'long' || !param ? listWeekends[0] : listWeekends[1];

}

getWeekends()