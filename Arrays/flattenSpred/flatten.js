/*
https://ru.hexlet.io/courses/js-arrays/lessons/spread-operator/exercise_unit

Реализуйте и экспортируйте функцию flatten().
Эта функция принимает на вход массив и выпрямляет его: если элементами массива являются массивы,
то flatten сводит всё к одному массиву, раскрывая каждый вложенный.

js эта функция реализована как метод flat() у массивов. Его использовать нельзя.

import { flatten } from '../arrays.js';

// Для пустого массив возвращается []
flatten([]); // []
flatten([1, [3, 2], 9]); // [1, 3, 2, 9]
flatten([1, [[2], [3]], [9]]); // [1, [2], [3], 9]
Реализуйте добавление элементов вложенного массива в результирующий через spread-оператор.
*/
const flatten = (arr) => {
    let flatArr = [];
    for (const item of arr) {
        if (Array.isArray(item)) {
            flatArr = [...flatArr, ...item];
            console.log('flatArr: ', flatArr);
        } else {
            flatArr.push(item);
            console.log('flatArr: ', flatArr);
        }
    }
    return flatArr;
};

// flatten([1, [3, 2], 9]); // [1, 3, 2, 9]
// flatten([1, [[2], [3]], [9]]);


// making it harder. Adding recursion until the array is completely flat!
const flatten1 = (arr) => {
    let flatArr = [];
    for (const item of arr) {
        if (Array.isArray(item)) {
            flatArr = [...flatArr, ...item];
            flatArr = flatten1(flatArr);
        } else {
            flatArr.push(item);
        }
    }
    return flatArr;
};
flatten1([1, [[2], [3]], [9]]);

