/*
https://ru.hexlet.io/courses/js-arrays/lessons/modification/exercise_unit

Реализуйте и экспортируйте функцию swap(), которая меняет местами первый и последний элемент массива.
Если массив содержит меньше двух элементов, то он возвращается как есть.

import { swap } from '../arrays';

swap([]); // []
swap([1]); // [1]
swap([1, 2]); // [2, 1]
swap(['one', 'two', 'three']); // ['three', 'two', 'one']
*/

const swap = (arr) => {
    const lastItem = arr.length - 1;
    if (lastItem > 0) {
        const tmp = arr[0];
        arr[0] = arr[lastItem];
        arr[lastItem] = tmp;
    }
    return arr;
};

swap(['one', 'two', 'three'])