/*
https://ru.hexlet.io/courses/js-arrays/lessons/destructuring/exercise_unit

Реализуйте и экспортируйте функцию getTheNearestLocation(), которая находит место ближайшее к указанной точке на карте и возвращает его.

Параметры функции:
locations – массив мест на карте.
Каждое место это массив из двух элементов, где первый элемент это название места, второй – точка на карте (массив из двух чисел x и y).

point – текущая точка на карте. Массив из двух элементов-координат x и y.

const locations = [
  ['Park', [10, 5]],
  ['Sea', [1, 3]],
  ['Museum', [8, 4]],
];

const currentPoint = [5, 5];

// Если точек нет, то возвращается null
getTheNearestLocation([], currentPoint); // null

getTheNearestLocation(locations, currentPoint); // ['Museum', [8, 4]]
Для решения этой задачи деструктуризация не нужна, но мы хотим её потренировать. Поэтому решите эту задачу без обращения к индексам массивов.

Подсказки
Расстояние между точками высчитывается с помощью функции getDistance.
*/
const locations = [
    ['Park', [10, 5]],
    ['Sea', [1, 3]],
    ['Museum', [8, 4]],
];
const currentPoint = [5, 5];


const getTheNearestLocation = (locations, currentPoint) => {
    const distances = [];
    for (const point of locations) {
        const [, coords] = point;
        distances.push(getDistance(currentPoint, coords));
    }
    const minDist = Math.min.apply(null, distances);
    const indexMinDist = distances.indexOf(minDist);

    return locations[indexMinDist] || null;
};


const getDistance = (arr1, arr2) => {
    const [x1, y1] = arr1;
    const [x2, y2] = arr2;
    const dist = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
    return dist;
};

// getTheNearestLocation(locations, currentPoint);

// teacher`s solution

const getDistance1 = ([x1, y1], [x2, y2]) => {
    const xs = x2 - x1;
    const ys = y2 - y1;

    return Math.sqrt(xs ** 2 + ys ** 2);
};


const getTheNearestLocation1 = (locations, currentPoint) => {
    if (locations.length === 0) {
        return null;
    }

    let [nearestLocation] = locations;
    const [, nearestPoint] = nearestLocation;
    let lowestDistance = getDistance1(currentPoint, nearestPoint);

    for (const location of locations) {
        const [, point] = location;
        const distance = getDistance1(currentPoint, point);
        if (distance < lowestDistance) {
            lowestDistance = distance;
            nearestLocation = location;
        }
    }

    return nearestLocation;
};

getTheNearestLocation1(locations, currentPoint);