/*
https://ru.hexlet.io/courses/js-arrays/lessons/set-theory/exercise_unit

Реализуйте и экспортируйте по умолчанию функцию countUniqChars, которая получает на вход строку и считает,
сколько символов (без учёта повторяющихся символов) использовано в этой строке.

Например, в строке yy используется всего один символ — y. А в строке 111yya! — используется четыре символа: 1, y, a и !.

const text1 = 'yyab'; // y, a, b
countUniqChars(text1); // 3

const text2 = 'You know nothing Jon Snow';
countUniqChars(text2); // 13

// Если передана пустая строка, то функция должна вернуть `0`
const text3 = '';
countUniqChars(text3); // 0
*/

const countUniqChars = (str) => {
    let uniqueStr = '';
    for (const char of str) {
        if (!uniqueStr.includes(char)) {
            uniqueStr += char;
        }
    }
    return uniqueStr.length;
};

countUniqChars('You know nothing Jon Snow')