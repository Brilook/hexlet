/*
https://ru.hexlet.io/courses/js-arrays/lessons/sorting/exercise_unit

Реализуйте и экспортируйте по умолчанию функцию bubbleSort, которая сортирует массив используя пузырьковую сортировку
*/

const bubbleSort = (arr) => {
    let stepsCount = arr.length - 1;
    let swapped;
    do {
        swapped = false;
        for (let i = 0; i < stepsCount; i++) {
            if (arr[i] > arr[i + 1]) {
                const tmp = arr[i];
                arr[i] = arr[i + 1];
                arr[i + 1] = tmp;
                swapped = true;
            }
        }
        stepsCount--;
    } while (swapped);
    console.log('arr: ', arr);
    return arr;
};

bubbleSort([3, 10, 4, 1, 1, 4, 12, 34, 13, 1, 1, 165, -2, 0, -7, 3])