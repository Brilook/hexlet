import ReactDOM from 'react-dom';
import React from 'react';
import Card from './Card';

const mountNode = document.getElementById('container');
ReactDOM.render(<Card />, mountNode);
