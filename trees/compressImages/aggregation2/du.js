/*
https://ru.hexlet.io/courses/js-trees/lessons/calculate/exercise_unit

Реализуйте и экспортируйте по умолчанию функцию,
которая принимает на вход директорию, а возвращает список узлов вложенных,
(директорий и файлов) в указанную директорию на один уровень, и место которое, они занимают.
Размер файла задается в метаданных. Размер директории складывается из сумм всех размеров файлов, находящихся внутри во всех поддиректориях.
Сами директории размера не имеют.

Примечания
Обратите внимание на структуру результирующего массива. Каждый элемент — массив с двумя значениями: именем директории и размером файлов внутри.
Результат отсортирован по размеру в обратном порядке. То есть сверху самые тяжёлые, внизу самые лёгкие.
*/

import { isFile, getName, getChildren, getMeta } from '@hexlet/immutable-fs-trees';

const getMemorysSum = (node) => {
    if (isFile(node)) return getMeta(node).size;
    const descendantMemCount = getChildren(node).map(getMemorysSum);
    return descendantMemCount.reduce((a, b) => a + b, 0) || 0;
};

 const du = (tree) => {
    const result = getChildren(tree).map((node) => [getName(node), getMemorysSum(node)]);
    return result.sort((a, b) => b[1] - a[1]);
};

export default du;