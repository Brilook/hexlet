// theory
/*
функция, которая принимает на вход директорию и
возвращает список директорий первого уровня вложенности и
количество файлов внутри каждой из них, включая все поддиректории
*/
import { isFile, getChildren, getName, isDirectory } from '@hexlet/immutable-fs-trees';

const getFilesCount = (node) => {
    if (isFile(node)) return 1;
    const descendantCounts = getChildren(node).map(getFilesCount);
    return descendantCounts.reduce((a, b) => a + b, 0) || 0;
}

const getSubdirectoriesInfo = (tree) => {
    const children = getChildren(tree);
    const result = children
        .filter(isDirectory)
        .map((child) => [getName(child), getFilesCount(child)]);
    return result;
}

export default getSubdirectoriesInfo;
