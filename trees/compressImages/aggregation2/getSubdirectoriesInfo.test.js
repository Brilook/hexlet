import { mkdir, mkfile } from '@hexlet/immutable-fs-trees';
import getSubdirectoriesInfo from './getSubdirectoriesInfo.js'

const tree = mkdir('/', [
    mkdir('etc', [
        mkdir('apache'),
        mkdir('nginx', [
            mkfile('nginx.conf'),
        ]),
    ]),
    mkdir('consul', [
        mkfile('config.json'),
        mkfile('file.tmp'),
        mkdir('data'),
    ]),
    mkfile('hosts'),
    mkfile('resolve'),
]);

test('getSubdirectoriesInfov', () => {
    expect(getSubdirectoriesInfo(tree)).toEqual([ ['etc', 1], ['consul', 2]]);
});
