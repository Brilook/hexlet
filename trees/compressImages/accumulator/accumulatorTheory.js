// найдём все пустые директории в нашей файловой системе.

import { mkdir, mkfile, getName, getChildren, isDirectory} from '@hexlet/immutable-fs-trees';

const tree = mkdir('/', [
    mkdir('etc', [
      mkdir('apache'),
      mkdir('nginx', [
        mkfile('nginx.conf'),
      ]),
      mkdir('consul', [
        mkfile('config.json'),
        mkdir('data'),
      ]),
    ]),
    mkdir('logs'),
    mkfile('hosts'),
  ]);

const depth = 2;

  const findEmptyDirPaths1 = (tree) => {
      const iter = (node, depth) => {
          const children = getChildren(node);
          if (children.length === 0) return getName(node);
          if (depth === 2) return [];

          return children.filter(isDirectory).flatMap((child) => iter(child, depth + 1));
      }
      return iter(tree, 0);
  };

  const findEmptyDirPaths2 = (node, depth = Infinity) => {
        const children = getChildren(node);
        if (children.length === 0) return getName(node);
        if (depth === 0) return [];

        return children.filter(isDirectory).flatMap((child) => findEmptyDirPaths2(child, depth - 1));
    }


const findEmptyDirPathsDfs1 = (tree, callback, depth = Infinity) => {
  const childrenDir = depth === 0 ? [] : getChildren(tree).filter(isDirectory);
  return childrenDir.reduce((acc, child) => {
    acc.push(callback(child) ? getName(child) : findEmptyDirPathsDfs(child, callback, depth - 1));
    return acc;
  }, []).flat();
}




const findEmptyDirPaths = (tree, depth = Infinity) => {
  const result = [];

  const fillResult = (node, depth) => {
    if (getChildren(node).length === 0 && depth >= 0) result.push(getName(node));
  };
  
  const dfs = (node, callback, depth) => {
    callback(node, depth);
    getChildren(node).filter(isDirectory).forEach(child => dfs(child, callback, depth - 1));
  };

  dfs(tree, fillResult, depth);

  return result;
}

console.log(findEmptyDirPaths(tree, depth));


// findEmptyDirPaths(tree); // ['apache', 'logs']
// findEmptyDirPaths1(tree, 2); // ['apache', 'logs']