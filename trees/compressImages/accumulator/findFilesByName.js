/*
https://ru.hexlet.io/courses/js-trees/lessons/accumulator/exercise_unit

Реализуйте и экспортируйте по умолчанию функцию,
которая принимает на вход файловое дерево и подстроку,
а возвращает список файлов, имена которых содержат эту подстроку. Функция должна вернуть полные пути до файлов.
*/
import {
    mkdir,
    mkfile,
    isFile,
    getName,
    getChildren,
    isDirectory, 
} from '@hexlet/immutable-fs-trees';

import path from 'path';


const tree = mkdir('/', [
    mkdir('etc', [
        mkdir('apache'),
        mkdir('nginx', [
            mkfile('nginx.conf', {
                size: 800
            }),
        ]),
        mkdir('consul', [
            mkfile('config.json', {
                size: 1200
            }),
            mkfile('data', {
                size: 8200
            }),
            mkfile('raft', {
                size: 80
            }),
        ]),
    ]),
    mkfile('hosts', {
        size: 3500
    }),
    mkfile('resolve', {
        size: 1000
    }),
]);

const findFilesByName = (tree, str) => { //Time: 1.063 s
    const ancestry = getName(tree);
    const iter = (node, newAncestry) => {
        const name = getName(node);
        if (isFile(node)) return name.includes(str) ? path.join(newAncestry, name) : [];
        const children = getChildren(node);
        return children.flatMap((child) => iter(child, path.join(newAncestry, name)));
    };
    return iter(tree, ancestry);
};



const findFilesByName2 = (tree, substr) => {
    const iter = (node, ancestry) => {
      const name = getName(node);
      const newAncestry = path.join(ancestry, name);
      if (isFile(node)) return name.includes(substr) ? newAncestry : [];
      return getChildren(node).flatMap((child) => iter(child, newAncestry));
    };
    return iter(tree, '');
  };




const findFilesByNameDfs = (tree, substr) => {
    const result = [];
    const fillResult = (node, parents) => {
        const name = getName(node);
        if (isFile(node) && name.includes(substr)) {
            result.push(path.join(...parents, name));
        }
    }

    const dfs = (node, callback, parents = []) => {
        callback(node, parents);
        if (isDirectory(node)) {
            const newParents = [...parents, getName(node)];
            const children = getChildren(node);
            children.forEach(child => dfs(child, callback, newParents));
        }
    };
    dfs(tree, fillResult);

    return result;
}


console.log(findFilesByNameDfs(tree, 'co'));
// ['/etc/nginx/nginx.conf', '/etc/consul/config.json']