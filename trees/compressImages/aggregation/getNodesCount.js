//  проработка теории
// агрегация с использованием рекурсивного процесса на примере подсчёта общего количества узлов в дереве
import {
	mkdir, mkfile, isFile, getChildren, getName, getMeta
  } from '@hexlet/immutable-fs-trees';

const tree = mkdir('/', [
    mkdir('etc', [
      mkfile('bashrc'),
      mkfile('consul.cfg'),
    ]),
    mkfile('hexletrc'),
    mkdir('bin', [
      mkfile('ls'),
      mkfile('cat'),
    ]),
  ]);
  const tree1 = mkdir('my documents', [
    mkfile('avatar.jpg', { size: 100 }),
    mkfile('passport.jpg', { size: 200 }),
    mkfile('family.jpg', { size: 150 }),
    mkfile('addresses', { size: 125 }),
    mkdir('presentationsjpg')
  ]);

  const tree2 = mkdir('/', [
	mkdir('eTc', [
		mkdir('NgiNx'),
		mkdir('CONSUL', [
			mkfile('config.json'),
		]),
	]),
	mkfile('hOsts'),
]);
const tree3 = mkdir('/', [
    mkdir('etc', [
      mkdir('apache'),
      mkdir('nginx', [
        mkfile('.nginx.conf', { size: 800 }),
      ]),
      mkdir('.consul', [
        mkfile('.config.json', { size: 1200 }),
        mkfile('data', { size: 8200 }),
        mkfile('raft', { size: 80 }),
      ]),
    ]),
    mkfile('.hosts', { size: 3500 }),
    mkfile('resolve', { size: 1000 }),
  ]);

  const getNodesCount = (tree) => {
      if (isFile(tree)) return 1;
      const descendantCounts = getChildren(tree).map(getNodesCount);
      return  descendantCounts.length > 0 ? descendantCounts.reduce((a,b) => a + b) + 1 : 1 ;
  };


  let res = getNodesCount(tree);
  console.log(res); // 8
  res = getNodesCount(tree1);
  console.log(res); // 6
  res = getNodesCount(tree2);
  console.log(res); // 6
  res = getNodesCount(tree3);
  console.log(res); // 11