/*
https://ru.hexlet.io/courses/js-trees/lessons/aggregation/exercise_unit

Реализуйте и экспортируйте по умолчанию функцию, которая считает количество скрытых файлов в директории и всех поддиректориях.
Скрытым файлом в Linux системах считается файл, название которого начинается с точки.
*/

import { mkdir, mkfile, isFile, getName, getChildren, isDirectory } from '@hexlet/immutable-fs-trees';

const tree = mkdir('/', [
    mkdir('etc', [
      mkdir('apache'),
      mkdir('nginx', [
        mkfile('.nginx.conf', { size: 800 }),
      ]),
      mkdir('.consul', [
        mkfile('.config.json', { size: 1200 }),
        mkfile('data', { size: 8200 }),
        mkfile('raft', { size: 80 }),
      ]),
    ]),
    mkfile('.hosts', { size: 3500 }),
    mkfile('resolve', { size: 1000 }),
  ]);
  
  const tree1 = mkdir('/', [
    mkdir('etc', [
      mkdir('apache'),
      mkdir('nginx', [
        mkfile('.nginx.conf', { size: 800 }),
      ]),
      mkdir('.consul', [
        mkfile('.config.json', { size: 1200 }),
        mkfile('data', { size: 8200 }),
        mkfile('raft', { size: 80 }),
      ]),
    ]),
    mkfile('.hosts', { size: 3500 }),
    mkfile('resolve', { size: 1000 }),
  ]);


const getHiddenFilesCount = (tree) => { // Time: 1.324 s
    if (isFile(tree)) {
        if (getName(tree)[0] === '.') return 1;
        return 0;
    }
    const count = getChildren(tree).map(getHiddenFilesCount);
    return count.length > 0 ? count.reduce((a, b) => a + b) : 0;
};

const getHiddenFilesCount1 = (tree) => { // Time: 1.258
    if (isFile(tree)) return getName(tree).startsWith('.') ? 1 : 0;
    const count = getChildren(tree).map(getHiddenFilesCount1);
    return count.length > 0 ? count.reduce((a, b) => a + b) : 0;
};

const res = getHiddenFilesCount1(tree1); // 3
console.log(res);