/*
https://ru.hexlet.io/courses/js-trees/lessons/traversal/exercise_unit

Реализуйте и экспортируйте по умолчанию функцию, которая принимает на вход директорию (объект-дерево),
приводит имена всех файлов в этой и во всех вложенных директориях к нижнему регистру.
Результат в виде обработанной директории возвращается наружу.
*/

import {
	mkdir,
	mkfile,
	isFile,
	getName,
	getMeta,
	getChildren,
} from '@hexlet/immutable-fs-trees';
import _ from 'lodash';

const tree = mkdir('/', [
	mkdir('eTc', [
		mkdir('NgiNx'),
		mkdir('CONSUL', [
			mkfile('config.json'),
		]),
	]),
	mkfile('hOsts'),
]);

const downcaseFileNames = (tree) => {
	const newName = isFile(tree) ? _.cloneDeep(getName(tree)).toLowerCase() : getName(tree);
	const meta = getMeta(tree);
	if (isFile(tree)) return mkfile(newName, meta);
	const newChildren = _.cloneDeep(getChildren(tree)).map(downcaseFileNames);
	return mkdir(newName, newChildren, meta);
  };

  const downcaseFileNames1 = (tree) => {
	const newName = getName(tree);
	const meta = getMeta(tree);
	if (isFile(tree)) return mkfile(newName.toLowerCase(), meta);
  
	const newChildren = getChildren(tree).map(downcaseFileNames);
	return mkdir(newName, newChildren, meta);
  };

downcaseFileNames(tree)