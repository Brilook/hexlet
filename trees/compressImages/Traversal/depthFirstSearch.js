// задание со *! просто поиграться

import {
	mkdir, mkfile, isFile, getChildren, getName, getMeta,
  } from '@hexlet/immutable-fs-trees';
import _ from 'lodash';


const tree = mkdir('/', [
	mkdir('etc', [
	  mkfile('bashrc'),
	  mkfile('consul.cfg'),
	]),
	mkfile('hexletrc'),
	mkdir('bin', [
	  mkfile('ls'),
	  mkfile('cat'),
	]),
  ]);

// рекурсивный проход в глубину
const dfs = (tree) => {
	console.log(getName(tree));
	if (isFile(tree)) return;
	getChildren(tree).forEach(dfs);
};

// dfs(tree);

// dfs принимает два аргумента. Первый – вершину дерева, второй – функцию, которую надо вызвать для каждого узла дерева
const dfs1 = (tree, cbf) => {
	cbf(getName(tree));
	if (isFile(tree)) return;
	getChildren(tree).forEach(child => dfs(child, cbf));
};

// dfs(tree, console.log);

//функция, которая меняет владельца для всего дерева, то есть всех директорий и файлов

const changeOwner = (tree, owner) => {
	const name = getName(tree);
	const newMeta = _.cloneDeep(getMeta(tree));
	newMeta.owner = owner;
	if (isFile(tree)) {
		console.log(`File -- Name:${getName(tree)} Owner:${getMeta(tree).owner}`);
		const newFile = mkfile(name, newMeta);
		console.log(`File -- Name:${getName(newFile)} Owner:${getMeta(newFile).owner}`);
		return newFile;
	}
	const children = getChildren(tree)
	const newChildren = children.map((child) => changeOwner(child, owner));
	console.log(`Dir -- Name:${getName(tree)} Owner:${getMeta(tree).owner}`);
	const newTree = mkdir(name, newChildren, newMeta);
	console.log(`Dir -- Name:${getName(newTree)} Owner:${getMeta(newTree).owner}`);
	return newTree;
};

// changeOwner(tree, 'brilook');
