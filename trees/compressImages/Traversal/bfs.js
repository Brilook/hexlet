function bfs(node, callback) {
    const queue = [node];
    while (queue.length) {
      const a = queue.shift();
      queue.push(...a.children);
      callback(a);
    }
  }

  const testData = 
{
  name: '1',
  children: [
    {
      name: '2',
      children: [
        {
          name: '5',
          children: []
        },
        {
          name: '6',
          children: []
        }
      ]
    },
    {
      name: '3',
      children: [
        {
          name: '7',
          children: []
        },
        {
          name: '8',
          children: []
        },
        {
          name: '9',
          children: []
        }
      ]
    },
    {
      name: '4',
      children: [
        {
          name: '10',
          children: [
            {
              name: '11',
              children: [
                {
                  name: '12',
                  children: []
                },
                {
                  name: '13',
                  children: []
                }
              ]
            }        
          ]
        }    
      ]
    }
  ]
}

bfs(testData, (node => node.name));