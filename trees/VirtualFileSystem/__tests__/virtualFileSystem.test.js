import { mkdir, mkfile } from '@hexlet/immutable-fs-trees';
import generate from '../src/virtualFileSystem.js';


// test('mkdir', () => {
//     const newDir = mkdir();

//     expect(newDir).toBeInstanceOf(Object);
//     expect(newDir.children).toBeInstanceOf(Array);
//     expect(newDir.name).toEqual('New Folder');
//     expect(newDir.type).toEqual('directory');
//     expect(newDir.meta).toBeInstanceOf(Object);
// });

// test('mkfile', () => {
//     const newFile = mkfile();

//     expect(newFile).toBeInstanceOf(Object);
//     expect(newFile.name).toEqual('New File');
//     expect(newFile.type).toEqual('file');
//     expect(newFile.meta).toBeInstanceOf(Object);
// });
test('generate', () => {
    const expectation = ({
        name: 'nodejs-package',
        meta: { hidden: true },
        children: [
          { name: 'Makefile' },
          { name: 'README.md' },
          { name: 'dist' },
          {
            name: '__tests__',
            children: [
              { name: 'half.test.js', meta: { type: 'text/javascript' } },
            ],
          },
          { name: 'babel.config.js', meta: { type: 'text/javascript' } },
          {
            name: 'node_modules',
            meta: { owner: 'root', hidden: false },
            children: [
              {
                name: '@babel',
                children: [
                  {
                    name: 'cli',
                    children: ([
                      {
                        name: 'LICENSE',
                      },
                    ]),
                  },
                ],
              },
            ],
          },
        ],
      });
      const tree = generate();
    
      expect(tree).toMatchObject(expectation);
    
});
