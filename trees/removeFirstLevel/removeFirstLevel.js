/*
https://ru.hexlet.io/courses/js-trees/lessons/definition/exercise_unit

Реализуйте и экспортируйте по умолчанию функцию, которая принимает на вход дерево,
и возвращает новое, элементами которого являются дети вложенных узлов (см. пример).

// Второй уровень тут: 5, 3, 4
const tree1 = [[5], 1, [3, 4]];
removeFirstLevel(tree1); // [5, 3, 4]

const tree2 = [1, 2, [3, 5], [[4, 3], 2]];
removeFirstLevel(tree2);
// [3, 5, [4, 3], 2]
*/
const removeFirstLevel = (tree) => tree.reduce((acc, elem) => { // Time: 2.571 s
    if (Array.isArray(elem)) acc.push(elem);
    return acc;
  }, []).flat();

const removeFirstLevel1 = (tree) => tree.filter(Array.isArray).flat(); // Time: 1.724 s



// const tree1 = [[5], 1, [3, 4]];
// removeFirstLevel(tree1); // [5, 3, 4]

const tree2 = [1, 2, [3, 5], [[4, 3], 2]];
removeFirstLevel1(tree2);