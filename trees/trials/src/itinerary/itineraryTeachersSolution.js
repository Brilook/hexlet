import _ from 'lodash';

const makeJoints = (tree, parent) => {
  const [leaf, children] = tree;

  if (!children) {
    return { [leaf]: [parent] };
  }

  const flatChildren = _.flatten(children);
  const neighbors = [...flatChildren, parent]
    .filter((neighbor) => neighbor && !_.isArray(neighbor));
  const joints = children
    .reduce((acc, child) => ({ ...acc, ...makeJoints(child, leaf) }), {});

  return { [leaf]: neighbors, ...joints };
};

const findRoute = (start, finish, joints) => {
  const iter = (current, route) => {
    const routeToCurrent = [...route, current];

    if (current === finish) {
      return routeToCurrent;
    }

    const neighbors = joints[current];
    const filtered = neighbors
      .filter((neighbor) => !routeToCurrent.includes(neighbor));

    return filtered
      .reduce((acc, neighbor) => _.concat(acc, iter(neighbor, routeToCurrent)), []);
  };

  return iter(start, []);
};

export default (tree, start, finish) => {
  const joints = makeJoints(tree);
  return findRoute(start, finish, joints);
};