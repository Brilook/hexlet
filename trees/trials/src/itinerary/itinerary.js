/*
https://ru.hexlet.io/challenges/js_trees_itinerary/instance

Реализуйте и экспортируйте по умолчанию функцию, которая выстраивает маршрут между городами.

Функция принимает 3 аргумента:

дерево городов
город старта
город окончания маршрута
и возвращает массив городов, выстроенный в том же порядке, в котором они находятся на пути следования по маршруту.
*/

const formatingGraf = (tree, dictionary, parent = null) => {
	const [nodeName, branches = []] = tree; 
	const children = [];
	dictionary[nodeName] = {name: nodeName, parent: parent, children: children};
	for (const branch of branches) {
		const point = formatingGraf(branch, dictionary, dictionary[nodeName]);
		children.push(point);
		
	}
	return dictionary[nodeName];
};


const itinerary = (tree, start, finish) => {
	const adjacencyList = {};
	formatingGraf(tree, adjacencyList);

	const result = [];
	const fillResult = (node, parents) => {
		const name = node.name;
		if (name === finish) {
			result.push(...parents, name);
		}
	}

	const dfs = (node, callback, parents = []) => {
		callback(node, parents);
		const newParents = [...parents, node.name];
		const children = node.children;
		children.forEach(child => dfs(child, callback, newParents));
	}

	dfs(adjacencyList[start], fillResult);

	while (!result.includes(finish)) {
		result.push(start)
		start = adjacencyList[start].parent.name;
		dfs(adjacencyList[start], fillResult);
	}

return result;
};


export default itinerary;
