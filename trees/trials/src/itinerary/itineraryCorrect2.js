'use strict';

// Трансформация представления
// Представления1 => Представление2
const formGraph = (tree) => {
  const graph = {};

  const iterate = (repr1, parent = null) => {
    const [nodeName, children = []] = repr1;
    const neighbors = parent ? [parent] : [];
    const newNode = { name: nodeName, neighbors };
    graph[nodeName] = newNode;
  
    children.forEach(child => neighbors.push(iterate(child, newNode)));
    return newNode;
  }

  iterate(tree);
  return graph;
};

// Ходит по произвольному графу в глубину
const deepWalk = (node, callback, walkedPath = []) => {
  const newWalkedPath = [...walkedPath, node];
  let keepWalking = callback(newWalkedPath);
  if (keepWalking) {
    for (const neighbor of node.neighbors) {
      if (!walkedPath.includes(neighbor)) {
        deepWalk(neighbor, callback, newWalkedPath);
      }
    }
  }
}

// В произвольном графе (но только дереве) находит путь между двумя вершинами
const findPath = (start, finish) => {
  let result = null;

  const processNode = (path) => {
    if (path[path.length - 1] === finish) {
      result = path;
    }
    return !result;
  }

  deepWalk(start, processNode);
  return result;
};

// Находит путь между двумя городами
const pathBetweenCities = (treeRepresnt, startName, finishName) => {
  const graph = formGraph(treeRepresnt);
  return findPath(graph[startName], graph[finishName]).map(node => node.name);;
}

export default pathBetweenCities;
