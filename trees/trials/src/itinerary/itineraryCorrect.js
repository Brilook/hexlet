
const formGraph = (tree, routesList, parent = null) => {
  const [pointName, branches = []] = tree;
  const neighbors = parent ? [parent] : [];
  const routePoint = { name: pointName, neighbors: neighbors };
  routesList[pointName] = routePoint;

  branches.forEach(branch => neighbors.push(formGraph(branch, routesList, routePoint)));

  return routePoint;
};


const itineraryCorrect = (tree, start, finish) => {
  const adjacencyList = {};
  formGraph(tree, adjacencyList);

  const result = [];
  const fillResult = (point, parents) => {
    const { name } = point;
    if (name === finish) result.push(...parents, name);
  };

  const dfs = (point, callback, previousPoints = []) => {
    const { name, neighbors } = point;
    if (!previousPoints.includes(name)) {
      callback(point, previousPoints);
      const lastParents = [...previousPoints, name];

      for (const neighbor of neighbors) {
        if (result.length) break;
        dfs(neighbor, callback, lastParents);
      }
    }

  };
  
  dfs(adjacencyList[start], fillResult);

  return result;
};

export default itineraryCorrect;