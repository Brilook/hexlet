/*
https://ru.hexlet.io/challenges/js_trees_flatten/instance


Реализуйте и экспортируйте по умолчанию функцию, которая делает плоским вложенный массив.

Для решения задачи нельзя использовать готовые методы для выравнивания массивов.
*/


const flatten = (list, res = []) => {
    for (const el of list) {
        if (Array.isArray(el)) flatten(el, res);
        else res.push(el);
    }
    return res;
};

const flatten1 = (list) => list.reduce((acc, element) => {
    const result = (Array.isArray(element) ? [...acc, ...flatten(element)] : [...acc, element]);
    return result;
  }, []);


export default flatten;