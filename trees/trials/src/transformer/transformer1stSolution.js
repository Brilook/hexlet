'use strict'

const transform = (tree, startNodeName) => {

  const formGraph = (tree, dependencyList, parent = null) => {
    const [nodeName, branches = []] = tree;
    const children = [];
    const node = { nodeName, parent, children };
    dependencyList[nodeName] = node;
    branches.forEach((branch) => children.push(formGraph(branch, dependencyList, node)));
    return node;
  };
  const dependencyList = {};
  formGraph(tree, dependencyList);

  const startNode = dependencyList[startNodeName];

  const changeHead = (node) => {
    const { children, parent } = node;
    if (parent.parent) {
      changeHead(parent);
    }
    node.children = [...children, parent];
    node.parent = parent.parent;
    parent.parent = node;
    const index = parent.children.indexOf(node);
    parent.children.splice(index, 1);
  };

  changeHead(startNode);

  const formTree = ({ nodeName, children }) => {
    const result = [nodeName];
    if (children.length) result.push(children.map((child) => formTree(child)));
    return result;
  };

  return formTree(startNode);
};

export default transform;