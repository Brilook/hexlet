'use strict';
/*
https://ru.hexlet.io/challenges/js_trees_transformer/instance

Реализуйте и экспортируйте по умолчанию функцию, которая строит дерево относительно заданного корневого узла.

Функция принимает 2 аргумента:
исходное дерево
узел, от которого будет построено новое дерево.

Функция должна возвращать новое дерево с сохранёнными связями между узлами, в котором переданный узел является корневым.
*/

const formGraph = (tree) => {
  const graph = {};

  const iter = (repr, parent = null) => {
    const [nodeName, children = []] = repr;
    const neighbors = parent ? [parent] : [];
    const newNode = { name: nodeName, neighbors };
    graph[nodeName] = newNode;

    children.forEach((child) => neighbors.push(iter(child, newNode)));
    return newNode;
  }
  iter(tree);
  return graph;
};

const deepWalk = (node, callback, walkedPath = []) => {
  const newWalkedPath = [...walkedPath, node];
  const keepWalking = callback(newWalkedPath);
  if (keepWalking) {
    for (const neighbor of node.neighbors) {
      if (!walkedPath.includes(neighbor)) {
        deepWalk(neighbor, callback, newWalkedPath);
      }
    }
  }
};

const transform = (tree, startNodeName) => {
  const reprTree = formGraph(tree);

  const newTree = {};

  const formNewTree = (path) => {

    const visitedNodeOld = path[path.length - 1];
    const parentNodeOld = path.length > 1 ? path[path.length - 2] : null;
    const parentNodeNew = parentNodeOld ? newTree[parentNodeOld.name] : null;

    const newNode = 
      {
         name: visitedNodeOld.name,
         parent: parentNodeNew,
         children: []
      };
  
    newTree[newNode.name] = newNode;

    if (parentNodeNew) {
      parentNodeNew.children.push(newNode);
    }

    return true;
  };

  deepWalk(reprTree[startNodeName], formNewTree);

  const serialize = (node => {
    const result = [node.name];
    if (node.children.length) {
      result.push([]);
      node.children.forEach(child => result[1].push(serialize(child)));
    }
    return result;
  })
  
  return serialize(newTree[startNodeName]);
};

export default transform;