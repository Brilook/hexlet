/*
https://ru.hexlet.io/challenges/js_trees_map_filter_reduce/instance

В этом испытании вам предстоит написать собственную реализацию функций map, filter и reduce,
только работать они будут с файловыми деревьями.

map принимает на вход функцию-обработчик и дерево, а возвращает отображенное дерево.

filter принимает в качестве параметров предикат и дерево, а возвращает отфильтрованное дерево по предикату.

reduce кроме основных параметров (функция-обработчик и дерево) принимает также начальное значение аккумулятора.

Все функции необходимо экспортировать.
*/

export const map = (callback, node) => {
  const newNode = callback(node);
  if (node.children) {
    newNode.children = node.children.map(child => map(callback, child));
  }
  return newNode;
};

export const filter = (callback, node) => {
  const newNode = callback(node) ? node : null;
  if (newNode?.children) {
    const newChildren = newNode.children
      .filter(child => callback(child))
      .map(child => filter(callback, child));
    newNode.children = newChildren;
  }
  return newNode;
};

export const reduce = (callback, node, acc) => {
  const newAcc = callback(acc, node);
  const children = node.children;
  if (!children) return newAcc;
  return children.reduce((acc, n) => reduce(callback, n, acc), newAcc);
};

//== alternative correct solution(ACS)
const reduceACS = (callback, node, acc) => {
  let newAcc = callback(acc, node);
  if (node.children) {
    newAcc = node.children.reduce((acc, n) => reduce(callback, n, acc), newAcc);
  }
  return newAcc;
};

