'use strict';
/*
https://ru.hexlet.io/challenges/js_trees_puzzle/instance

Реализуйте и экспортируйте по умолчанию функцию,
которая объединяет отдельные ветки в одно дерево.
Каждая из веток в свою очередь является также деревом.

Функция может принимать на вход неограниченное количество веток и соединяет их.
Корневым узлом объединённого дерева является корневой узел первой переданной ветки.
*/

export const deserialize = (tree) => {
  const graph = {};

  const iter = (repr, parent = null) => {
    const [nodeName, children = []] = repr;
    const neighbors = parent
      ? [parent]
      : [];
    const newNode = {
      name: nodeName,
      neighbors,
    };
    graph[nodeName] = newNode;

    children.forEach((child) => neighbors.push(iter(child, newNode)));
    return newNode;
  };
  iter(tree);
  return graph;
};

export const deepWalk = (node, callback, walkedPath = []) => {
  const newWalkedPath = [...walkedPath, node];
  const keepWalking = callback(newWalkedPath);
  if (keepWalking) {
    node.neighbors.forEach((neighbor) => {
      if (!walkedPath.includes(neighbor)) {
        deepWalk(neighbor, callback, newWalkedPath);
      }
    });
  }
};

export const serialize = ((node) => {
  const result = [node.name];
  if (node.children.length) {
    result.push([]);
    node.children.forEach((child) => result[1].push(serialize(child)));
  }
  return result;
});

const getIntersectionNode = (graphs) => {
  const nodeNames = graphs.map(graph => Object.keys(graph));
  const intersection = nodeNames.reduce((a, b) => a.filter(x => b.includes(x)));
  const intersectionName = intersection[0];
  return intersectionName ? graphs[0][intersectionName] : null;
};

export const formOrientedTree = (startNode) => {
  const result = {};

  const createNode = (path) => {
    const visitedNodeOld = path[path.length - 1];
    const parentNodeOld = path.length > 1 ? path[path.length - 2] : null;
    const parentNodeNew = parentNodeOld ? result[parentNodeOld.name] : null;

    const newNode = {
      name: visitedNodeOld.name,
      parent: parentNodeNew,
      children: [],
    };

    result[newNode.name] = newNode;

    if (parentNodeNew) {
      parentNodeNew.children.push(newNode);
    }

    return true;
  };

  deepWalk(startNode, createNode);

  return result;
};

const merge = (...serializedBranches) => {

  const branches = serializedBranches.map(deserialize);
  const [mainGraph, ...adjoing] = branches;

  const splicing = (path) => {
    const visitedNodeOld = path[path.length - 1];

    adjoing.forEach((nextGraph) => {
      const splisingNode = nextGraph[visitedNodeOld.name];
      if (splisingNode) {
        const getNameNeighbors = (node) => node.neighbors.map((neighbor) => neighbor.name);
        const uniqName = [
          ...new Set([...getNameNeighbors(visitedNodeOld), ...getNameNeighbors(splisingNode)]),
        ];

        const uniqNode = uniqName.map((name) => {
          if (!(mainGraph[name] || nextGraph[name])) {
            visitedNodeOld.neighbors.forEach((neighbor) => {
              if (!mainGraph[neighbor.name]) {
                mainGraph[neighbor.name] = neighbor;
                mainGraph[visitedNodeOld.name] = visitedNodeOld;
              }
            });
          }

          return mainGraph[name] || nextGraph[name];
        });
        visitedNodeOld.neighbors = uniqNode;
      }
    });

    return true;
  };
  
  const intersectionNode = getIntersectionNode(branches);
  deepWalk(intersectionNode, splicing);

  const startNodeName = serializedBranches[0][0];
  const orientedTree = formOrientedTree(mainGraph[startNodeName]);
  return serialize(orientedTree[startNodeName]);
};

export default merge;
