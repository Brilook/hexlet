'use strict';

import { deserialize, serialize, formOrientedTree } from './puzzle.js';

const getDependences = (matrix, namesList, name) => {
  const index = namesList.indexOf(name);
  const row = matrix[index];
  const { length } = row;
  const res = [];
  for (let i = 0; i < length; i++) {
    if (row[i]) {
      res.push(namesList[i]);
    }
  }
  return res;
};


const formGraphfromMatrix = (matrix, namesList) => {

  const result = {};

  const iter = (name) => {
    const newNode = { name, neighbors: [] };
    result[name] = newNode;
    const neighbsName = getDependences(matrix, namesList, name);
    neighbsName.forEach(neighb => newNode.neighbors.push(result[neighb] || iter(neighb)));
    return newNode;
  };

  iter(namesList[0]);
  return result;
};


const merge = (...serializedBranches) => {
  const branches = serializedBranches.map(deserialize);
  const vertexesName = [...new Set(branches.map(branch => Object.keys(branch)).flat())];
  const vertCount = vertexesName.length;
  const adjMat = Array.from(Array(vertCount), () => new Array(vertCount).fill(0));

  for(let i = 0; i < vertCount; i++) {
    const edges = branches
      .map(branch => branch[vertexesName[i]] ? branch[vertexesName[i]].neighbors : [])
      .flat();
    edges.forEach(edge => adjMat[i][vertexesName.indexOf(edge.name)] = 1);
  }

  const adjGraph = formGraphfromMatrix(adjMat, vertexesName);
  const startNodeName = vertexesName[0];
  const orientedTree = formOrientedTree(adjGraph[startNodeName]);

  return serialize(orientedTree[startNodeName]);
};

export default merge;

