'use strict';

class Graph {
  constructor() {
    this._matrix = [];
    this._nodeNameByIndex = new Map();
    this._nodeIndexByName = new Map();
  }

  _nodeIndexWithName(name) {
    let index = this._nodeIndexByName.get(name);
    if (index === undefined) {
      index = this._nodeIndexByName.size;
      this._nodeIndexByName.set(name, index);
      this._nodeNameByIndex.set(index, name);
      this._matrix[index] = new Array(index).fill(0);
    }

    return index;
  }

  saveNodeWithName(name) {
    this._nodeIndexWithName(name);
  }
 
  saveEdgeWithNodeNames(nodeName1, nodeName2) {
    const index1 = this._nodeIndexWithName(nodeName1);
    const index2 = this._nodeIndexWithName(nodeName2);
    this._matrix[Math.max(index1, index2)][Math.min(index1, index2)] = 1;
  }

  _walkDeep(startNodeName, callback, walkedPath = []) {
    const nodeIndex = this._nodeIndexByName.get(startNodeName);
    const newWalkedPath = [...walkedPath, startNodeName];
    callback(newWalkedPath);

    const neighborNames = this._matrix[nodeIndex]
      .map((val, idx) => val ? idx : null)
      .filter(idx => idx !== null)
      .map(index => this._nodeNameByIndex.get(index));

    for (let i = nodeIndex + 1; i < this._matrix.length; ++i) {
      if (this._matrix[i][nodeIndex]) {
        neighborNames.push(this._nodeNameByIndex.get(i));
      }
    }

    neighborNames.forEach((name) => {
      if (!newWalkedPath.includes(name)) {
        this._walkDeep(name, callback, newWalkedPath);
      }
    });
  }

  serializeWithStart(startNodeName) {
    const graph = {};
    const iteration = (walkedPath) => {
      const newNodeName = walkedPath[walkedPath.length - 1];
      const newNode = [newNodeName]
      graph[newNodeName] = newNode;
      if (walkedPath.length > 1) {
        const parentName = walkedPath[walkedPath.length - 2];
        const parent = graph[parentName];
        if (parent.length == 1) {
          parent[1] = [newNode]
        } else {
          parent[1].push(newNode);
        }
      }
    }

    this._walkDeep(startNodeName, iteration);
    return graph[startNodeName];
  }
}

const merge = (...trees) => {
  const graph = new Graph();

  const storeNode = (node) => {
    graph.saveNodeWithName(node[0]);
    if (node[1]) {
      node[1].forEach((child) => {
        graph.saveEdgeWithNodeNames(node[0], child[0]);
        storeNode(child);
      })
    }
  } 
  
  trees.forEach(storeNode);

  return graph.serializeWithStart(trees[0][0]);
}

export default merge;

