/*
https://ru.hexlet.io/challenges/js_trees_dependencies/instance

Реализуйте и экспортируйте по умолчанию функцию,
которая принимает на вход список зависимостей и возвращает список (массив) отсортированных узлов.

Независимые библиотеки и цепочки библиотек должны быть в порядке, соответствующему порядку элементов в графе зависимостей.
*/

const sortDeps = (depsList) => {
    const nodeNames = new Set(Object.entries(depsList).flat(2));
    const nodes = {};
    for (const nodeName of nodeNames) {
        nodes[nodeName] = {'name' : nodeName, 'in': [], 'out': []};
    }

    for (const node1 in depsList) {
        for (const node2 of depsList[node1]) {
            nodes[node1].out.push(nodes[node2]);
            nodes[node2].in.push(nodes[node1]);
        }
    }

    const result = [];
    const visitedNodes = new Set();

    function fillResult(node) {
        if (!visitedNodes.has(node)) {
            visitedNodes.add(node);
            result.push(node.name);
        }
    }
    
    function dfs(startNode, callbackFn) {
        const children = startNode.out;
        children.forEach(child => dfs(child, callbackFn));
        callbackFn(startNode);
    }
    

    for (const node of nodeNames) {
        dfs(nodes[node], fillResult);
    }

    return result;
}

export default sortDeps;




