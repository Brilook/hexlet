import itineraryCorrect from '../src/itinerary/itineraryCorrect.js'

const tree = ['Moscow', [
    ['Smolensk'],
    ['Yaroslavl'],
    ['Voronezh', [
      ['Liski'],
      ['Boguchar'],
      ['Kursk', [
        ['Belgorod', [
          ['Borisovka'],
        ]],
        ['Kurchatov'],
      ]],
    ]],
    ['Ivanovo', [
      ['Kostroma'], ['Kineshma'],
    ]],
    ['Vladimir'],
    ['Tver', [
      ['Klin'], ['Dubna'], ['Rzhev'],
    ]],
  ]];
  
  export { tree };


  test('itineraryCorrect', () => {
    expect(itineraryCorrect(tree, 'Dubna', 'Kostroma')).toEqual(['Dubna', 'Tver', 'Moscow', 'Ivanovo', 'Kostroma']);
    expect(itineraryCorrect(tree, 'Borisovka', 'Kurchatov')).toEqual(['Borisovka', 'Belgorod', 'Kursk', 'Kurchatov']);
    expect(itineraryCorrect(tree, 'Tver', 'Dubna')).toEqual(['Tver', 'Dubna']);
  })
  

