import itinerary from '../src/itinerary/itinerary.js'

const tree = ['Moscow', [
    ['Smolensk'],
    ['Yaroslavl'],
    ['Voronezh', [
      ['Liski'],
      ['Boguchar'],
      ['Kursk', [
        ['Belgorod', [
          ['Borisovka'],
        ]],
        ['Kurchatov'],
      ]],
    ]],
    ['Ivanovo', [
      ['Kostroma'], ['Kineshma'],
    ]],
    ['Vladimir'],
    ['Tver', [
      ['Klin'], ['Dubna'], ['Rzhev'],
    ]],
  ]];
  
  export { tree };


  test('itinerary', () => {
    expect(itinerary(tree, 'Dubna', 'Kostroma')).toEqual(['Dubna', 'Tver', 'Moscow', 'Ivanovo', 'Kostroma']);
    expect(itinerary(tree, 'Borisovka', 'Kurchatov')).toEqual(['Borisovka', 'Belgorod', 'Kursk', 'Kurchatov']);
    expect(itinerary(tree, 'Tver', 'Dubna')).toEqual(['Tver', 'Dubna']);
  })
  

