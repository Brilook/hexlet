 

import { sortTree } from '@hexlet/graphs';
import merge from '../src/puzzle/puzzleMatrix2.js';

export const branch1 = ['A', [
  ['B', [
    ['C'],
    ['D'],
  ]],
]];

export const branch2 = ['B', [
  ['D', [
    ['E'],
    ['F'],
  ]],
]];

export const branch3 = ['I', [
  ['A', [
    ['B', [
      ['C'],
      ['H'],
    ]],
  ]],
]];


describe('merge', () => {
  
  it('#test1', () => {
    const expected = ['A', [
      ['B', [
        ['C'],
        ['D', [
          ['E'],
          ['F'],
        ]],
        ['H'],
      ]],
      ['I'],
    ]];

    const actual = merge(branch1, branch2, branch3);
    expect(sortTree(actual)).toEqual(expected);
  });

  it('#test2', () => {
    const expected = ['B', [
      ['A', [
        ['I'],
      ]],
      ['C'],
      ['D', [
        ['E'],
        ['F'],
      ]],
      ['H'],
    ]];

    const actual = merge(branch2, branch1, branch3);
    expect(sortTree(actual)).toEqual(expected);
  });

  it('#test3', () => {
    const expected = ['I', [
      ['A', [
        ['B', [
          ['C'],
          ['D', [
            ['E'],
            ['F'],
          ]],
          ['H'],
        ]],
      ]],
    ]];

    const actual = merge(branch3, branch2, branch1);
    expect(sortTree(actual)).toEqual(expected);
  });

  it('#test4', () => {
    const expected = ['B', [
      ['A', [
        ['I'],
      ]],
      ['C'],
      ['D', [
        ['E'],
        ['F'],
      ]],
      ['H'],
    ]];

    const actual = merge(branch2, branch3);
    expect(sortTree(actual)).toEqual(expected);
  });
});
