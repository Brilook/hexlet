import sortDeps from '../src/sortDeps/sortDeps.js';

const deps1 = {
    mongo: [],
    tzinfo: ['thread_safe'],
    uglifier: ['execjs'],
    execjs: ['thread_safe', 'json'],
    redis: [],
  };
  
//   console.log(sortDeps(deps1));
const result = ['mongo', 'thread_safe', 'tzinfo', 'json', 'execjs', 'uglifier', 'redis'];

test('sortDeps', () => {
  expect(sortDeps(deps1)).toEqual(result);
});
