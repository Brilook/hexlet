import transform from '../src/transformer/transformer.js';

const test1 = ['A', [['B', [['D'],]],['C', [['E'],['F'],]],]];
const expected1 = ['F',[['C',[['A',[['B',[['D']]]]],['E']]]]];

const test2 = ['A',[['B',[['D',[['H']]],['E']]],['C',[['F',[['I',[['M']]],['J',[['N'],['O']]]]],['G',[['K'],['L']]]]]]];
const expected2 = ['F',[['C',[['A',[['B',[['D',[['H']]],['E']]]]],['G',[['K'],['L']]]]],['I',[['M']]],['J',[['N'],['O']]]]];

test('transform', () => {
  expect(transform(test1, 'F')).toEqual(expected1);
  expect(transform(test2, 'F')).toEqual(expected2);
});

