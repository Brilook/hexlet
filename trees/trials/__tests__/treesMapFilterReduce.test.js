
import _ from 'lodash';
import { mkdir, mkfile } from '@hexlet/immutable-fs-trees';
import { map, filter, reduce } from '../src/treesMapFilterReduce/treesMapFilterReduce.js';


describe('map', () => {  
  test('map 1', () => {
      const tree = mkdir('/', [
        mkdir('eTc', [
          mkdir('NgiNx'),
          mkdir('CONSUL', [
            mkfile('config.json'),
          ]),
        ]),
        mkfile('hOsts'),
      ]);
    
      const actual = map((n) => ({ ...n, name: n.name.toUpperCase() }), tree);
    
      const expected = {
        children: [
          {
            children: [
              {
                children: [],
                meta: {},
                name: 'NGINX',
                type: 'directory',
              },
              {
                children: [{ meta: {}, name: 'CONFIG.JSON', type: 'file' }],
                meta: {},
                name: 'CONSUL',
                type: 'directory',
              },
            ],
            meta: {},
            name: 'ETC',
            type: 'directory',
          },
          { meta: {}, name: 'HOSTS', type: 'file' },
        ],
        meta: {},
        name: '/',
        type: 'directory',
      };
    
      expect(actual).toEqual(expected);
    });

    test('immutable', () => {
      const tree = mkdir('/', [
        mkdir('eTc', [
          mkdir('NgiNx'),
          mkdir('CONSUL', [
            mkfile('config.json'),
          ]),
        ]),
        mkfile('hOsts'),
      ]);
      const original = _.cloneDeep(tree);
    
      map((n) => ({ ...n, name: n.name.toUpperCase() }), tree);
    
      expect(tree).toEqual(original);
    });

  });

describe('filter', () => {
  test('filter 1', () => {
    const tree = mkdir('/', [
      mkdir('etc', [
        mkdir('nginx', [
          mkdir('conf.d'),
        ]),
        mkdir('consul', [
          mkfile('config.json'),
        ]),
      ]),
      mkfile('hosts'),
    ]);
    const actual = filter((n) => n.type === 'directory', tree);
  
    const expected = {
      children: [
        {
          children: [
            {
              children: [{
                children: [],
                meta: {},
                name: 'conf.d',
                type: 'directory',
              }],
              meta: {},
              name: 'nginx',
              type: 'directory',
            },
            {
              children: [],
              meta: {},
              name: 'consul',
              type: 'directory',
            },
          ],
          meta: {},
          name: 'etc',
          type: 'directory',
        },
      ],
      meta: {},
      name: '/',
      type: 'directory',
    };
  
    expect(actual).toEqual(expected);
  });
  
  test('filter 2', () => {
    const tree = mkdir('/', [
      mkdir('etc', [
        mkdir('nginx', [
          mkdir('conf.d'),
        ]),
        mkdir('consul', [
          mkfile('config.json'),
        ]),
      ]),
      mkfile('hosts'),
    ]);
    const names = new Set(['/', 'hosts', 'etc', 'consul']);
    const actual = filter((n) => names.has(n.name), tree);
  
    const expected = {
      children: [
        {
          children: [
            {
              children: [],
              meta: {},
              name: 'consul',
              type: 'directory',
            },
          ],
          meta: {},
          name: 'etc',
          type: 'directory',
        },
        {
          name: 'hosts',
          meta: {},
          type: 'file',
        },
      ],
      meta: {},
      name: '/',
      type: 'directory',
    };
    expect(actual).toEqual(expected);
  });

  test('filter empty', () => {
    const tree = mkdir('empty');
    const actual = filter((n) => false, tree);
    const expected = null;
    expect(actual).toEqual(expected);
  });

  test('filter empty children', () => {
    const tree = mkdir('/', []);
    const names = new Set(['/', 'hosts', 'etc', 'consul']);
    const actual = filter((n) => names.has(n.name), tree);
  
    const expected = {
      children: [],
      meta: {},
      name: '/',
      type: 'directory',
    };
    
    expect(actual).toEqual(expected);
  });

});

describe('reduce', () => {
  test('reduce 1', () => {
    const tree = mkdir('/', [
      mkdir('eTc', [
        mkdir('NgiNx'),
        mkdir('CONSUL', [
          mkfile('config.json'),
        ]),
      ]),
      mkfile('hOsts'),
    ]);
    const actual = reduce((acc) => acc + 1, tree, 0);
    expect(actual).toEqual(6);
  
    const actual2 = reduce((acc, n) => (n.type === 'file' ? acc + 1 : acc), tree, 0);
    expect(actual2).toEqual(2);
  
    const actual3 = reduce((acc, n) => (n.type === 'directory' ? acc + 1 : acc), tree, 0);
    expect(actual3).toEqual(4);
  });
});
