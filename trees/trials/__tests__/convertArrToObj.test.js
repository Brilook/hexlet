import convert from '../src/arrToObj/convertArrToObj.js'



const test1 = [['key', [['key2', 'anotherValue']]],['key2', 'value2']];
const test2 = [['key', 'value']];
const test3 = [['key', 'value'], ['key2', 'value2']];


test('convert', () => {
    expect(convert(test1)).toMatchObject({ key: { key2: 'anotherValue' }, key2: 'value2' });
    expect(convert(test2)).toMatchObject({ key: 'value' });
    expect(convert(test3)).toMatchObject( { key: 'value', key2: 'value2' });
  });