
import transform from '../src/transformer/transformer1stSolution.js';

export const test1 = ['A', [['B', [['D'],]],['C', [['E'],['F'],]],]];
const expected1 = ['F',[['C',[['E'],['A',[['B',[['D']]]]]]]]];

const test2 = ['A',[['B',[['D',[['H']]],['E']]],['C',[['F',[['I',[['M']]],['J',[['N'],['O']]]]],['G',[['K'],['L']]]]]]];
const expected2 = ['F',[['I',[['M']]],['J',[['N'],['O']]],['C',[['G',[['K'],['L']]],['A',[['B',[['D',[['H']]],['E']]]]]]]]];

test('transform', () => {
  expect(transform(test1, 'F')).toEqual(expected1);
  expect(transform(test2, 'F')).toEqual(expected2);
});