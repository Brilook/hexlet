import flatten from '../src/flatten/flatten.js';

const list1= [1, 2, [3, 5], [[4, 3], 2]];
const list2 = [[1, [5], [], [[-3, 'hi']]], 'string', 10, [[[5]]]];

test ('flatten', () => {
    expect(flatten(list1)).toEqual([1, 2, 3, 5, 4, 3, 2]);
    expect(flatten(list2)).toEqual([1, 5, -3, 'hi', 'string', 10, 5]);
});