/*
https://ru.hexlet.io/courses/introduction_to_programming/lessons/pure/exercise_unit

Реализуйте и экспортируйте по умолчанию функцию, которая делает заглавной первую букву каждого слова в предложении.
solution('hello, world!'); // Hello, World!
Подсказки
Вычисление длины строки: length(str).
Перевод строки/буквы в верхний регистр: toUpperCase(str).
*/

// import { length, toUpperCase } from './strings';

const solution = (str) => {
    let result = '';
    const length  = str.length;
    for (let i = 0; i < length; i += 1) {
      if (str[i] !== ' ' && (str[i - 1] === ' ' || i === 0)) {
        result += str[i].toUpperCase();
      } else {
        result += str[i];
      }
    }
    return result;
};

  solution('hello, world!');
//   export default solution;

