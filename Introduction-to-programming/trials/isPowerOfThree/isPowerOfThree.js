/*
https://ru.hexlet.io/challenges/programming_basics_power3/instance

Реализуйте и экспортируйте по умолчанию функцию isPowerOfThree, которая определяет, является ли переданное число натуральной степенью тройки.
Например, число 27 — это третья степень: 3^3, а 81 — это четвёртая: 3^4.

Пример:

isPowerOfThree(1); // true (3^0)
isPowerOfThree(2); // false
isPowerOfThree(9); // true (3^2)
*/

const isPowerOfThree = (num) => {
    let res = false;
    for (let i = 0; i <= num; i++) {
        if (Math.pow(3, i) === num) {
            res = true;
            break;
        }
    }
    return res;
};

const isPowerOfThree1 = (num) => {
    let current = 1;
    while (current < num) {
        current *= 3;
    }
    return current === num;
};

isPowerOfThree1(27)