/*
https://ru.hexlet.io/challenges/programming_basics_happy_ticket/instance

"Счастливым" называют билет с номером, в котором сумма первой половины цифр равна сумме второй половины цифр. Номера могут быть произвольной длины, с единственным условием, что количество цифр всегда чётно, например: 33 или 2341 и так далее.

Билет с номером 385916 — счастливый, так как 3 + 8 + 5 = 9 + 1 + 6. Билет с номером 231002 не является счастливым, так как 2 + 3 + 1 != 0 + 0 + 2.

isHappyTicket.js
Реализуйте и экспортируйте по умолчанию функцию isHappyTicket, проверяющую является ли номер счастливым (номер — всегда строка).
Функция должна возвращать true, если билет счастливый, или false, если нет.
 */

const isHappyTicket = (str) => {
    let right = 0;
    let left = 0;
    const length = str.length - 1;
    for (let i = 0; i <= length; i++) {
        if (i < length / 2) {
            left += Number(str[i]);
        } else {
            right += Number(str[i]);
        }
    }
    return left === right;
};

const isHappyTicket1 = (str) => {
    let balance = 0;
    const length = str.length - 1;
    for (let i = 0, j = length; i < j; i++, j--) {
        balance += Number(str[i]) - Number(str[j]);
    }
    return balance === 0;
}


isHappyTicket1('385916'); // true
isHappyTicket('231002'); // false
isHappyTicket('1222'); // false
isHappyTicket('054702'); // true
isHappyTicket('00'); // true