/*
https://ru.hexlet.io/challenges/intro_to_programming_angle_difference/instance

Напишите функцию diff, которая принимает два угла (integer), каждый от 0 до 360, и возвращает разницу между ними.
Угол между лучами измеряется двумя способами:
Функция должна вернуть наименьшее значение.
Примеры:
diff(0, 45) === 45;         // не 315, а 45, потому что 45 меньше
diff(0, 180) === 180;
diff(0, 190) === 170;       // не 190, а 170, потому что 170 меньше
diff(120, 280) === 160;
*/

const diff = (firstAngle, secondAngle) => {
    let big;
    let small;
    if (firstAngle >= secondAngle) {
        big = firstAngle;
        small = secondAngle;
    } else {
        big = secondAngle;
        small = firstAngle;
    }

    const res1 = big - small;
    const res2 = 360 - big + small;

    return (res1 >= res2) ? res2 : res1;
};

//----teacher solution
const diff1 = (a, b) => {
    const angle = Math.abs(a - b);
    return Math.min(angle, 360 - angle);
};

diff(300, 45); //(105)
diff(0, 45); //45
diff(0, 180); //180
diff(0, 190); //170
diff(120, 280); //160