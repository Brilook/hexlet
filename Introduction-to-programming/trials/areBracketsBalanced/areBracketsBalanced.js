/*
https://ru.hexlet.io/challenges/intro_to_programming_balanced_brackets/instance

Реализуйте и экспортируйте функцию по умолчанию, которая принимает на вход строку, состоящую только из открывающих и закрывающих круглых скобок, и проверяет является ли эта строка корректной.
Пустая строка (отсутствие скобок) считается корректной.
Строка считается корректной (сбалансированной), если содержащаяся в ней скобочная структура соответствует требованиям:

Скобки — это парные структуры. У каждой открывающей скобки должна быть соответствующая ей закрывающая скобка.
Закрывающая скобка не должна идти впереди открывающей.
import areBracketsBalanced from 'roundBracketsValidator';
*/
const areBracketsBalanced = (str) => {
    let res = false;
    if (str.length % 2 === 0) {
        let open = 0;
        let close = 0;
        for (let i = 0; i < str.length; i++) {
            str[i] === '(' ? open++ : close++;
            if (close > open) {
                res = false;
                break;
            }
        }
        res = open === close;
    }
    return res;
};

const areBracketsBalanced1 = (str) => {
    let balanse = 0;
    for (let i = 0; i < str.length && balanse >= 0; i++) {
        str[i] === '(' ? balanse++ : balanse--;
    }
    return balanse === 0;
};

areBracketsBalanced1('(())'); // true
areBracketsBalanced1('((())'); // false
areBracketsBalanced1('())('); // false