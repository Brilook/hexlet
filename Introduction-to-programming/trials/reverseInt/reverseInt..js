/*
https://ru.hexlet.io/challenges/programming_basics_reverse_integer/instance

Реализуйте и экспортируйте по умолчанию функцию reverseInt, которая переворачивает цифры в
переданном числе и возвращает новое число.

reverseInt(13); // 31
reverseInt(-123); // -321
reverseInt(8900); // 98
 */


const reverseInt = (num) => {
    let reversNum = 0;
    const div = 10;
    let tmp = num;
    while (tmp !== 0) {
      reversNum *= div;
      reversNum += (tmp % div);
      tmp = Math.trunc(tmp / div);
    }
    return reversNum;
  };