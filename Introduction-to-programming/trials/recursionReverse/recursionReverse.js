/*
https://ru.hexlet.io/challenges/programming_basics_reverse_string/instance

Реализуйте и экспортируйте функцию по умолчанию, которая переворачивает строку задом наперед, используя рекурсию.
*/


const recursionReverse = (str) => {
    if (str.length === 0) {
        return str;
    }
    return `${str.slice(-1)}${recursionReverse(str.slice(0, -1))}`;
};


reverse('str'); // rts
reverse('hexlet'); // telxeh