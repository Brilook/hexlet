/*
https://ru.hexlet.io/challenges/intro_to_programming_perfect_numbers/instance

Создайте функцию isPerfect, которая принимает число и возвращает true, если оно совершенное, и false — в ином случае.
Совершенное число — положительное целое число, равное сумме его положительных делителей (не считая само число).
Например, 6 — идеальное число, потому что 6 = 1 + 2 + 3.
*/

const isPerfect = (num) => {
    let divisorsSum = 1;
    for (let i = 2; divisorsSum < num; i++) {
        if (num % i === 0) {
            divisorsSum += i;
        }
    }
    return divisorsSum === num;
};
isPerfect(28)