/*
https://ru.hexlet.io/challenges/programming_basics_invert_case/instance

invertCase.js
Реализуйте и экспортируйте по умолчанию функцию invertCase, которая меняет в строке регистр каждой буквы на противоположный.

*/

const invertCase = (str) => {
    const length = str.length;
    let revertStr = '';
    for (let i = 0; i < length; i++) {
        let letter;
        if (str[i] > 'Z') {
            letter = str[i].toUpperCase();
        } else {
            letter = str[i].toLowerCase();
        }
        revertStr += letter;
    }
    return revertStr;
};


const invertCase1 = (str) => {
    const length = str.length;
    let revertStr = '';
    for (let i = 0; i < length; i++) {
        revertStr += (str[i] > 'Z') ? str[i].toUpperCase() : str[i].toLowerCase();
    }
    return revertStr;
};

invertCase('Hello, World!'); // hELLO, wORLD!
invertCase1('I loVe JS'); // i LOvE js