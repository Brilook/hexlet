/*
https://ru.hexlet.io/challenges/programming_basics_time/instance

Реализуйте и экспортируйте по умолчанию функцию, которая принимает на вход количество минут (прошедших с начала суток)
и возвращает строку, являющуюся временем в формате чч:мм. Если количество минут содержит больше 24 часов (1 суток),
то функция возвращает время прошеднее с полуночи последних суток.
 */
const format = (t) => t.toString().padStart(2, '0');

const formattedTime = (min) => {
    const hours = Math.floor(min / 60) % 24;
    const minutes = min % 60;
    return `${format(hours)}:${format(minutes)}`;
};



// formattedTime(5); // 00:05
// formattedTime(15); // 00:15
// formattedTime(60); // 01:00
// formattedTime(67); // 01:07
// formattedTime(175); // 02:55
// formattedTime(600); // 10:00
// formattedTime(754); // 12:34
formattedTime(1504); // 01:04