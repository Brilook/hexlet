/*
Source
https://ru.hexlet.io/courses/introduction_to_programming/lessons/mutators/exercise_unit

Напишите функцию isPrime. Она принимает число и возвращает true, если число является простым, и false в ином случае.
*/


// function isPrime(number) {
//     let count = 0;
//     let res = true;
//     if (number <= 1) {
//         res = false;
//     }
//     for (let divisor = 1; divisor <= number; divisor++) {
//         if (number % divisor === 0) {
//             count++
//             if (count > 2 && divisor === number) {
//                 res = false;
//                 break;
//             }
//         }
//     }
//     return res;
// }
function isPrime(number) {
    let result = true;
    if (number < 2) {
        result = false;
    }
    const sqrt = Math.round(Math.sqrt(number));
    for (let i = 2; i <= sqrt; i++) {
        if (number % i === 0) {
            result = false;
        }
    }
    return result;
}



isPrime(21); // false
isPrime(1); // false
isPrime(7); // true
isPrime(10); // false