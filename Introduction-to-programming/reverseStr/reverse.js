/*
https://ru.hexlet.io/courses/introduction_to_programming/lessons/string/exercise_unit

Реализуйте функцию reverse, которая переворачивает строку. Например:

reverse('hello, world!'); // !dlrow ,olleh
*/

const reverse = (str) => {
  let reverseStr = '';
  const length = str.length;
  for (let i = length - 1; i >= 0; i--) {
    reverseStr += str[i];
  }
  return reverseStr;
};


const reverse1 = (str) => str.split('').reverse().join('');


reverse('hello, world!');