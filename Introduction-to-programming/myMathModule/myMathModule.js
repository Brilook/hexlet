/*
https://ru.hexlet.io/courses/introduction_to_programming/lessons/modules/exercise_unit

В файле myMathModule.js:
Создайте функцию getTriangleArea, которая принимает два аргумента h и b и вычисляет площадь треугольника
по формуле A = 1/2 * h * b, где h — высота, а b — основание треугольника.
Экспортируйте функцию.

В файле solution.js:
Импортируйте getTriangleArea из модуля myMathModule.
Создайте функцию, которая принимает аргумент n и возвращает площадь треугольника высотой n и основанием n2/2. Используйте функцию square (принимает число и возвращает его квадрат).
Экспортируйте созданную функцию по умолчанию.
*/


export const getTriangleArea = function (h, b) {
    return  h * b / 2;
}

// getTriangleArea(5, 10) //25;
// getTriangleArea(15, 12) //90;



