/*
https://ru.hexlet.io/courses/introduction_to_programming/lessons/types/exercise_unit

Реализуйте и экспортируйте по умолчанию функцию addDigits, которая работает следующим образом:

Дано неотрицательное целое число num. Складывать все входящие в него цифры до тех пор, пока не останется одна цифра.

Для числа 38 процесс будет выглядеть так:

3 + 8 = 11
1 + 1 = 2
Результат: 2

Пример:

addDigits(10); // 1
addDigits(19); // 1
addDigits(38); // 2
addDigits(1259); // 8
Подсказка
Выделите процесс суммирования цифр в числе в отдельную функцию
*/

// import { length } from './strings';

const addDigits = (num) => {
    let sum = num;
    if (num >= 10) {
        sum = sumDigitsOfNum(num);
        while (sum.toString().length > 1) {
            sum = sumDigitsOfNum(sum);
        }
    }
    return sum;
}

function sumDigitsOfNum(number) {
    const divisor = 10;
    let summ = 0;
    let newNum = number;
    while (newNum > 0) {
        const digit = newNum % divisor;
        summ += digit;
        newNum = Math.trunc(newNum / divisor);
    }
    return summ;
}

addDigits(598997686567)

//--------------------2nd scenario

const sumDigits = (num) => {
  const str = String(num);
  let result = 0;
  const length = str.length;
  for (let i = 0; i < length; i += 1) {
    result += Number(str[i]);
  }

  return result;
};

const addDigits1 = (num) => {
  let result = num;
  while (result >= 10) {
    result = sumDigits(result);
  }

  return result;
};


//--------------------3rd scenario

function sumDigitsOfNum1(number) {
  const divisor = 10;
  let summ = 0;
  let newNum = number;
  while (newNum > 0) {
      const digit = newNum % divisor;
      summ += digit;
      newNum = Math.trunc(newNum / divisor);
  }
  return summ;
}

const addDigits2 = (num) => {
  let result = num;
  while (result >= 10) {
    result = sumDigitsOfNum1(result);
  }

  return result;
};



addDigits3(598997686567)

// export default addDigits;