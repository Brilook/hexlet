/*
https://ru.hexlet.io/courses/introduction_to_programming/lessons/expressions/exercise_unit

squares.js
Реализуйте функцию square, которая возвращает квадрат числа.
Реализуйте функцию sumOfSquares, которая возвращает сумму квадратов двух чисел.
Реализуйте функцию squareSumOfSquares, которая возвращает квадрат суммы квадратов двух чисел.
*/

const square = (number) => number * number;

const sumOfSquares = (a, b) => square(a) + square(b);

const squareSumOfSquares = (a, b) => square(sumOfSquares(a, b));