/*
Напишите функцию smallestDivisor. Она должна находить наименьший целый делитель числа.
реализуйте императивный итеративный процесс, что означает:

не используйте рекурсию
используйте переменные
используйте цикл while
Например, наименьший делитель числа 15 это 3.

smallestDivisor(15); // 3
smallestDivisor(17); // 17

smallestDivisor(0); // NaN
smallestDivisor(-3); // NaN
Замечание: Если переданное в smallestDivisor число меньше единицы, возвращайте NaN.
*/

const smallestDivisor = (number) => {
    let divisor = 1;
    if (number < 1) {
        divisor = NaN;
    } else {
        let result;
        while (divisor !== number && result !== 0) {
            divisor++;
            result = number % divisor;
        }
    }
    return divisor;
};

smallestDivisor(7)


// export default smallestDivisor;