'use strict';
/*
https://ru.hexlet.io/challenges/js_object_oriented_design_lazy/instance

В этой задаче необходимо реализовать ленивую коллекцию.

enumerable.js
Реализуйте и экспортируйте по умолчанию класс,
который предназначен для обработки коллекций объектов.
Основная особенность работы данного класса заключается в том, что он использует lazy вариант обработки.

import Enumerable from './enumerable.js';

const elements = [
  { key: 'value' },
  { key: '' },
];
const coll = Enumerable.wrap(elements);
const result = coll.where('key', 'value');

result.all() // [{ key: 'value' }]

Подсказки
Подробнее способы использования описаны в тестах.
Усложнённый вариант: добавьте метод с мемоизацией и протестируйте его,
запуская командой make test-memo в терминале упражнения.
*/
export default class Enumerable {
  static wrap(elements) {
    return new Enumerable(elements);
  }

  constructor(elements) {
    this._elements = elements;
    this._memorizedAll = null;
  }

  getElements() {
    return this._elements;
  }

  all() {
    return Object.assign([], this.getElements());
  }

  where(key, value) {
    const filtered = this.getElements().filter(elem => (key in elem) && elem[key] === value);
    return new Enumerable(filtered);
  }

  allWithMemoization() {
    if (!this._memorizedAll) {
      this._memorizedAll = this.all();
    }
    return this._memorizedAll;
  }
}
