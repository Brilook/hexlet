'use strict';
/*
https://ru.hexlet.io/courses/js-object-oriented-design/lessons/proxy/exercise_unit

JavaScript долгое время не поддерживал приватных свойств и методов.
Для них появилось соглашение об именовании с нижнего подчёркивания _,
чтобы предотвратить доступ ко внутренностям объекта в обход интерфейса.
Но сама возможность прямого доступа остаётся.

Нам предстоит разработать обёртку над объектом, защищающую его приватные свойства от прямого доступа.

Реализуйте и экспортируйте по умолчанию функцию,
которая принимает объект и позволяет обращаться только к "публичным" свойствам и методам.
При попытке прочитать или перезаписать приватное или несуществующее свойство должно выбрасываться исключение.

Примеры:
import protect from '../protect.js';

class Course {
  constructor(name) {
    this._name = name;
  }

  getName() {
    return this._name;
  }
}

const course = new Course('Object-oriented design');
const protectedCourse = protect(course);

course.getName(); // "Object-oriented design"
protectedCourse.getName(); // "Object-oriented design"
course._name; // "Object-oriented design"
course._nonExists; // undefined

protectedCourse._name; // Error
protectedCourse._name = 'OOD'; // Error
protectedCourse._nonExists; // Error
*/

function protect(obj, prefix = '_') {
  return new Proxy(obj, {
    get(target, prop) {
      if (!(prop in target) || prop.startsWith(prefix)) {
        throw new Error('Access denied');
      }
      const value = target[prop];
      return (typeof value === 'function') ? value.bind(target) : value;
    },
    set(target, prop, val) {
      if (prop.startsWith(prefix)) {
        throw new Error('Access denied');
      } else {
        target[prop] = val;
        return true;
      }
    },
  });
}

export default protect;