'use strict';
/*
https://ru.hexlet.io/challenges/js_object_oriented_design_range_of_dates/instance

Реализуйте и экспортируйте по умолчанию функцию, которая переводит входные данные в удобный для построения графика формат.

На вход эта функция принимает три аргумента: массив данных; дата начала периода; дата конца периода.
Данные представлены в формате объекта вида { value: 14, date: '02.08.2018' }, а даты диапазона в формате 'yyyy-MM-dd'.

Диапазон дат задаёт размер выходного массива, который должна сгенерить реализуемая функция. Правила формирования итогового массива:

он заполняется записями по всем дням из диапазона begin - end.
если во входном массиве нет данных для какого-то дня из диапазона, то в свойство value записи этого дня установить значение 0.
import buildRange from './dates.js';

const dates = [
  { value: 14, date: '02.08.2018' },
  { value: 43, date: '03.08.2018' },
];
const beginDate = '2018-08-01';
const endDate = '2018-08-04';

buildRange(dates, beginDate, endDate);
// [
//   { value: 0, date: '01.08.2018' },
//   { value: 14, date: '02.08.2018' },
//   { value: 43, date: '03.08.2018' },
//   { value: 0, date: '04.08.2018' },
// ]
*/

import { eachDayOfInterval, format } from 'date-fns';

const buildRange = (dates, beginDate, endDate) => {
  const interval = {
    start: new Date(beginDate),
    end: new Date(endDate),
  };

  const reprDate = dates.reduce((acc, { date, value }) => {
    acc[date] = value;
    return acc;
  }, {});

  const range = eachDayOfInterval(interval).map(date => format(date, 'dd.MM.yyyy'));

  return range.map(date => ({ value: reprDate[date] ?? 0, date }));
};

export default buildRange;