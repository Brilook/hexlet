'use strict';

export default class Drunkard {
  run(cards1, cards2, maxRound = 100) {
    const player1 = [...cards1];
    const player2 = [...cards2];
    let result = 'Botva!';

    for (let round = 0; round < maxRound; round++) {
      const player1IsEmpty = player1.length === 0;
      const player2IsEmpty = player2.length === 0;

      if (player1IsEmpty && player2IsEmpty) {
        break;
      } else if (player1IsEmpty) {
        result = `Second player. Round: ${round}`;
        break;
      } else if (player2IsEmpty) {
        result = `First player. Round: ${round}`;
        break;
      }

      const card1 = player1.pop();
      const card2 = player2.pop();

      if (card1 > card2) {
        player1.unshift(card1, card2);
      } else if (card1 < card2) {
        player2.unshift(card2, card1);
      }
    }
    return result;
  }
}

