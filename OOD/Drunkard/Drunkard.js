'use ctrict';
/*
https://ru.hexlet.io/challenges/js_object_oriented_design_drunkard/instance

Пьяница — карточная игра, в которой побеждает тот игрок, который собирает все карты.
В нашей задаче используется модификация игры с двумя игроками.
Игрокам раздаётся равное количество карт. Игроки не смотрят в свои карты, а кладут их в стопку рядом с собой.
Затем каждый игрок снимает верхнюю карту и показывает её сопернику.
Тот игрок, чья карта оказалась большего номинала,
берёт обе карты и кладёт их к себе в колоду снизу (так что своя карта идёт первой).
Если карты имеют одинаковый номинал, то они выкидываются из игры. В игре возможны три исхода:

У обоих игроков не осталось карт
Игра не может закончиться
Победил один из игроков
drunkard.js
Реализуйте и экспортируйте по умолчанию класс с методом run(),
принимающей на вход два списка чисел, которые представляют собой карты для первого и второго игроков.

Если выиграл первый игрок, то метод должен вернуть First player. Round: <номер раунда>.
Если выиграл второй игрок, то метод должен вернуть Second player. Round: <номер раунда>.
Если у игроков не осталось карт, то метод должен вернуть Botva!
Если за 100 раундов не удалось выявить победителя то также возвращается Botva!
*/

export default class Drunkard {
  static maxRoundCount = 100;

  constructor() {
    this.firstPlayerCards = [],
    this.secondPlayerCards = []
  }

  _runRound() {
    const roundCards = [this.firstPlayerCards, this.secondPlayerCards].map(list => list.pop());

    if (roundCards[0] > roundCards[1]) {
      roundCards.forEach(card => this.firstPlayerCards.unshift(card));
    }
    if (roundCards[0] < roundCards[1]) {
      roundCards.reverse().forEach(card => this.secondPlayerCards.unshift(card));
    }
  }

  run(firstCards, secondCards) {
    this.firstPlayerCards = firstCards;
    this.secondPlayerCards = secondCards;
    let roundCount = 0;
    let result = null;
    
    do {
      this._runRound();
      roundCount++;
      const isEmptyFirstList = this.firstPlayerCards.length === 0;
      const isEmptySecondList = this.secondPlayerCards.length === 0;

      if (isEmptyFirstList && !isEmptySecondList) {
        result = `Second player. Round: ${roundCount}`;
      } else if (!isEmptyFirstList && isEmptySecondList) {
        result = `First player. Round: ${roundCount}`;
      } else if ((isEmptyFirstList && isEmptySecondList) || roundCount === this.maxCountRound) {
        result = 'Botva!';
      }

    } while (!result);

    return result;
  }
}

const game = new Drunkard();
game.run([1, 2], [3, 2]) //, 'Second player. Round: 2']

