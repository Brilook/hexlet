'use strict';
/*
https://ru.hexlet.io/courses/js-object-oriented-design/lessons/modeling/exercise_unit

В данном упражнении вам предстоит реализовать класс Url, который позволяет извлекать из HTTP адреса,
представленного строкой, его части. Экспортируйте класс по умолчанию.

Класс должен содержать конструктор и методы:

конструктор - принимает на вход HTTP адрес в виде строки.
getScheme() - возвращает протокол передачи данных (без двоеточия).
getHostName() - возвращает имя хоста.
getQueryParams() - возвращает параметры запроса в виде пар ключ-значение объекта.
getQueryParam() - получает значение параметра запроса по имени.
Если параметр с переданным именем не существует, метод возвращает значение заданное вторым параметром (по умолчанию равно null).
equals(url) - принимает объект класса Url и возвращает результат сравнения с текущим объектом - true или false.
В процессе прохождения испытания вам нужно будет хорошо поработать с документацией и изучить возможности класса URL,
для того чтобы распарсить строковое представление HTTP адреса.

Примеры
const url = new Url('http://yandex.ru:80?key=value&key2=value2');
url.getScheme(); // 'http'
url.getHostName(); // 'yandex.ru'
url.getQueryParams();
// {
//   key: 'value',
//   key2: 'value2',
// };
url.getQueryParam('key'); // 'value'
// второй параметр - значение по умолчанию
url.getQueryParam('key2', 'lala'); // 'value2'
url.getQueryParam('new', 'ehu'); // 'ehu'
url.getQueryParam('new'); // null
url.equals(new Url('http://yandex.ru:80?key=value&key2=value2')); // true
url.equals(new Url('http://yandex.ru:80?key=value')); // false
Подсказки
Не используйте в решении устаревшие возможности (Legacy URL API).
*/

export default class Url {
  constructor(address) {
    this.url = new URL(address);
  }

  getScheme() {
    return this.url.protocol.slice(0, -1);
  }

  getHostName() {
    return this.url.hostname;
  }

  getQueryParams() {
    return Object.fromEntries(this.url.searchParams);
  }

  getQueryParam(key, defaultValue = null) {
    return this.getQueryParams()[key] || defaultValue;
  }

  equals(url) {
    return this.url.href === url.url.href;
  }
}

// teacher`s solution
export class Url2 {
  constructor(url) {
    this.url = new URL(url);
    this.url.scheme = this.url.protocol.slice(0, -1);
    this.url.queryParams = Object.fromEntries(this.url.searchParams);
  }

  getScheme() {
    return this.url.scheme;
  }

  getHostName() {
    return this.url.hostname;
  }

  getQueryParams() {
    return this.url.queryParams;
  }

  getQueryParam(key, defaultValue = null) {
    return this.url.searchParams.has(key) ? this.url.searchParams.get(key) : defaultValue;
  }

  toString() {
    return this.url.toString();
  }

  equals(url) {
    return (this.toString() === url.toString());
  }
}

// mykolka
export class Url3 {
  constructor(url) {
    this._url = new URL(url);
  }

  getScheme() {
    if (!this._scheme) {
      this._scheme = this._url.protocol.slice(0, -1);
    }

    return this._scheme;
  }

  getHostName() {
    return this._url.hostname;
  }

  getQueryParams() {
    if (!this._queryParams) {
      this._queryParams = Object.fromEntries(this._url.searchParams);
    }

    return this._queryParams; 
  }

  getQueryParam(key, defaultValue = null) {
    return this._url.searchParams.has(key) ? this._url.searchParams.get(key) : defaultValue;
  }

  toString() {
    return this._url.toString();
  }

  equals(url) {
    return (this.toString() === url.toString());
  }
}
