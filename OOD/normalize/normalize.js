'use strict';
/*
https://ru.hexlet.io/courses/js-object-oriented-design/lessons/fluent-interface/exercise_unit

Реализуйте и экспортируйте по умолчанию функцию normalize()
которая принимает на вход список городов и стран, нормализует их имена, сортирует и группирует по стране.

import normalize from './solution.js';

const countries = [
  { name: 'Miami', country: 'usa' },
  { name: 'samarA', country: '  ruSsiA' },
  { name: 'Moscow ', country: ' Russia' },
];

normalize(countries);
// {
//   russia: [
//     'moscow',
//     'samara',
//   ],
//   usa: [
//     'miami',
//   ],
// }
*/

const normalize = (list) => {
  const regroup = list.reduce((acc, { name: city, country }) => {
    city = city.toLowerCase().trim();
    country = country.toLowerCase().trim();
    acc[country] ? acc[country].add(city) : acc[country] = new Set([city]);
    return acc;
  }, {});

  return Object.keys(regroup).sort()
    .reduce((acc, country) => {
      acc[country] = [...regroup[country]].sort();
      return acc;
    }, {});
};

export default normalize;
