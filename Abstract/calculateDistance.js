/*
https://ru.hexlet.io/courses/js-data-abstraction/lessons/points/exercise_unit


Реализуйте и экспортируйте по умолчанию функцию,
которая находит расстояние между двумя точками на плоскости:
*/
const calculateDistance = (point1, point2) => {
  const [x1, y1] = point1;
  const [x2, y2] = point2;
  return Math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2);
};

export default calculateDistance;
