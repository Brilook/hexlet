/*
https://ru.hexlet.io/courses/js-data-abstraction/lessons/invariants/exercise_unit

Реализуйте абстракцию для работы с рациональными числами включающую в себя следующие функции:

Конструктор makeRational() - принимает на вход числитель и знаменатель, возвращает дробь.
Селектор getNumer() - возвращает числитель
Селектор getDenom() - возвращает знаменатель
Сложение add() - складывает переданные дроби
Вычитание sub() - находит разность между двумя дробями

Не забудьте реализовать нормализацию дробей удобным для вас способом.

const rat1 = makeRational(3, 9);
getNumer(rat1); // 1
getDenom(rat1); // 3

const rat2 = makeRational(10, 3);

const rat3 = add(rat1, rat2);
ratToString(rat3); // '11/3'

const rat4 = sub(rat1, rat2);
ratToString(rat4); // '-3/1'
Подсказки
Действия с дробями
Функция getGcd() находит наибольший общий делитель двух чисел (уже импортирована в модуль)
Функция ratToString() возвращает строковое представление числа (используется для отладки)
*/

import { getGcd } from './serviceFunctions.js';

const makeRational = (a, b) => {
  const div = getGcd(a, b);
  return { numer: a / div, denom: b / div };
};

const getNumer = (ratNum) => ratNum.numer;

const getDenom = (ratNum) => ratNum.denom;

const doOperationForRat = (fractions, callback) => {
  const denoms = fractions.map(getDenom);
  const commonDenom = denoms.reduce((a, b) => a * b);
  const sumNumers = fractions
    .map(fraction => Math.round((getNumer(fraction) * commonDenom) / getDenom(fraction)))
    .reduce(callback);

  return makeRational(sumNumers, commonDenom);
};
const add = (...fractions) => doOperationForRat(fractions, (a, b) => a + b);

const sub = (...fractions) => doOperationForRat(fractions, (a, b) => a - b);

const ratToString = (rat) => `${getNumer(rat)}/${getDenom(rat)}`;

export {
  makeRational,
  getNumer,
  getDenom,
  add,
  sub,
  ratToString,
};
