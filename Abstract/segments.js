/*
https://ru.hexlet.io/courses/js-data-abstraction/lessons/abstractions/exercise_unit

В этом задании подразумевается, что вы уже понимаете принцип построения абстракции
и способны самостоятельно принять решение о том, как она будет реализована.
Напомню, что сделать это можно разными способами и нет одного правильного.


Реализуйте и экспортируйте указанные ниже функции:

makeSegment(). Принимает на вход две точки и возвращает отрезок.
getMidpointOfSegment(). Принимает на вход отрезок и возвращает точку находящуюся на середине отрезка.
getBeginPoint(). Принимает на вход отрезок и возвращает точку начала отрезка.
getEndPoint(). Принимает на вход отрезок и возвращает точку конца отрезка.
*/

const makeDecartPoint = (x, y) => ({ x, y });

const getX = (point) => point.x;

const getY = (point) => point.y;


const makeSegment = (point1, point2) => ({ beginPoint: point1, endPoint: point2 });

const getBeginPoint = (segment) => segment.beginPoint;

const getEndPoint = (segment) => segment.endPoint;

const getMidpointOfSegment = (segment) => {
  const beginPoint = getBeginPoint(segment);
  const endPoint = getEndPoint(segment);
  const x = (getX(beginPoint) + getX(endPoint)) / 2;
  const y = (getY(beginPoint) + getY(endPoint)) / 2;
  return makeDecartPoint(x, y);
};

