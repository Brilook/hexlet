/*
https://ru.hexlet.io/courses/js-data-abstraction/lessons/arrays/exercise_unit

Реализуйте и экспортируйте по умолчанию функцию, которая находит точку по середине между двумя указанными точками.
*/

const point1 = { x: 0, y: 0 };
const point2 = { x: 4, y: 4 };

const getMidpoint = (point1, point2) => {
  return {
    x: (point1.x + point2.x) / 2,
    y: (point1.y + point2.y) / 2,
  }
};

const getMidpoint = (point1, point2) => {
  const x = (point1.x + point2.x) / 2;
  const y = (point1.y + point2.y) / 2;
  return { x, y };
};


const point3 = getMidpoint(point1, point2);