/*
https://ru.hexlet.io/courses/js-data-abstraction/lessons/levels/exercise_unit

Реализуйте абстракцию (набор функций) для работы с прямоугольниками, стороны которого всегда параллельны осям.
Прямоугольник может располагаться в любом месте координатной плоскости.

При такой постановке, достаточно знать только три параметра для однозначного задания прямоугольника на плоскости:
координаты левой верхней точки, ширину и высоту. Зная их, мы всегда можем построить прямоугольник одним единственным способом.

Основной интерфейс:

makeRectangle() (конструктор) – создает прямоугольник.
Принимает параметры: левую-верхнюю точку, ширину и высоту. Ширина и высота – положительные числа.

Селекторы getStartPoint(), getWidth() и getHeight()

containsOrigin() – проверяет, принадлежит ли центр координат прямоугольнику
(не лежит на границе прямоугольника, а находится внутри).
Чтобы в этом убедиться, достаточно проверить, что все вершины прямоугольника лежат в разных квадрантах
(их можно высчитать в момент проверки).

Экспортируйте функции makeRectangle() и containsOrigin().
*/
import { getX, getY, makeDecartPoint } from './serviceFunctions.js';

const makeRectangle = (point, width, heigth) => ({ point, width, heigth });

const getStartPoint = (rectangle) => rectangle.point;

const getWidth = (rectangle) => rectangle.width;

const getHeight = (rectangle) => rectangle.heigth;

const containsOrigin = (rectangle) => {
  const point1 = getStartPoint(rectangle);
  const point2 = makeDecartPoint(
    getX(point1) + getWidth(rectangle),
    getY(point1) - getHeight(rectangle)
  );
  return getX(point1) * getX(point2) < 0 && getY(point1) * getY(point2) < 0;
};
export { makeRectangle, containsOrigin };