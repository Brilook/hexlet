const makeDecartPoint = (x, y) => ({ x, y });

const getX = (point) => point.x;

const getY = (point) => point.y;

const getQuadrant = (point) => {
  const x = getX(point);
  const y = getY(point);
  let result = null;

  if (x > 0 && y > 0) {
    result = 1;
  } else if (x < 0 && y > 0) {
    result = 2;
  } else if (x < 0 && y < 0) {
    result = 3;
  } else if (x > 0 && y < 0) {
    result = 4;
  }

  return result;
};



const getGcd = (a, b) => ((a % b) ? getGcd(b, a % b) : Math.abs(b));

const getDivisor = (...nums) => { // my realisation getGcd for >= 2 nums 
  let max = nums[0];
  let min = nums[0];

  for(let i = 0; i < nums.length; i++) {
    if(nums[i] > max) {
      max = nums[i];
    } else if (nums[i] < min) {
      min = nums[i];
    }
  }
  while(min !== max) {
    max = max - min;
  }
  return max;
};


export {makeDecartPoint, getX, getY, getQuadrant, getDivisor, getGcd };

