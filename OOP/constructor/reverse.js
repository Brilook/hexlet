/*
https://ru.hexlet.io/courses/js-introduction-to-oop/lessons/constructor/exercise_unit

Point.js
Реализуйте и экспортируйте по умолчанию функцию-конструктор Point с двумя свойствами,
представляющими координаты на плоскости x и y.

Segment.js
Реализуйте и экспортируйте по умолчанию функцию-конструктор Segment с двумя свойствами beginPoint и endPoint
и геттеры для извлечения этих свойств: getBeginPoint и getEndPoint.

solution.js
Реализуйте функцию reverse, которая принимает на вход отрезок и
возвращает новый отрезок с точками, добавленными в обратном порядке (begin меняется местами с end).

Точки в результирующем отрезке должны быть копиями (по значению) соответствующих точек исходного отрезка.
То есть они не должны быть ссылкой на один и тот же объект, так как это разные объекты (пускай и с одинаковыми координатами).

Примеры
const beginPoint = new Point(1, 10);
const endPoint = new Point(11, -3);

const segment = new Segment(beginPoint, endPoint);
const reversedSegment = reverse(segment);
*/

import Segment from './Segment.js';
import Point from './Point.js';


const reverse = (segment) => {
  const beginPoint = segment.getBeginPoint();
  const endPoint = segment.getEndPoint();
  const newBeginPoint = new Point(endPoint.getX(), endPoint.getY());
  const newEndPoint = new Point(beginPoint.getX(), beginPoint.getY());

  return new Segment(newBeginPoint, newEndPoint);
};

export default reverse;
