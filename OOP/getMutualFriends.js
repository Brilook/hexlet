/*
https://ru.hexlet.io/courses/js-introduction-to-oop/lessons/encapsulation/exercise_unit

Реализуйте и экспортируйте функцию getMutualFriends(), которая принимает на вход двух пользователей
и возвращает массив состоящий из общих друзей этих пользователей.

Интерфейс абстракции "Пользователь":

user.id – возвращает идентификатор пользователя, по которому можно его отличить от остальных.
user.getFriends() – возвращает список друзей (то есть пользователей).

const user1 = makeUser({
  friends: [
    makeUser({ id: 1 }),
    makeUser({ id: 2 }), // общий друг
  ],
});
const user2 = makeUser({
  friends: [
    makeUser({ id: 2 }), // общий друг
    makeUser({ id: 3 }),
  ],
});

getMutualFriends(user1, user2); // [ { friends: [], id: 2, getFriends: [Function: getFriends] } ]
*/
export const makeUser = ({ id = null, friends = [] } = {}) => ({
  friends,
  id,
  getFriends() {
    return this.friends.slice();
  },
});

const getMutualFriends1 = (user1, user2) => {
  const friends1 = user1.getFriends();
  const friends2 = user2.getFriends();
  return friends1.filter(
    (friend1) => friends2
    .some((friend2) => friend2.id === friend1.id),
  );
};

// for >= 2 users
export const getMutualFriends = (...users) => {
  const friends = users.map(user => user.getFriends())
    .flat()
    .map(friend => friend.id);
  const count = friends.reduce((acc, n) => (acc[n] = (acc[n] || 0) + 1, acc), {});
  const mutualIds = Object.keys(count).filter(key => count[key] === users.length).map(id => Number(id));
  const [user] = users;
  return user.friends.filter(friend => mutualIds.includes(friend.id));
};
