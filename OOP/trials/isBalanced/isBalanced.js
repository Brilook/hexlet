'use strict';
/*
https://ru.hexlet.io/challenges/js_introduction_to_oop_balanced_binary_tree/instance

Особенность структуры двоичного дерева даёт хороший прирост к эффективности при поиске нужного значения.
Для этого нужно, чтобы двоичное дерево было сбалансированным.
То есть необходимо построить дерево так, чтобы общее количество узлов
в левом и правом поддеревьях было примерно одинаковым для любого узла дерева.

Node.js
Реализуйте метод isBalanced(), который проверяет дерево на сбалансированность.
Он возвращает true, если количество узлов в левом и правом поддеревьях каждого узла отличается не более, чем на 2.
В ином случае метод должен вернуть false.
*/

export default class Node {
  constructor(key = null, leftChild = null, rightChild = null) {
    this._key = key;
    this._leftChild = leftChild;
    this._rightChild = rightChild;
  }

  _deepWalk(callback) {
    return callback(this) &&
      (!this.getLeft() || this.getLeft()._deepWalk(callback)) &&
      (!this.getRight() || this.getRight()._deepWalk(callback));
  }
  
  getLeft() {
    return this._leftChild;
  }

  getRight() {
    return this._rightChild;
  }

  getCount() {
    if (!this._count) {
      this._count = 1;
      this._count += this.getLeft()?.getCount() ?? 0;
      this._count += this.getRight()?.getCount() ?? 0;
    }

    return this._count;
  }

  isBalanced() {
    const checkBalance = (node, threshold = 2) => {
      const leftCount = node.getLeft()?.getCount() ?? 0;
      const rightCount = node.getRight()?.getCount() ?? 0;
      return Math.abs(leftCount - rightCount) <= threshold;
    }

    return this._deepWalk(checkBalance);
  }
}
