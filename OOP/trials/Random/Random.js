'use strict';
/*
https://ru.hexlet.io/challenges/js_introduction_to_oop_rand/instance

Реализуйте генератор случайных чисел, представленный классом Random.

Интерфейс объекта включает в себя три функции:
Конструктор. Принимает на вход seed, начальное число генератора псевдослучайных чисел.
getNext() — метод, возвращающий новое случайное число.
reset() — метод, сбрасывающий генератор на начальное значение.
Экспортируйте класс по умолчанию.

Примеры
const seq = new Random(100);
const result1 = seq.getNext();
const result2 = seq.getNext();

result1 !== result2; // true

seq.reset();

const result21 = seq.getNext();
const result22 = seq.getNext();

result1 === result21; // true
result2 === result22; // true
*/

export default class Random {
  constructor(seed) {
    this.seed = seed;
    this.startSeed = seed;
  }

  getNext() {
    const _MULTIPLIER = 1103515245;
    const _INCREMENT = 12345;
    const _MODULUS = Number.MAX_SAFE_INTEGER;

    this.seed = (this.seed * _MULTIPLIER + _INCREMENT) % _MODULUS;
    return this.seed;
  }

  reset() {
    return this.seed = this.startSeed;
  }
}