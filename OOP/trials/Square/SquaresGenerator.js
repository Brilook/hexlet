'use strict';
/*
Реализуйте класс SquaresGenerator со статическим методом generate(),
принимающим два параметра: сторону и количество экземпляров квадрата (по умолчанию 5 штук),
которые нужно создать.
Функция должна вернуть массив из квадратов. Экспортируйте класс по умолчанию.

Пример
const squares = SquaresGenerator.generate(3, 2);
// [new Square(3), new Square(3)];
*/
import Square from './Square.js';

export default class SquaresGenerator {
  static generate(side, numberOfCopies = 5) {
    const result = [];

    for (let count = 0; count < numberOfCopies; count++) {
      result.push(new Square(side));
    }
    return result;
  }
}