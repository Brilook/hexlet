'use strict';
/*
https://ru.hexlet.io/challenges/js_introduction_to_oop_binary_tree_build/instance

Реализуйте и экспортируйте по умолчанию класс, который реализует представление узла.

Класс должен содержать:

Геттер getKey() — возвращает ключ.
Геттеры getLeft(), getRight() — возвращают соответственно левого и правого ребёнка.
Если ребёнок в узле отсутствует, геттер возвращает null.
Метод insert(key) — выполняет добавление узла, формируя правильное двоичное дерево.
Примеры
const tree = new Node();
tree.insert(9);
tree.insert(17);
tree.insert(4);
tree.insert(3);
tree.insert(6);

tree.getKey(); // 9
tree.getLeft().getKey(); // 4
tree.getRight().getKey(); // 17
tree.getLeft().getLeft().getKey(); // 3
tree.getLeft().getRight().getKey(); // 6
*/

export default class Node {  
 
  constructor(key = null, leftChild = null, rightChild = null) {
      this.key = key;
      this.leftChild = leftChild;
      this.rightChild = rightChild;
  }

  getKey() {
    return this.key;
  }

  getLeft() {
    return this.leftChild;
  }
  
  getRight() {
    return this.rightChild;
  }

  insert(initKey) {
    const key = this.getKey();
    if (key === null) {
      this.key = initKey;
    } else if (initKey > key) {
      if (this.rightChild === null) {
        this.rightChild = new Node();
      }
      this.rightChild.insert(initKey);
    } else if (initKey < key) {
      if (this.leftChild === null) {
        this.leftChild = new Node();
      }
      this.leftChild.insert(initKey);
    }
  }
}