'use strict';
/*
https://ru.hexlet.io/challenges/js_introduction_to_oop_binary_tree_aggregation/instance

В данном испытании мы будем использовать двоичное дерево, и выполнять агрегацию данных.

Node.js
Реализуйте следующие методы в классе:

getCount() — возвращает количество узлов в дереве.
getSum() — возвращает сумму всех ключей дерева.
toArray() — возвращает одномерный массив содержащий все ключи.
toString() — возвращает строковое представление дерева.
every(fn) — проверяет, удовлетворяют ли все ключи дерева условию, заданному в передаваемой функции.
some(fn) - проверяет, удовлетворяет ли какой-либо ключ дерева условию, заданному в передаваемой функции.
При обходе дерева нужно использовать порядок слева-направо.
То есть вначале обрабатываем ключ узла, затем ключ левого ребёнка, после чего ключ правого ребёнка.

Примеры
const tree = new Node(9,
    new Node(4,
      new Node(8),
      new Node(6,
        new Node(3),
        new Node(7))),
    new Node(17,
      null,
      new Node(22,
        null,
        new Node(20))));

tree.getCount() // 9
tree.getSum(); // 96
tree.toArray(); // [9, 4, 8, 6, 3, 7, 17, 22, 20]
tree.toString(); // '(9, 4, 8, 6, 3, 7, 17, 22, 20)'

tree.every((key) => key <= 22); // true
tree.every((key) => key < 22); // false
tree.some((key) => key < 4); // true
tree.some((key) => key > 22); // false
*/

export default class Node {  
  constructor(key = null, leftChild = null, rightChild = null) {
      this._key = key;
      this._left = leftChild;
      this._right = rightChild;
  }

  _deepWalk(callback) {
    let proceed = callback(this);
    if (proceed) {
      const children = [this.getLeft(), this.getRight()].filter(child => !!child);
      for (const child of children) {
        proceed = child._deepWalk(callback);
        if(!proceed) break;
      }
    } 
    return proceed
  }

  getKey() {
    return this._key;
  }

  getLeft() {
    return this._left;
  }

  getRight() {
    return this._right;
  }

  getCount() {
    let count = 0;
    this._deepWalk(() => {
      count++;
      return true;
    });
    return count;
  }

  getSum() {
    let sum = 0;
    this._deepWalk(node => {
      sum += node.getKey();
      return true;
    });
    return sum;
  }

  toArray() {
    const result = [];
    this._deepWalk(node => {
      result.push(node.getKey());
      return true;
    });
    return result;
  }

  toString() {
    return `(${this.toArray().join(', ')})`;
  }

  every(fn) {
    let res = true;
    this._deepWalk((node) => {
      if (!fn(node.getKey())) {
        res = false;
      }
      return res;
    });
    return res;
  }

  some(fn) {
    let res = false;
    this._deepWalk((node) => {
      if (fn(node.getKey())) {
        res = true;
      }
      return !res;
    });
    return res;
  }
}