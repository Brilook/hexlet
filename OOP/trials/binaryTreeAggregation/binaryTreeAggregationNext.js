'use strict';
export default class Node {  
  constructor(key = null, leftChild = null, rightChild = null) {
    this._key = key;
    this._leftChild = leftChild;
    this._rightChild = rightChild;

    if (this._leftChild) {
      this._leftChild._parent = this;
    }

    if (this._rightChild) {
      this._rightChild._parent = this;
    }
  }

  _next() {
    const getParentsRightChild = (node) => {
      let res = null;
      const parent = node.getParent();
      if (parent && parent.getRightChild() && (parent.getRightChild() !== node)) {
        res = parent.getRightChild();
      } else if (parent) {
        res = getParentsRightChild(parent);
      }
      return res;
    }
    
    return this.getLeftChild() || this.getRightChild() || getParentsRightChild(this);
  }

  getKey() {
    return this._key;
  }

  getLeftChild() {
    return this._leftChild;
  }

  getRightChild() {
    return this._rightChild;
  }

  getParent() {
    return this._parent;
  }

  getCount() {
    let count = 0;
    let nextNode = this;
    do {
      count++;
      nextNode = nextNode._next();
    } while (nextNode);
    return count;
  }

  getSum() {
    let sum = 0;
    let nextNode = this;
    do {
      sum += nextNode.getKey();
      nextNode = nextNode._next();
    } while (nextNode);
    return sum;
  }

  toArray() {
    const result = [];
    let nextNode = this;
    do {
      result.push(nextNode.getKey());
      nextNode = nextNode._next();
    } while (nextNode);
    return result;
  }

  toString() {
    return `(${this.toArray().join(', ')})`;
  }

  every(fn) {
    let res = null;
    let nextNode = this;

    do {
      res = fn(nextNode.getKey());
      nextNode = nextNode._next();
    } while (res && nextNode);
    return res;
  }

  some(fn) {
    let res = null;
    let nextNode = this;

    do {
      res = fn(nextNode.getKey());
      nextNode = nextNode._next();
    } while (!res && nextNode);
    return res;
  }
}
