'use strict';
/*
https://ru.hexlet.io/courses/js-introduction-to-oop/lessons/to-string/exercise_unit

Реализуйте метод toString(), который преобразует точку к строке в соответствии с примером ниже (1, 10)

const point1 = new Point(1, 10);

console.log(point1.toString()); // => (1, 10)
*/

Point.prototype.getX = function getX() {
  return this.x;
};
Point.prototype.getY = function getY() {
  return this.y;
};

function Point(x, y) {
  this.x = x;
  this.y = y;
};


Point.prototype.toString = function toString() {
  return `(${this.getX()}, ${this.getY()})`;
};

export default Point;
