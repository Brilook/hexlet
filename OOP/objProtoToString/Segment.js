'use strict';
/*
https://ru.hexlet.io/courses/js-introduction-to-oop/lessons/to-string/exercise_unit

Реализуйте метод toString(), который преобразует отрезок к строке в соответствии с примером ниже [(1, 10)]

const point1 = new Point(1, 10);
const point2 = new Point(11, -3);
const segment1 = new Segment(point1, point2);
console.log(segment1.toString()); // => [(1, 10), (11, -3)]

const segment2 = new Segment(point2, point1);
console.log(segment2.toString()); // => [(11, -3), (1, 10)]
*/

Segment.prototype.getBeginPoint = function getBeginPoint() {
  return this.beginPoint;
};

Segment.prototype.getEndPoint = function getEndPoint() {
  return this.endPoint;
};

Segment.prototype.toString = function toString() {
  return `[${this.getBeginPoint()}, ${this.getEndPoint()}]`;
};

function Segment(point1, point2) {
  this.beginPoint = point1;
  this.endPoint = point2;
}

export default Segment;
