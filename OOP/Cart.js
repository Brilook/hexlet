/*
https://ru.hexlet.io/courses/js-introduction-to-oop/lessons/class/exercise_unit

Реализуйте и экспортируйте по умолчанию класс Cart, представляющий из себя покупательскую корзину. Интерфейс:

addItem(item, count) – добавляет в корзину товары и их количество. Товар это объект у которого два свойства: name – имя и price – стоимость.
getItems – возвращает товары в формате [{ item, count }, { item, count }, ...]
getCost – возвращает стоимость корзины. Общая стоимость корзины высчитывается как стоимость всех добавленных товаров с учетом их количества.
getCount – возвращает количество товаров в корзине
Примеры
const cart = new Cart();
cart.addItem({ name: 'car', price: 3 }, 5);
cart.addItem({ name: 'house', price: 10 }, 2);
cart.getItems().length; // 2
cart.getCost(); // 35
*/
class Cart {
  constructor() {
    this.items = [];
  }

  addItem(item, count) {
    this.items.push({ item, count});
  }

  getItems() {
    return this.items;
  }

  getCost() {
    return this.getItems().reduce((cost, good) => cost += good.item.price * good.count, 0)
  }

  getCount() {
    return this.getItems().reduce((count, good) => count += good.count, 0);
  }
};

export default Cart;