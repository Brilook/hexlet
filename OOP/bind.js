/*
https://ru.hexlet.io/courses/js-introduction-to-oop/lessons/bind/exercise_unit

Функции bind(), apply() и call() работают с контекстом и аргументами.
В этом упражнении вы научитесь заменять одну функцию другой для получения функциональности, аналогичной bind().

Реализуйте и экспортируйте по умолчанию функцию, которая ведет себя аналогично встроенной bind(obj, fn).

Аргументы функции:
obj – объект выступающий в роли контекста
fn() – функция привязываемая к контексту

const obj = { number: 5 };
const fn = function fn(number) {
  return number + this.number;
};
const fnWithContext = bind(obj, fn);

// Принимает столько же аргументов сколько и исходная функция
fnWithContext(3); // 8
Примечания
Ограничение: для реализации нельзя пользоваться только встроенной функцией bind().
*/

const bind = function (context, fn) {
  return function (...args) {
    return fn.apply(context, args);
  };
};

export default bind;