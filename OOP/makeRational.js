'use strict';
/*
https://ru.hexlet.io/courses/js-introduction-to-oop/lessons/this/exercise_unit

Подобное задание уже было в курсе по абстракциям с помощью данных.
Теперь мы делаем то же самое, но используя объекты и методы. Нормализацию дробей делать не нужно.

Реализуйте и экспортируйте по умолчанию функцию,
которая создает рациональное число.
Рациональное число должно быть представлено объектом со следующими методами:

Геттер getNumer() - возвращает числитель
Геттер getDenom() - возвращает знаменатель
Геттер toString() - возвращает строковое представление числа
Метод add() - складывает дробь на которой он был вызван с переданной дробью и возвращает новое рациональное число
(не изменяет текущее!)
*/
const make = (numer, denom) => ({
  numer,
  denom,
  getNumer() {
    return this.numer;
  },
  getDenom() {
    return this.denom;
  },
  toString() {
    return `${this.numer}/${this.denom}`;
  },
  add(rat) {
    const newNumer = this.numer * rat.getDenom() + this.denom * rat.getNumer();
    const newDenom = this.denom * rat.getDenom();
    return make(newNumer, newDenom);
  },
});

export default make;

const printer = {
  name: 'Hexlet',
  print(greeting = 'hello') {
    console.log(`${greeting}, ${this.name}`);
  }
};
