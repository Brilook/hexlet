'use strict';
/*
https://ru.hexlet.io/courses/js-introduction-to-oop/lessons/exceptions/exercise_unit

Реализуйте и экспортируйте функцию-обёртку parseJson() для функции JSON.parse(),
которая работает как встроенная. Но в случае если в функцию была передана некорректная json строка,
функция должна выбросить исключение ParseError. Класс ParseError реализовывать не нужно, он уже импортирован.

Пример:
const json = '{ "key": "value" }';
parseJson(json); // { key: 'value' }

const incorrectJson = '{ key": "value" }';
parseJson(incorrectJson); // => ParseError: Invalid JSON string
*/

import ParseError from './ParseError.js';

const isValidJson = (json) => {
  let res = true;
  try {
    JSON.parse(json);
  } catch {
    res = false;
  }
  return res;
};

export const parseJson1 = (json) => {
  if (!isValidJson(json)) {
    throw new ParseError('Invalid JSON string');
  } else {
    return JSON.parse(json);
  }
};

//============
export const parseJson = (json) => {
  try {
    return JSON.parse(json);
  } catch (e) {
    throw new ParseError('Invalid JSON string');
  }
};
