/*
https://ru.hexlet.io/courses/js-objects/lessons/destructuring/exercise_unit

Реализуйте и экспортируйте по умолчанию функцию,
которая принимает на вход список пользователей, извлекает их имена,
сортирует в алфавитном порядке и возвращает отсортированный список имен.
*/

const users = [
    { gender: 'male', birthday: '1973-03-23', name: 'Bronn' },
    { name: 'Reigar', gender: 'male', birthday: '1973-11-03' },
    { name: 'Eiegon', gender: 'male', birthday: '1963-11-03' },
    { name: 'Sansa', gender: 'female', birthday: '2012-11-03' },
  ];

const getSortedNames = (users) => {
    const names = [];
    for (const obj of users) {
        const name = obj.name;
        names.push(name);
    }
    return names.sort();
};

const getSortedNames1 = (users) => {
    const names = [];
    for (const obj of users) {
      const { name } = obj;
      names.push(name);
    }
    return names.sort();
  };
  
  const getSortedNames2 = users => users.map(item => item.name).sort();

  // teachers solution
  const getSortedNamesT = (users) => {
    const names = [];
    for (const { name } of users) {
      names.push(name);
    }
    return names.sort();
  };
  getSortedNames2(users); // ['Bronn', 'Eiegon', 'Reigar', 'Sansa']