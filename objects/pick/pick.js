/*
https://ru.hexlet.io/courses/js-objects/lessons/for-of/exercise_unit

Реализуйте функцию pick, которая извлекает из переданного объекта все элементы по указанным ключам и возвращает новый объект.
Аргументы:
Исходный объект
Массив ключей, по которому необходимо выбирать элементы (ключ и значение) из исходного объекта, и на основе выбранных данных сформировать новый объект
Экспортируйте функцию по умолчанию.
*/
const data = {
    user: 'ubuntu',
    cores: 4,
    os: 'linux',
};

const pick = (data, keys) => {
    const result = {};

    for (const key of keys) {
        if (data.hasOwnProperty(key)) {
            const prop = data[key];
            const tmp = {[key]: prop};
            Object.assign(result, tmp);
        }
    }
    return result;
};

// teachr`s solution

const pick1 = (data, keys) => {
    const result = {};
    const dataKeys = Object.keys(data);
  
    for (const key of keys) {
      if (dataKeys.includes(key)) {
        result[key] = data[key];
      }
    }
  
    return result;
  };

// pick(data, ['user']); // { user: 'ubuntu' }
pick1(data, ['user', 'os']); // { user: 'ubuntu', os: 'linux' }
// pick(data, []); // {}
// pick(data, ['none']); // {}