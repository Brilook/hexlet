/*
https://ru.hexlet.io/courses/js-objects/lessons/using/exercise_unit


Реализуйте и экспортируйте по умолчанию функцию, которая извлекает из объекта (который может быть любой глубины вложенности)
значение по указанным ключам.
Аргументы:
Исходный объект
Цепочка ключей (массив), по которой ведётся поиск значения
В случае, когда добраться до значения невозможно, возвращается null.
*/

// import { has } from 'lodash';
const { has } = require('lodash');

const data = {
  user: 'ubuntu',
  hosts: {
    0: {
      name: 'web1',
    },
    1: {
      name: 'web2',
      null: 3,
      active: false,
    },
  },
};


const getValue = (obj, keys) => {
  let res = obj;
  for (const pathItem of keys) {
    if (res.hasOwnProperty(pathItem)) {
      res = res[pathItem];
    } else {
      res = null;
      break;
    };
  }
  return res;
};

const getValue1 = (object, keys) => {
  let result = null;
  if (has(object, [...keys])) {
    result = object;
    for (const key of keys) {
      result = result[key];
    }
  };
  return result;
};

// getValue1(data, ['hosts', 1, 'name']);
getValue1(data, [null]);
// 
// getValue(data, ['hosts', 1, null]);
// getValue(data, [['hosts'], [1], ['name']]);
