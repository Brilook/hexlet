/*
https://ru.hexlet.io/challenges/js_objects_to_roman/instance

Для записи цифр римляне использовали буквы латинского алфафита: I, V, X, L, C, D, M. Например:

1 обозначалась с помощью буквы I
10 с помощью Х
7 с помощью VII
Число 2020 в римской записи — это MMXX (2000 = MM, 20 = XX).

solution.js
Реализуйте и экспортируйте функцию toRoman(), которая переводит арабские числа в римские.
Функция принимает на вход целое число в диапазоне от 1 до 3000, а возвращает строку с римским представлением этого числа.

Реализуйте и экспортируйте функцию toArabic(), которая переводит число в римской записи в число, записанное арабскими цифрами.
Если переданное римское число не корректно, то функция должна вернуть значение false.
*/
const arabicVSroman = {
    1: 'I',
    5: 'V',
    10: 'X',
    50: 'L',
    100: 'C',
    500: 'D',
    1000: 'M',

};
const mirror = (obj) => {
 const result = {}; // without variable? reduce!!
  Object.keys(obj).forEach(key => result[obj[key]] = key);
  return result;
};


const toRoman = (arabicNum) => {
    let num = arabicNum;
    const arabices = Object.keys(arabicVSroman).map(Number).reverse();
    const divisionByCategories = [];
    const arabicSize = arabices.length;
    for (let index = 0; index < arabicSize; index++) {
        const cat = Math.trunc(num / arabices[index]);
        num -= arabices[index] * cat;
        divisionByCategories.push([arabices[index], cat]);
        if (!num) break;
    }
    for (let i = 0; i < divisionByCategories.length; i++) {
        const thisItem = divisionByCategories[i];
        let prevItem = divisionByCategories[i - 1];
        if (thisItem[1] === 4 && prevItem[1] === 0) {
            thisItem[1] = 1;
            prevItem[1] = 1;
            const copyLast = [arabices[i]];
            divisionByCategories.splice(i - 1, 2, thisItem, prevItem);
            copyLast[1] = 0;
            divisionByCategories.splice(i + 1, 0, copyLast);
        } else if (thisItem[1] === 4 && prevItem[1] === 1) {
            thisItem[1] = 1;
            prevItem = divisionByCategories[i - 2];
            prevItem[1] = 1;
            divisionByCategories.splice(i - 2, 3, thisItem, prevItem);
        }
    }
    let romanNum = '';
    divisionByCategories.forEach(([key, value]) => romanNum += arabicVSroman[key].repeat(value));
    return romanNum;
};

const toArabic1 = (romanNum) => {
  const getKeyByValue = (value, object = arabicVSroman) => Number(Object.keys(object).find(key => object[key] === value));
  const romanNumArr = romanNum.split('');
  const romanNumSize = romanNumArr.length;
  let arabicNum = 0;
  for (let i = 0; i < romanNumSize; i++) {
    const thisItem = getKeyByValue(romanNumArr[i]);
    const nextItem = getKeyByValue(romanNumArr[i + 1]);
    if (thisItem < nextItem) {
      arabicNum += nextItem - thisItem;
      i++;
    } else {
      arabicNum += thisItem;
    }
  }
  if (romanNum !== toRoman(arabicNum)) arabicNum = false;
  return arabicNum;
};

const toArabic = (romanNum) => {
  const romanVSarabic = mirror(arabicVSroman);
  const keysRoman = Object.keys(romanVSarabic);
  let arabicNum = 0;




  
  return romanNum !== toRoman(arabicNum) ? false : arabicNum;
}
// toArabic('I'); // 1
// toArabic('LIX'); // 59
toArabic('MMM'); // 3000
toArabic('IIII'); // false
toArabic('VX'); // false
toArabic('VV'); // false

// toRoman(1) //.toEqual('I');
// toRoman(2) //.toEqual('II');
// toRoman(4) //.toEqual('IV');
// toRoman(5) //.toEqual('V');
// toRoman(6) //.toEqual('VI');
// toRoman(7) //.toEqual('VII');
// toRoman(8) //.toEqual('VIII');
// toRoman(9) //.toEqual('IX');
// toRoman(10) //.toEqual('X');
// toRoman(11) //.toEqual('XI'); 
// toRoman(21) //.toEqual('XXI');
// toRoman(20) //.toEqual('XX');
// toRoman(27) //.toEqual('XXVII');
// toRoman(48) //.toEqual('XLVIII');
// toRoman(49) //.toEqual('XLIX');
// toRoman(59) //.toEqual('LIX');
// toRoman(93) //.toEqual('XCIII');
// toRoman(141) //.toEqual('CXLI');
// toRoman(163) //.toEqual('CLXIII');
// toRoman(402) //.toEqual('CDII');
// toRoman(575) //.toEqual('DLXXV');
// toRoman(911) //.toEqual('CMXI');
// toRoman(1024) //.toEqual('MXXIV');
// toRoman(3000) //.toEqual('MMM');


// // не уверен нужна ли такая проверка!
// //  разобраться с передаваемыми в ф-цию параметрами !!!!!
// const checkValidRomanNum = () => {
//     const romanSambols = romanNumArr.every(sambol => Object.values(arabicVSroman).includes(sambol));

//     const inArow = (romanNumArr, maxReplays = 2) => {
//         const romanNumSize = romanNumArr.length;

//         for (let i = 1; i < romanNumSize; i++) {
//             if (romanNumArr[i] === romanNumArr[i - 1]) maxReplays--;
//         }
//         return maxReplays >= 0;
//     };
//     return romanSambols && inArow(romanNumArr);
// };

// teachers solution
// const numerals = {
//     M: 1000,
//     CM: 900,
//     D: 500,
//     CD: 400,
//     C: 100,
//     XC: 90,
//     L: 50,
//     XL: 40,
//     X: 10,
//     IX: 9,
//     V: 5,
//     IV: 4,
//     I: 1,
//   };
  
//   const sortedNumerals = Object.entries(numerals)
//     .sort(([, arabic1], [, arabic2]) => Math.sign(arabic2 - arabic1));
  
//   export const toRoman = (number) => {
//     let digit = number;
//     const result = [];
//     for (const [roman, arabic] of sortedNumerals) {
//       const repetitionsCount = Math.floor(digit / arabic);
//       digit -= repetitionsCount * arabic;
//       result.push(roman.repeat(repetitionsCount));
//     }
  
//     return result.join('');
//   };
  
//   export const toArabic = (romanNumber) => {
//     let result = 0;
//     let currentLine = romanNumber;
//     for (const [roman, arabic] of sortedNumerals) {
//       while (currentLine.indexOf(roman) === 0) {
//         result += arabic;
//         currentLine = currentLine.slice(roman.length);
//       }
//     }
  
//     if (toRoman(result) !== romanNumber) {
//       return false;
//     }
  
//     return result;
//   };


// Mykolka solution validRoman
const romanToArabic = Object.keys(arabicVSroman).reduce((acc, curr) => {
  acc[arabicVSroman[curr]] = curr;
  return acc;
}, {});

function validateRoman(roman) {
  const romanOrdered = Object.keys(romanToArabic);
  const romanDiff = (a, b) => romanOrdered.indexOf(a) - romanOrdered.indexOf(b);
  const isPowerOfTen = char => Number.isInteger(Math.log10(romanToArabic[char]));

  let result = true;
  let sameCount = 1;
  for (let i = 1; i < roman.length; ++i) {
    if (roman[i - 1] === roman[i]) {
      sameCount++;
      if (!isPowerOfTen(roman[i]) || sameCount > 3) {
        result = false;
        break;
      }
    } else {
      sameCount = 0;
    }
    
    if (romanDiff(roman[i - 1], roman[i]) < 0) {
      if (sameCount) {
        result = false;
        break;
      } else if (!isPowerOfTen(roman[i - 1])) {
        result = false;
        break;
      } else {
        let indexDiff = romanDiff(roman[i], roman[i - 1]);
        if (indexDiff < 1 || indexDiff > 2) {
          result = false;
          break;
        } else if (i < roman.length - 1 && roman[i - 1] == roman[i + 1]) {
          result = false;
          break;
        }
      } 
    }
  }

  return result;
};

// function test(arg) {
//   console.log(`${arg} : ${validateRoman(arg)}`);
// }

// test('I');
// test('II');
// test('III');
// test('IIII');
// test('V');
// test('IV');
// test('IVI');
// test('IIV');
// test('IX');
// test('XI');
// test('XXI');
// test('XIX');
// test('XIIX');
// test('VV');
// console.log();
// end Mykolka solution

