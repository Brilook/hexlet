/*
https://ru.hexlet.io/challenges/js_objects_scrabble/instance

Реализуйте и экспортируйте по умолчанию функцию-предикат, которая принимает на вход два параметра:
набор символов в нижнем регистре (строку) и слово, и проверяет, можно ли из переданного набора составить это слово.
В результате вызова функция возвращает true или false.

При проверке учитывается количество символов, нужных для составления слова, и не учитывается их регистр.
*/

const createListSumbolCount = (string) => {
    const str = string.toLowerCase();
    const symbolStatistics = {};
    for (const symbol of str) {
        if (symbolStatistics.hasOwnProperty(symbol)) {
            symbolStatistics[symbol] += 1;
        } else {
            symbolStatistics[symbol] = 1;
        }
    }
    return symbolStatistics;
};

const createListSumbolCount1 = (string) => {
  const symbols = string.toLowerCase().split('');
  return symbols.reduce((symbolStatistics, symbol) => {
    symbolStatistics[symbol] = (symbolStatistics[symbol] || 0) + 1;
    return symbolStatistics;
  },{})
}

const scrabble = (string, word) => {
    const stringList = createListSumbolCount(string);
    const wordList = createListSumbolCount(word);
    const keys = Object.keys(wordList);
    let result = true;
    for (const key of keys) {
        if (stringList[key] === undefined || stringList[key] < wordList[key]) {
            result = false;
            break;
        }
    }
    return result;
};

// scrabble('rkqodlw', 'world'); // true
// scrabble('avj', 'java'); // false
// scrabble('avjafff', 'java'); // true
// scrabble('', 'hexlet'); // false
// scrabble('scriptingjava', 'JavaScript'); // true
scrabble('katas', 'steak') // false


// teachers solution

// import { get } from 'lodash';

// // BEGIN
// // Эту функцию можно заменить функцией countBy
// // из библиотеки lodash https://lodash.com/docs/#countBy
// const countByChars = (str) => {
//   const chars = {};

//   for (const char of str) {
//     const count = get(chars, char, 0);
//     chars[char] = count + 1;
//   }

//   return chars;
// };

// export default (str, word) => {
//   const countsChars = countByChars(str);

//   for (const char of word.toLowerCase()) {
//     const count = get(countsChars, char, 0);
//     if (count === 0) {
//       return false;
//     }
//     countsChars[char] -= 1;
//   }

//   return true;
// };