/*
https://ru.hexlet.io/challenges/js_objects_to_rna/instance


ДНК и РНК это последовательности нуклеотидов.
Четыре нуклеотида в ДНК это аденин (A), цитозин (C), гуанин (G) и тимин (T).
Четыре нуклеотида в РНК это аденин (A), цитозин (C), гуанин (G) и урацил (U).
Цепь РНК составляется на основе цепи ДНК последовательной заменой каждого нуклеотида:
G -> C
C -> G
T -> A
A -> U
dnaToRna.js
Реализуйте и экспортируйте функцию по умолчанию, которая принимает на вход цепь ДНК и возвращает соответствующую цепь РНК (совершает транскрипцию РНК).
Если во входном параметре нет ни одного нуклеотида (т.е. передана пустая строка), то функция должна вернуть пустую строку.
Если в переданной цепи ДНК встретится "незнакомый" нуклеотид (не являющийся одним из четырех перечисленных выше), то функция должна вернуть null.
*/

const dnaToRna = (dna) => {
  let rna = '';
  const replacementRule = {
    G: 'C',
    C: 'G',
    T: 'A',
    A: 'U',
  };
  const dnaArr = dna.split('');
  for (const key of dnaArr) {
    if (!replacementRule[key]) {
      rna = null;
      break;
    }
    rna += replacementRule[key];
  }
  return rna;
};

const dnaToRna1 = (dna) => {
  let rna = '';
  const replacementRule = {
    G: 'C',
    C: 'G',
    T: 'A',
    A: 'U',
  };
  for (const key of dna) {
    if (!replacementRule.hasOwnProperty(key)) {
      rna = null;
      break;
    }
    rna += replacementRule[key];
  }
  return rna;
};

// teacher's solution
const map = {
  G: 'C',
  C: 'G',
  T: 'A',
  A: 'U',
};

const dnaToRnaT = (dna) => {
  const rna = [];

  for (const nucleotide of dna) {
    if (!map.hasOwnProperty(nucleotide)) {
      return null;
    }

    rna.push(map[nucleotide]);
  }

  return rna.join('');
};

// dnaToRna('ACGTGGTCTTAA'); // 'UGCACCAGAAUU'
dnaToRnaT('CCGTA'); // 'GGCAU'
// dnaToRna(''); // ''
// dnaToRna('ACNTG'); // null