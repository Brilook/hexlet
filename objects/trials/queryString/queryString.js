/*
https://ru.hexlet.io/challenges/js_objects_query_string/instance

Query String (строка запроса) - часть адреса страницы в интернете содержащая константы и их значения.
Она начинается после вопросительного знака и идет до конца адреса.

Пример:
# query string: page=5
https://ru.hexlet.io/blog?page=5
Если параметров несколько, то они отделяются амперсандом &:

# query string: page=5&per=10
https://ru.hexlet.io/blog?per=10&page=5
buildQueryString.js

Реализуйте и экспортируйте функцию по умолчанию,
которая принимает на вход список параметров и возвращает сформированный query string из этих параметров:

Имена параметров в выходной строке должны располагаться в алфавитном порядке (то есть их нужно отсортировать).

подезная ссылка по работе с url https://attacomsian.com/blog/javascript-url-search-params 
*/

const createQueryString = data => {
    const result = [];
    const keys = Object.keys(data).sort();
    for (const key of keys) {
        result.push(`${key}=${data[key]}`);
    }
    return result.join('&');
};

const createQueryString1 = (data, result = []) => {
    const keys = Object.keys(data).sort();
    keys.forEach(key => result.push(`${key}=${data[key]}`));
    return result.join('&');
};

//  давай добавим ещё один параметр. Например, надо ли отдавать подробную или краткую версию с этого API:

const createQueryString2 = (data, params, result = []) => {
    const dataKeys = Object.keys(data);
    if (!params) params = dataKeys;
    const keys = dataKeys.filter(key => params.includes(key)).sort();
    keys.forEach(key => result.push(`${key}=${data[key]}`));
    return result.join('&');
};


// create QS with filter params and coding illegal symbols
const createQueryString3 = (data, params) => {
    const dataKeys = Object.keys(data);
    if (!params) params = dataKeys;

    return dataKeys
        .filter(key => params.includes(key))
        .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(data[key])}`)
        .sort()
        .join('&')};

const filterParams = ['id', 'show_details']
const qs = createQueryString3({
    per: 10,
    page: 1,
    id: 'cb&df&hj39f392nfj239dj',
    show_details: true,
}, filterParams);

const url = `http://example.com?${qs}`

const getValueOfParameter = (url, parameter) => {
const urlObj = new URL(url);
const params = new URLSearchParams(urlObj.search);
return params.get(parameter);
};
getValueOfParameter(url, 'id');






// const queryStr = createQueryString1({
//     per: 10,
//     page: 1,
//     id: 'cb&df&hj39f392nfj239dj',
//     show_details: true,
// }); 
// page=1&per=10
// bqs({ param: 'all', param1: true });
// param=all&param1=true

