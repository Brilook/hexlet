/*
https://ru.hexlet.io/courses/js-objects/lessons/syntax/exercise_unit

Реализуйте и экспортируйте по умолчанию функцию, которая возвращает ассоциативный массив, соответствующий json из файла example.json в этом упражнении. Всё, что вам нужно сделать — посмотреть на содержимое example.json и руками сформировать объект аналогичной структуры.

Подсказки
Обратите внимание на сходство JSON и объекта. Оно не случайно. JSON является представлением ассоциативного массива в текстовом виде.
*/

const getJsonFileData = () => {
    const json = {
        common: {
            files: ['src/objects.js'],
        },
        config: {
            outdir: '/dist',
        }
    };
    return json;
};

// teacher`s solution

// export default () => ({
//     common: {
//       files: ['src/objects.js'],
//     },
//     config: {
//       outdir: '/dist',
//     },
//   });

// subtask
// функцию fruitsCounter, подсчитывающую количество каждого вида фруктов в сумке.
// То есть функция на основе массива должна сформировать объект, каждый ключ которого — это определённый фрукт, а значение — количество

const bag = [
    'apple', 'banana', 'pear',
    'apricot', 'apple', 'banana',
    'apple', 'orange', 'pear',
  ];

  const fruitsCounter = (bag) => {
      const statistics = {};

      for (elem of bag) {
          if (statistics.hasOwnProperty(elem)) {
              statistics[elem] += 1;
          } else {
              statistics[elem] = 1;
          }
      }
      return statistics;
  };


  fruitsCounter(bag);
