// /*
// https://ru.hexlet.io/courses/js-objects/lessons/hash-table/exercise_unit

// Реализуйте и экспортируйте набор функций, для работы со словарём, построенным на хеш-таблице.
// Для простоты, наша реализация не поддерживает разрешение коллизий.

// По сути в этом задании надо реализовать объекты.
// По понятным причинам использовать объекты для создания объектов нельзя.
// Представьте что в языке объектов нет и мы их хотим добавить.

// make() — создаёт новый словарь
// set(map, key, value) — устанавливает в словарь значение по ключу. Работает и для создания и для изменения.
// Функция возвращает true, если удалось установить значение. При возникновении коллизии, функция никак не меняет словарь и возвращает false.

// get(map, key, defaultValue = null) — возвращает значение указанного ключа. Параметр defaultValue — значение, которое функция возвращает,
// если в словаре нет ключа (по умолчанию равно null). При возникновении коллизии функция также возвращает значение по умолчанию.

// Функции set и get принимают первым параметром словарь. Передача идёт по ссылке, поэтому set может изменить его напрямую.

// Примеры
// import { make, set, get } from './map.js';

// const map = make();
// let result = get(map, 'key');
// console.log(result); // => null

// result = get(map, 'key', 'default_value');
// console.log(result); // => "default_value"

// set(map, 'key2', 'value2');
// result = get(map, 'key2');
// console.log(result); // => "value2"

// Подсказки
// Для внутреннего представления словаря, используйте массив, где индекс содержит хеш записи, а значение — key и value (их можно упаковать в массив).
const crc32 = require('crc-32');

function make() {
    const _store = [];

    const hash = (key) => {
        const hash = crc32.str(key);
        return Math.abs(hash % 1000);
    };

    function findMatchingIndex(list, key) {
        for (let i = 0; i < Object.keys(list).length; i++) {
            if (list[i][key]) return i;
        }
    };

    return {
        set(key, value) {
            const index = hash(key);
            if (!_store[index]) {
                _store[index] = [{ [key]: value }];
            } else {
                const list = _store[index];
                const matchingIndex = findMatchingIndex(list, key);

                if (matchingIndex >= 0) {
                    list[matchingIndex] = { [key]: value };
                } else {
                    list.push({ [key]: value });
                }
            }
        },
        get(key, result = null) {
            const index = hash(key);
            if (_store[index]) {
                const list = _store[index];
                const matchingIndex = findMatchingIndex(list, key);
                if (matchingIndex >= 0) result = list[matchingIndex][key];
            }
            return result;
        },

        delete(key) {
            const index = hash(key);
            if (_store[index]) {
                const list = _store[index];
                const matchingIndex = findMatchingIndex(list, key);
                if (matchingIndex >= 0) {
                    list.splice(matchingIndex, 1);
                }
            }
        }
    }
};

const newDictionary = make();
newDictionary.set('v1', '111');
newDictionary.set('v11', '1111');
newDictionary.set('v2', '222');
newDictionary.set('v3', '333');
newDictionary.set('vv3', '3v3v3');
newDictionary.set('v4', '444');
newDictionary.set('v5', '555');
newDictionary.set('v6', '666');
newDictionary.set('v7', '777');
newDictionary.set('v8', '888');
newDictionary.set('v9', '8888');
newDictionary.set('v10', '8888');
newDictionary.set('v11', '8888');
newDictionary.set('v12', '8888');
newDictionary.set('v13', '8888');
newDictionary.set('v14', '8888');
newDictionary.set('v15', '8888');
newDictionary.set('v16', '8888');
newDictionary.set('v17', '8888');
newDictionary.set('v18', '8888');
newDictionary.set('v19', '8888');
newDictionary.set('v20', '8888');
newDictionary.set('aaaaa0.462031558722291', 'vvv');
newDictionary.set('sdfsdf', '23423424');
newDictionary.delete('v3');
newDictionary.delete('v9');
newDictionary.get('v3');
newDictionary.set('v3', '333');
newDictionary.set('v3', '333');
newDictionary.get('v3');


// newDictionary.set('key2', 'value2');
// newDictionary.get('key2');

// newDictionary.set( 'key2', 'another value');
// newDictionary.get('key2');


// по мнению hexlet здесь должна быть коллизия(я так думаю)
newDictionary.set('aaaaa0.462031558722291', 'vvv') // true
newDictionary.set('aaaaa0.462031558722291', 'VVV') // true

newDictionary.set('aaaaa0.0585754039730588', 'boom!') // fALSE
newDictionary.get('aaaaa0.462031558722291') // 'vvv'


newDictionary.set('aaaaa0.462031558722291', 'wop');
newDictionary.get('aaaaa0.462031558722291');
// newDictionary.get('sdfsdf');



// special for hexlet

// @ts-check
/* eslint-disable no-param-reassign */

// import crc32 from 'crc-32';

// // BEGIN (write your solution here)
// const make = () => [];

// const createIndex = (key) => {
//   const hash = crc32.str(key);
//   return Math.abs(hash % 1000);
// };

// function findMatchingIndex(list, key) {
//   for (let i = 0; i < list.length; i += 1) {
//     if (list[i][0] === key) return i;
//   }
// }


// function set(map, key, value) {
//   const index = createIndex(key);
//   let res = false;
//   if (!map[index]) {
//     map[index] = [
//       [key, value],
//     ];
//     res = true;
//   } else if (map[index][0][0] === key) {
//     map[index][0][1] = value;
//     res = true;
//   }
//   return res;
// }

// function get(map, key, result = null) {
//   const index = createIndex(key);
//   if (map[index]) {
//     const list = map[index];
//     const matchingIndex = findMatchingIndex(list, key);
//     if (matchingIndex >= 0) result = list[matchingIndex][1];
//   }
//   return result;
// }

// export { make, set, get };
// // END