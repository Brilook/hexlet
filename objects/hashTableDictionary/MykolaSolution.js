const tableHeight = 20;


class HashTable {
    constructor() {
        this._storage = [].fill(null, 0, tableHeight);
    }
    
    static _hash(str) {
        return (str.charCodeAt(0) + str.charCodeAt(str.length - 1) + str.length) % tableHeight;
    }

    _getNodeByKey(key) {
        const keyHash = HashTable._hash(key);
        let node = this._storage[keyHash];
        while (node && node.key !== key) {
            node = node.next;
        }
        return node;
    }

    getValueByKey(key) {
        const node = this._getNodeByKey(key);
        return node ? node.value : null;
    }

    setValueByKey(value, key) {
        const existingNode = this._getNodeByKey(key);
        if (existingNode) {
            existingNode.value = value;
        } else {
            const keyHash = HashTable._hash(key);
            const node = this._storage[keyHash];
            const newNode = {
                key,
                value,
                next: node
            }
    
            this._storage[keyHash] = newNode;        
        }
    }
};


const table = new HashTable();
console.log('table: ', table);
table.setValueByKey('aa', 'a');
table.setValueByKey('bb', 'b');
table.setValueByKey('cc', 'c');
table.setValueByKey('cc2', 'c');
table.setValueByKey('aabb', 'ab');
table.setValueByKey('aabbcc', 'abc');

console.log(table.getValueByKey('a'));
console.log(table.getValueByKey('b'));
console.log(table.getValueByKey('c'));
console.log(table.getValueByKey('ab'));
console.log(table.getValueByKey('abc'));
console.log(table.getValueByKey('z'));