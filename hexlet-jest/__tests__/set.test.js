// import _ from 'lodash';
const _ = require('lodash');

// import getFunction from '../functions.js';
const getFunction = require('../src/functionals.js');


const set = getFunction();


// // traying
// let object = { a: [{ b: { c: 3 } }] };

// let copY = Object.assign({}, object);
// ['x', '0', 'y', 'z'].reduce((acc, item) => {
//     return (acc[item] = {});
//   }, copY);
//   const path = [];
//   ['x', '0', 'y', 'z'].forEach(i => path.push([i]));
//   //   copY.(['x', '0', 'y', 'z'].join('.')) = 5;
// set(object, ['x', '0', 'y', 'z'], 5);
// console.log('object:', object);
// // =========

// BEGIN (write your solution here)
describe('test set:', () => {
    let object;
    let copy;
    beforeEach(() => {
        object = {
            a: [{
                b: {
                    c: 3
                }
            }]
        };
        copy = _.cloneDeep(object);
    });

    test('value change deep', () => {
        set(object, 'a[0].b.c', 4);
        copy.a[0].b.c = 4;
        expect(object).toEqual(copy);
    });

    test('add property', () => {
        set(object, ['x', '0', 'y', 'z'], 5);
        copy.x = [{
            y: {
                z: 5
            }
        }];
        expect(object).toEqual(copy);
    });
});
// END