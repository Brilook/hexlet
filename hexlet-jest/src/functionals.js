const { _ } = require('lodash');

const functions = {
    wrong1: (obj, path, value) => {
        _.set(obj, path, value);
        obj.key = 'value';
        return obj;
    },
    wrong2: (obj, path, value) => {
        obj[path] = value;
        return obj;
    },
    right1: _.set,
  };
  
  module.exports =  () => {
    const name = process.env.FUNCTION_VERSION || 'right1';
    return functions[name];
  };
  