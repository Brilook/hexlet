/**
https://ru.hexlet.io/courses/js-functions/lessons/first-class-citizen/exercise_unit

Реализуйте внутреннюю функцию takeLast, которая возвращает последние n символов строки в обратном порядке.
Количество символов передаётся в takeLast вторым параметром. Если передаётся пустая строка или строка меньше необходимой длины,
функция должна вернуть null.
 */
const run = (text) => {
    
    const takeLast = (str, size) => {
        const length = str.length;
        const resLength = length - size;
        let result = '';

        if (length >= size) {
            for (let i = length - 1; i >= resLength; i -= 1) {
                result += str[i];
            }
        }
        return result || null;
    };
    return takeLast(text, 4);
};


// teachers solution

const run1 = (text) => {
    // BEGIN
    const takeLast1 = (str, length) => {
      if (str.length < length) {
        return null;
      }
  
      const result = [];
      for (let i = str.length - 1; result.length < length; i -= 1) {
        result.push(str[i]);
      }
  
      return result.join('');
    };
    // END
  
    return takeLast1(text, 4);
  };

run(''); // null
run('cb'); // null
run('power'); // rewo
run('hexlet'); // telx