/*
https://ru.hexlet.io/courses/js-functions/lessons/reduce/exercise_unit

Реализуйте и экспортируйте по умолчанию функцию, которая принимает на вход список пользователей и возвращает объект,
в котором ключ это год рождения, а значение это количество мужчин, родившихся в этот год.
*/

const users = [{
        name: 'Bronn',
        gender: 'male',
        birthday: '1973-03-23'
    },
    {
        name: 'Reigar',
        gender: 'male',
        birthday: '1973-11-03'
    },
    {
        name: 'Eiegon',
        gender: 'male',
        birthday: '1963-11-03'
    },
    {
        name: 'Sansa',
        gender: 'female',
        birthday: '2012-11-03'
    },
    {
        name: 'Jon',
        gender: 'male',
        birthday: '1980-11-03'
    },
    {
        name: 'Robb',
        gender: 'male',
        birthday: '1980-05-14'
    },
    {
        name: 'Tisha',
        gender: 'female',
        birthday: '2012-11-03'
    },
    {
        name: 'Rick',
        gender: 'male',
        birthday: '2012-11-03'
    },
    {
        name: 'Joffrey',
        gender: 'male',
        birthday: '1999-11-03'
    },
    {
        name: 'Edd',
        gender: 'male',
        birthday: '1973-11-03'
    },
];

const getMenCountByYear = (users) => {
    const result = users.reduce((acc, user) => {
        const year = user.birthday.slice(0, 4);
        const isMale = user.gender === 'male';
        if (isMale && !acc.hasOwnProperty(year)) {
            acc[year] = 1;
        } else if (isMale) {
            acc[year] += 1;
        }
        return acc;
    }, {});
    return result;
};

const getMenCountByYear1 = (users) => {
    const men = users.filter(({ gender }) => gender === 'male');
    const years = men.map(({ birthday }) => birthday.slice(0, 4));
    return years.reduce((acc, year) => {
        acc[year] = (acc[year] || 0) + 1;
        return acc;
    }, {});
};


getMenCountByYear1(users);
// {
//   1973: 3,
//   1963: 1,
//   1980: 2,
//   2012: 1,
//   1999: 1,
// };