/*
https://ru.hexlet.io/courses/js-functions/lessons/map/exercise_unit

Реализуйте и экспортируйте по умолчанию функцию, которая принимает на вход список пользователей и возвращает плоский список их детей.
Дети каждого пользователя хранятся в виде массива в ключе children.
*/

const users = [
    {
      name: 'Tirion',
      children: [
        { name: 'Mira', birthday: '1983-03-23' },
      ],
    },
    { name: 'Bronn', children: [] },
    {
      name: 'Sam',
      children: [
        { name: 'Aria', birthday: '2012-11-03' },
        { name: 'Keit', birthday: '1933-05-14' },
      ],
    },
    {
      name: 'Rob',
      children: [
        { name: 'Tisha', birthday: '2012-11-03' },
      ],
    },
  ];


  
  const getChildren1 = (users) => {
      const allChildren = users.map(({ children }) => children);
      return allChildren.flat();
  }

  const getChildren = (users) => users.map(({children}) => children).flat();

  const getChildren2 = (users) => {
    const children = [];
    for (const user of users) {
      children.push(...user.children);
    }
    return children;
  };

  const getChildren3 = (users) => {
    const children = [];
    users.forEach(user => children.push(...user.children));
    return children;
  };


  
  getChildren3(users);
  // [
  //   { name: 'Mira', birthday: '1983-03-23' },
  //   { name: 'Aria', birthday: '2012-11-03' },
  //   { name: 'Keit', birthday: '1933-05-14' },
  //   { name: 'Tisha', birthday: '2012-11-03' },
  // ];