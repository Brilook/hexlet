/**
 https://ru.hexlet.io/courses/js-functions/lessons/high-order-functions/exercise_unit

 Реализуйте функцию takeOldest, которая принимает на вход список пользователей и возвращает самых взрослых.
 Количество возвращаемых пользователей задается вторым параметром, который по умолчанию равен единице. Экспортируйте данную функцию по умолчанию.
 */
// import { sortBy } from 'lodash';
const { sortBy } = require('lodash');

const users = [
    { name: 'Tirion', birthday: 'Nov 19, 1988' },
    { name: 'Sam', birthday: 'Nov 22, 1999' },
    { name: 'Rob', birthday: 'Jan 11, 1975' },
    { name: 'Sansa', birthday: 'Mar 20, 2001' },
    { name: 'Tisha', birthday: 'Feb 27, 1992' },
    { name: 'Chris', birthday: 'Dec 25, 1995' },
];

const takeOldest1 = (users, quantity = 1) => {
    const sortUsers = users.sort((user1, user2) => {
        const date1 = Date.parse(user1.birthday);
        const date2 = Date.parse(user2.birthday);
        if (date1 === date2) {
            return 0;
        }
        return date1 > date2 ? 1 : -1;
    }); 
    return sortUsers.slice(0, quantity);
};

const takeOldest3 = (users, quantity = 1) => {
    const birthdaysInt = users.map(({ birthday }) => Date.parse(birthday));
    const oldest = [];
    for (let i = 0; i < quantity; i++) {
        const indexMaxEl = birthdaysInt.indexOf(Math.min(...birthdaysInt));
        birthdaysInt[indexMaxEl] = Infinity;
        oldest.push(users[indexMaxEl]);
    }
    return oldest;
};

// with lodash
const takeOldest2 = (users, quantity = 1) => {    
    const sortUsers = sortBy(users, ({ birthday }) => Date.parse(birthday));
    return sortUsers.slice(0, quantity);
};


takeOldest3(users, 5);
// [
//   { name: 'Rob', birthday: 'Jan 11, 1975' },
// ];