/*
https://ru.hexlet.io/courses/js-functions/lessons/paradigms/exercise_unit

Реализуйте и экспортируйте по умолчанию функцию, которая принимает изображение в виде двумерного массива и увеличивает его в два раза.
*/

const {
    times
} = require("lodash");


const arr = [
    ['*', '*', '*', '*'],
    ['*', ' ', ' ', '*'],
    ['*', ' ', ' ', '*'],
    ['*', '*', '*', '*'],
];
// ****
// *  *
// *  *
// ****
const duplicateArr1 = (array, times = 2) => array.reduce((acc, current) => acc.concat(Array(times).fill(current)), []);
const duplicateArr = (items) => items.map((item) => [item, item]).flat();

const enlargeArrayImage = (arr) => {
    const duplicateToLength = arr.map(duplicateArr);
    return duplicateArr(duplicateToLength);
};



enlargeArrayImage(arr);
// ********
// ********
// **    **
// **    **
// **    **
// **    **
// ********
// ********