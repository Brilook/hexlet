/*
https://ru.hexlet.io/courses/js-functions/lessons/spread-operator/exercise_unit

Реализуйте функцию, которая конвертирует даты в массив человеко-читаемых строк на английском языке.
Каждая из дат представлена массивом [2001, 10, 18], в котором первый элемент — это год, второй — месяц, и третий — число.
Функция на вход должна принимать любое количество параметров. Если в функцию ничего не было передано, она должна вернуть пустой массив.
Экспортируйте функцию по умолчанию.

Подсказки
Для работы с датами воспользуйтесь объектом new Date() и его методом toDateString()
*/

const convert = (...dates) => {
    const result = [];
    for (const date of dates) {
      result.push(new Date(...date).toDateString());
    }
    return result;
};

const convert1 = (...dates) => dates.map(date => new Date(...date).toDateString());


convert1(); // []
// convert([1993, 3, 24]); // ['Sat Apr 24 1993']
convert1([1993, 3, 24], [1997, 8, 12], [2001, 10, 18]); // ['Sat Apr 24 1993', 'Fri Sep 12 1997', 'Sun Nov 18 2001']