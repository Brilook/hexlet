/*
https://ru.hexlet.io/challenges/js_functions_find_nearest/instance

Реализуйте и экспортируйте по умолчанию функцию, которая принимает на вход массив чисел и искомое число.
Задача функции — найти в массиве ближайшее число к искомому и вернуть его индекс в массиве.
Если в массиве содержится несколько чисел, одновременно являющихся ближайшими к искомому числу,
то возвращается наименьший из индексов ближайших чисел.
*/
const findIndexOfNearest1 = (data, search) => { // slowed "Трешняк вообще!!! ТАК ДЕЛАТЬ НЕЛЬЗЯ!!"
    let result = null;
    if (data.length) {
        let nearest = [search, search];
        while (!result) {
            nearest.forEach(item => {
                if (data.includes(item)) {
                    result = data.indexOf(item);
                };
            });
            nearest = [nearest[0] += 1, nearest[1] -= 1]
        }
    }
    return result;
};

const findIndexOfNearest = (data, search) => {
    const margins = data.map((item) => Math.abs(item - search));
    const indxMinMargin = margins.indexOf(Math.min(...margins));
    return indxMinMargin >= 0 ? indxMinMargin : null;
};


// teachers solution
const findIndexOfNearest2 = (numbers, num) => {
    if (numbers.length === 0) {
        return null;
    }

    const diffs = numbers.map((element) => Math.abs(num - element));

    return diffs.reduce((index, diff, currentIndex) => (
        diff < diffs[index] ? currentIndex : index
    ), 0);
};





findIndexOfNearest([], 2); // null
// findIndexOfNearest([15, 10, 3, 4], 0); // 2

// findIndexOfNearest([10], 0); // 0
findIndexOfNearest2([7, 5, 3, 2], 4); // 1
findIndexOfNearest([7, 5, 4, 4, 3], 4); // 2
// findIndexOfNearest([15, 10], 12) // 1);
findIndexOfNearest([15, 10, 3, 4], 0) // 2