/*
https://ru.hexlet.io/challenges/js_functions_vertical_histogram/instance

Реализуйте и экспортируйте по умолчанию функцию, которая выводит на экран вертикальную гистограмму.
Функция принимает на вход количество бросков кубика и функцию, которая имитирует бросок игральной кости (её реализовывать не нужно).
Вызов этой функции генерирует значение от 1 до 6, что соответствует одной из граней игральной кости.

Гистограмма содержит столбцы, каждому из которых соответствует грань игральной кости и количество выпадений этой грани.
Результаты отображаются графически (с помощью символов #) и в виде процентного значения от общего количества бросков,
за исключением случаев, когда количество равно 0 (нулю).

Дополнительные условия:
Процентные значения должны быть прижаты влево относительно столбца.
Значения сторон игральной кости должны быть посредине столбца.
Столбцы между собой разделены пробелом
Количество секций в столбце (высота столбца) должно соответствовать количеству выпадений каждой из сторон игральной кости.
*/
const {
    _,
    result
} = require('lodash');

const MinDiece = 1;
const MaxDiece = 6;
// const rollDie = (min = MinDiece, max = MaxDiece) => _.random(min, max);
const rollDie = (min, max) => _.random(min, max);
const Brush = '###';
const ColumnWidth = Brush.length + 1;

const displayHistogram = (roundsCount, rollDie) => {
    const throwsValue = [...Array(roundsCount)].map(() => rollDie(MinDiece, MaxDiece));
    const valueCount = throwsValue.reduce((acc, num) => { 
        acc[num] = (acc[num] || 0) + 1;
        return acc;
    }, {});

    const createHistogram = (dices) => {
        const pushSymbol = (table, row, col, symbol) => {
            while (table[row].length < (col - MinDiece) * ColumnWidth) {
                table[row] += ' ';
            }
            table[row] += symbol;
        };

        const maxCount = Math.max(...(Object.values(dices)));
        const output = Array(maxCount + 1).fill('');
        for (let fringe = MinDiece; fringe <= MaxDiece; fringe++) {
            const fringeCount = dices[fringe];
            for (let i = 0; i < fringeCount; ++i) {
                pushSymbol(output, output.length - i - 1, fringe, Brush);
            }
            if (fringeCount) pushSymbol(output, output.length - fringeCount - 1, fringe, `${Math.round(fringeCount / roundsCount * 100)}%`);
        }

        output.forEach(row => console.log(row));
        const colums = [...Array(MaxDiece).keys()].map( i => ++i).join('   ');
        const line = '-'.repeat(colums.length + 2);
        console.log(`${line}\n ${colums}`);
    };
    return createHistogram(valueCount);
};





displayHistogram(32, rollDie);
// =>                 28%
//                    ###
//                    ###
//            19%     ###
//            ### 16% ### 16%
//    13%     ### ### ### ###
//    ### 9%  ### ### ### ###
//    ### ### ### ### ### ###
//    ### ### ### ### ### ###
//    ### ### ### ### ### ###
//    -----------------------
//     1   2   3   4   5   6

// displayHistogram(13, rollDie);
// =>                 31% 31%
//                    ### ###
//        15%     15% ### ###
//        ### 8%  ### ### ###
//        ### ### ### ### ###
//    -----------------------
//     1   2   3   4   5   6
