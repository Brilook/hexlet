/*
https://ru.hexlet.io/challenges/js_functions_bar_chart

Реализуйте и экспортируйте по умолчанию функцию, которая выводит на экран столбчатую диаграмму.
Функция принимает в качестве параметра последовательность чисел, длина которой равна количеству столбцов диаграммы.
Размер диаграммы по вертикали должен определяться входными данными.
*/
const barChart1 = (numbers) => { // Time: 3.166 s
  const maxPosNum = Math.max(...numbers);
  const minNegNum = Math.min(...numbers) < 0 ? Math.abs(Math.min(...numbers)) : 0;
  const hight = maxPosNum + minNegNum;
  const { length } = numbers;
  const output = [...Array(hight)].map(() => Array(length).fill(' '));

  for (let column = 0; column < length; column++) {
    const num = numbers[column];
    const symbol = num > 0 ? '*' : '#';
    const startInd = num > 0 ? maxPosNum - 1 : maxPosNum - 1 + Math.abs(num);
    const endInd = num > 0 ? startInd - num : startInd + num;

    for (let row = startInd; row > endInd; row--) {
      output[row][column] = symbol;
    }
  }
  return output.forEach(row => console.log(row.join('')));
};

const barChart = (numbers) => { // Time: ~ 2 s
  let max = min = numbers[0];
  for ( let i = 0; i < numbers.length; i++) {
    const num = numbers[i];
    if (num > max) max = num;
    if (num < min) min = num;
  }
  max = max > 0 ? max : 0;
  min = min < 0 ? min : 0;
  const height = max + Math.abs(min);
  const { length } = numbers;
  const output = [...Array(height)].map(() => Array(length).fill(' '));

  for (let column = 0; column < length; column++) {
    const num = numbers[column];
    const symbol = num > 0 ? '*' : '#';
    const startIdx = num > 0 ? max - 1 : max - 1 + Math.abs(num);
    const endIdx = num > 0 ? startIdx - num : startIdx + num;

    for (let row = startIdx; row > endIdx; row--) {
      output[row][column] = symbol;
    }
  }
  return output.forEach(row => console.log(row.join('')));
};

// barChart([8,8,8,8,8,7,6,6,2,6,8,6,4,6,5,2,6,7,3,7]);

/*
 *****     *        
 ******    *      * *
 ******** *** *  ** *
 ******** *** ** ** *
 ******** ****** ** *
 ******** ****** ****
 ********************
 ********************
 */


barChart([5, 10, 5, 3, 7]);
// =>  *   
//     *   
//     *   
//     *  *
//     *  *
//    **  *
//    **  *
//    **  *
//    **  *
//    **  *
//      ## 
//      ## 
//      ## 
//      #  
//      #  

// barChart([5, -2, 10, 6, 1, 2, 6, 4, 8, 1, -1, 7, 3, -5, 5]);
// =>   *            
//      *            
//      *     *      
//      *     *  *   
//      **  * *  *   
//    * **  * *  *  *
//    * **  ***  *  *
//    * **  ***  ** *
//    * ** ****  ** *
//    * ******** ** *
//     #        #  # 
//     #           # 
//                 # 
//                 # 
//                 # 