/**
https://ru.hexlet.io/challenges/js_functions_IPv6_validator/instance

Реализуйте функцию-предикат isValidIPv6, которая проверяет IPv6-адреса (адреса шестой версии интернет протокола) на корректность.
Функция принимает на вход строку с адресом IPv6 и возвращает true, если адрес корректный,
а в противном случае false. Экспортируйте функцию по умолчанию.

Дополнительные условия:

Работа функции не зависит от регистра символов.
Ведущие нули в группах цифр необязательны.
Самая длинная последовательность групп нулей, например, :0:0:0: может быть заменена на два двоеточия ::. Такую замену можно произвести только один раз.
Одна группа нулей :0: не может быть заменена на ::.
 */



const isValidValue = (components) => components.every(component => Number(`0x${component}`) >= 0);
const isValidSize = (components) => components.every(component => component.length < 5);
// a можно их обьеденить  ток название придумать))
const isValidValueSize = (data) => data.every(elem => Number(`0x${elem}`) >= 0 && elem.length < 5);

const joingElements = (data, separator = ':') => data.map(item => item.split(separator)).flat();
const fullingIP = (data) => data.map(el => (el === '' ? '0000' : el));
const areEdgeSymbolsValid = (data, separator = ':') => data.every(item => (
    item === '' || item[0] !== separator && item[item.length - 1] !== separator));


const isValidIPv6 = (IP) => {
    let res = true;
    const cutingIP = IP.split('::');
    const itemsIP = joingElements(cutingIP);
    if (cutingIP.length > 2) {
        res = false;
    } else if (cutingIP.length === 2 && areEdgeSymbolsValid(cutingIP)) {
        const fullIP = fullingIP(itemsIP);
        res = fullIP.length < 7 && isValidSize(fullIP) && isValidValue(fullIP);
    } else res = itemsIP.length === 8  && isValidSize(itemsIP) && isValidValue(itemsIP);

    return res;
};


const isValidIPv61 = (IP) => {
    let res = true;
    const cutingIP = IP.split('::');
    const itemsIP = joingElements(cutingIP);
    if (cutingIP.length > 2) {
        res = false;
    } else if (cutingIP.length === 2 && areEdgeSymbolsValid(cutingIP)) {
        const fullIP = fullingIP(itemsIP);
        res = fullIP.length < 7 && isValidValueSize(fullIP);
    } else res = itemsIP.length === 8 && isValidValueSize(itemsIP);

    return res;
};

// Mykolkas solution

function getComponentValidator(acceptEmpty) {
    return component => (component.length < 5) && (Number(`0x${component}`) >= 0) && (acceptEmpty || component != '');
}

const isSubcomponentValid = subcomponent => (subcomponent === '' ||
    subcomponent[0] !== ':' &&
    subcomponent[subcomponent.length - 1] !== ':');

function isValidIPv6M(IP) {
    let res = true;
    const subcomponents = IP.split('::');
    const components = subcomponents.map(item => item.split(':')).flat();
    
    if (subcomponents.length > 2) {
        res = false;
    } else if (subcomponents.length === 2) {
        res = subcomponents.every(isSubcomponentValid);
        res = res && components.length < 7;
        res = res && components.every(getComponentValidator(true));
    } else {
        res = components.length === 8 && components.every(getComponentValidator(false));
    }

    return res;
}



isValidIPv61('10:d3:2d06:24:400c:5ee0:be:3d'); // true
isValidIPv61('0B0:0F09:7f05:e2F3:0D:0:e0:7000'); // true
isValidIPv61('000::B36:3C:00F0:7:937'); // true
isValidIPv61('::'); // true
isValidIPv61('::1'); // true 
isValidIPv61('1::1'); // true



// isValidIPv6M('2607:G8B0:4010:801::1004'); // false
// isValidIPv6M('1001:208:67:4f00:e3::2c6:0'); // false
// isValidIPv6M('2.001::'); // false
isValidIPv61(':1::1'); // false !!!
isValidIPv61('1::1:'); // false !!
isValidIPv61('9f8:0:69S0:9:9:d9a:672:f90d'); // false !!
// isValidIPv6M('2001::0:64::2'); // false
// isValidIPv6M('5c03:0:a::b825:d690:4ce0:2831:acf0'); // false
isValidIPv61('2001'); // false !!
isValidIPv61('d:0:4:a004:3b96:10b0:10:800:15'); // false !!