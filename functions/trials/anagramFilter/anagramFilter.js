/*
https://ru.hexlet.io/challenges/js_functions_filter_anagrams/instance

Реализуйте и экспортируйте по умолчанию функцию, которая находит все анаграммы слова.
Функция принимает исходное слово и список для проверки (массив), а возвращает массив всех анаграмм.
Если в списке слов отсутствуют анаграммы, то возвращается пустой массив.
*/
const filterAnagrams = (originalWord, checklist) => {
    const getCountLetters = (str) => str.split('').reduce((acc, letter) => {
      acc[letter] = (acc[letter] || 0) + 1;
      return acc;
    }, {});

    const originalCount = getCountLetters(originalWord);
    const originalKeys = Object.keys(originalCount);
    const { length } = originalKeys;
    return checklist.filter((word) => {
      const checkCount = getCountLetters(word);
      const checkKeys = Object.keys(checkCount);
      return originalKeys.every((key) => length === checkKeys.length
        && checkCount[key] === originalCount[key]);
    });
  };



  // teacher solution !!!!бля так просто!!!!!

  const filterAnagrams1 = (word, words) => {
    const normalize = (str) => str.split('').sort().join('');
    const normal = normalize(word);
  
    return words.filter((item) => normalize(item) === normal);
  };

// filterAnagrams('abba', ['aabb', 'abcd', 'bbaa', 'dada']);
// ['aabb', 'bbaa']

filterAnagrams1('racer', ['crazer', 'carer', 'racar', 'caers', 'racer']);
// ['carer', 'racer']

filterAnagrams('laser', ['lazing', 'lazy',  'lacer']);
// []