/*
https://ru.hexlet.io/challenges/js_functions_dictionaries_merge/instance

Реализуйте и экспортируйте по умолчанию функцию, которая объединяет несколько словарей (объектов) в один общий словарь.
Функция принимает любое количество аргументов и возвращает результат в виде объекта, в котором каждый ключ содержит массив уникальных значений.
Элементы в массиве располагаются в том порядке, в котором они появляются во входящих словарях.
*/

const merge = (...connectionElements) => {
  const res = connectionElements.reduce((acc, elem) => {
    const keysElem = Object.keys(elem);
    keysElem.forEach((item) => {
      if (acc[item]) {
        if (!acc[item].includes(elem[item])) acc[item].push(elem[item]);
      } else acc[item] = [elem[item]];

    });
    return acc;
  }, {});
  return res;
}


// merge({}, {}, {});
// {}

// merge({ a: 1, b: 2 }, { a: 3 });
// // { a: [1, 3], b: [2] }

merge({
  a: 1,
  b: 2,
  c: 3
}, {}, {
  a: 3,
  b: 2,
  d: 5
}, {
  a: 6
}, {
  b: 4,
  c: 3,
  d: 2
}, {
  e: 9
}, );
// { a: [1, 3, 6], b: [2, 4], c: [3], d: [5, 2], e: [9] }