/*
https://ru.hexlet.io/challenges/js_functions_sea_battle/instance

Реализуйте и экспортируйте по умолчанию функцию,
которая принимает на вход поле боя в виде квадратного двумерного массива из нулей и единиц.
Ноль — пустая ячейка, единица — часть корабля. Функция должна вернуть количество кораблей на поле боя.

В отличии от классической игры "Морской бой", в данном варианте корабли могут изгибаться.
*/

const calcShipsCount = (matrix) => {
    let numberOfShips = 0;
    const length = matrix.length;
    const getMatrixValue = (row, col) => (col >= 0) && (row >= 0) && (col < length) && (row < length) ? matrix[row][col] : 0;
    for (let i = 0; i < length; i += 1) {
        for (let j = 0; j < length; j += 1) {
          // const nextHorizontal = getMatrixValue(i, j + 1);
          // const nextVertical = getMatrixValue(i + 1, j);
          // const diagonalUpRight = getMatrixValue(i - 1, j + 1);
          // const diagonalDownLeft = getMatrixValue(i + 1, j - 1);

          if (matrix[i][j] === 1
                && getMatrixValue(i, j + 1) === 0
                && getMatrixValue(i + 1, j) === 0
                && getMatrixValue(i - 1, j + 1) !== 1
                && getMatrixValue(i + 1, j - 1) !== 1) {
              numberOfShips += 1;
            }
        }
    }
    return numberOfShips;
};

const calcShipsCount2 = (matrix) => {
  let numberOfShips = 0;
  const length = matrix.length;
  for (let i = 0; i < length; i += 1) {
      for (let j = 0; j < length; j += 1) {
          const nextHorizontal = (j < length - 1) ? matrix[i][j + 1] : 0;
          const nextVertical = (i < length - 1) ? matrix[i + 1][j] : 0;
          const diagonalUpRight = (i !== 0 && j !== length - 1) ? matrix[i - 1][j + 1] : 0;
          const diagonalDownLeft = (i !== length - 1 && j !== 0) ? matrix[i + 1][j - 1] : 0;

          if (matrix[i][j] === 1
              && nextHorizontal === 0
              && nextVertical === 0
              && diagonalUpRight !== 1
              && diagonalDownLeft !== 1) {
            numberOfShips += 1;
          }
      }
  }
  return numberOfShips;
};
calcShipsCount([
    [0, 1, 0, 0, 1, 1, 0],
    [0, 1, 0, 0, 0, 1, 0],
    [0, 1, 1, 0, 0, 1, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [1, 1, 0, 0, 1, 0, 0],
    [1, 0, 0, 0, 1, 1, 1],
    [1, 0, 1, 0, 0, 0, 0],
]); // 5. test 6 


calcShipsCount([
    [0, 1, 0, 0, 0, 0],
    [0, 1, 0, 0, 0, 1],
    [0, 0, 0, 1, 0, 0],
    [0, 1, 1, 1, 0, 1],
    [0, 0, 0, 0, 0, 1],
    [1, 1, 0, 1, 0, 0],
]); // 6




calcShipsCount([]); // 0
// calcShipsCount([
//   [0, 1, 0, 0, 0, 0],
//   [0, 1, 0, 0, 0, 1],
//   [0, 0, 0, 1, 0, 0],
//   [0, 1, 1, 1, 0, 1],
//   [0, 0, 0, 0, 0, 1],
//   [1, 1, 0, 1, 0, 0],
// ]); // 6

// techrs solution - сложновато !!
const { _ } = require('lodash');

// Вычисляем общее количество палуб всех кораблей на поле
const getShipsDecks = (battleField) => battleField
  .reduce((acc, line) => acc + line.filter((item) => item === 1).length, 0);

const calcShipsCount1 = (battleField) => {
  // Корабли разделяются на две группы: линейные (battleship) и изогнутые.
  // Изогнутый корабль состоит из двух линейных, примыкающих друг к другу.
  // В нашем алгоритме подсчёт палуб линейных кораблей прост,
  // подсчёт палуб изогнутых кораблей потребует корректировки.

  // Начальное количество линейных кораблей
  let battleshipsCount = 0;
  // Начальное количество палуб линейных кораблей
  let battleshipsDecks = 0;
  // Объединяем исходное и повёрнутое поле
  // и считаем на них общее количество линейных кораблей.
  // Однопалубные корабли пропускаем.
  [battleField, _.zip(...battleField)].forEach((field) => field.forEach((line) => {
    const battleships = line.join('').split(0).filter((ship) => ship.length > 1);
    battleshipsCount += battleships.length;
    battleshipsDecks += battleships.reduce((acc, ship) => acc + ship.length, 0);
  }));
  // Алгоритм учитывает каждый изогнутый корабль дважды, например,
  // у 4-палубного корабля он посчитает 5 палуб (3 по горизонтали + 2 по вертикали).
  // Учитываем это при подсчёте итогового количества кораблей.
  const correctionCount = getShipsDecks(battleField) - battleshipsDecks;
  return battleshipsCount + correctionCount;
};

calcShipsCount1([
    [0, 1, 0, 0, 0, 0],
    [0, 1, 0, 0, 0, 1],
    [0, 0, 0, 1, 0, 0],
    [0, 1, 1, 1, 0, 1],
    [0, 0, 0, 0, 0, 1],
    [1, 1, 0, 1, 0, 0],
]); // 6
