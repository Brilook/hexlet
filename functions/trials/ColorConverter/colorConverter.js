/*
https://ru.hexlet.io/challenges/js_functions_rgb_hex_conversion/instance

Для задания цветов в HTML и CSS используются числа в шестнадцатеричной системе счисления.
Чтобы не возникало путаницы в определении системы счисления, перед шестнадцатеричным числом ставят символ решетки #, например, #135278.
Обозначение цвета (rrggbb) разбивается на три составляющие, где первые два символа обозначают красную компоненту цвета,
два средних — зеленую, а два последних — синюю.
Таким образом каждый из трех цветов — красный, зеленый и синий — может принимать значения от 00 до FF в шестнадцатеричной системе исчисления.

При работе с цветами часто нужно получить отдельные значения красного, зеленого и синего (RGB) компонентов цвета в десятичной системе исчисления
и наоборот. Реализуйте и экспортируйте функции rgbToHex и hexToRgb, которые возвращают соответствующие представление цвета.
*/
const chunk = (data, lengthChunk) => {
    const result = [];
    for (let i = 0; i < data.length; i += lengthChunk) {
        result.push(data.slice(i, i + lengthChunk));
    }
    return result;
};

const hexToRgb = (hex) => {
    const colorsValue = chunk(hex.slice(1), 2);
    const res = {
        r: 0,
        g: 0,
        b: 0
    };
    return colorsValue.reduce((acc, item, index) => {
        const nameColors = Object.keys(acc);
        acc[nameColors[index]] = parseInt(item, 16);
        return acc;
    }, res);
};


const hexToRgb1 = (hex) => 
    ({
        r: parseInt(hex[1] + hex[2], 16),
        g: parseInt(hex[3] + hex[4], 16),
        b: parseInt(hex[5] + hex[6], 16),
    });

const rgbToHex = (...nums) => {// линтер ругается на присваивание в стрелочной ф-ции
    let res = '#';
    nums.forEach((item) => res += item.toString(16).padStart(2, '0'));
    return res;
};

const rgbToHex1 = (...nums) => {
    const hexValue = nums.map((item) => item.toString(16).padStart(2, '0'));
    return `#${hexValue.join('')}`;
};

hexToRgb2('#24ab00'); // { r: 36, g: 171, b: 0 }

rgbToHex(36, 171, 0); // '#24ab00'