/*
https://ru.hexlet.io/challenges/js_functions_horisontal_histogram/instance

Игральная кость - шестигранный кубик, который бросается несколько раз.
Гистограмма - это графическое представление данных в виде столбцов или колонок.

Реализуйте и экспортируйте по умолчанию функцию, которая выводит на экран горизонтальную гистограмму.
Функция принимает на вход количество бросков кубика и функцию, которая имитирует бросок игральной кости (её реализовывать не нужно).
Вызов этой функции генерирует значение от 1 до 6, что соответствует одной из граней игральной кости.

Гистограмма содержит строки, каждой из которых соответствует грань игральной кости и количество выпадений этой грани.
Результаты отображаются графически (с помощью символов #) и в виде числового значения, за исключением случаев, когда количество равно 0 (нулю).
*/
// const roundsCount = 125;
const rollDie = (min = 1, max = 6) => {
    let rand = min + Math.random() * (max + 1 - min);
    return Math.floor(rand);
};

const showHistogram = (dices, min = 1, max = 6) => {
    const fringeKeys = Object.keys(dices);
    for (let i = min; i <= max; i++) {
        const histoData = fringeKeys.includes(`${i}`) ? `${'#'.repeat(dices[i])} ${dices[i]}` : '';
        console.log(`${i}|${histoData}`);
    }
};

const play = (roundsCount, rollDie) => {
    const throwsValue = [...Array(roundsCount)].map(() => rollDie());

    const fringeCount = throwsValue.reduce((acc, fringe) => {
        acc[fringe] = (acc[fringe] || 0) + 1;
        return acc;
    }, {});

    return showHistogram(fringeCount);
};


// ------------------------------
const showHistogram1 = (obj) => Object.values(obj).map((item, index) => {
    const histoData = item ? `${'#'.repeat(item)} ${item}` : '';
    console.log(`${index + 1}|${histoData}`);
});

const play1 = (roundsCount, rollDie) => {
    const throwsValue = [];
    while (roundsCount) {
        throwsValue.push(rollDie());
        roundsCount -= 1;
    }
    const fringeCountObj = throwsValue.reduce((acc, fringe) => {
        if (acc.hasOwnProperty(fringe)) acc[fringe] += 1;
        return acc;
    }, {
        1: 0,
        2: 0,
        3: 0,
        4: 0,
        5: 0,
        6: 0,
    });
    return showHistogram(fringeCountObj);
};


// play(roundsCount, rollDie);
play(10, rollDie);
// => 1|####################### 23
//    2|################## 18
//    3|############# 13
//    4|#################### 20
//    5|############ 12
//    6|############## 14




// play(13, rollDie);
// => 1|
//    2|## 2
//    3|# 1
//    4|## 2
//    5|#### 4
//    6|#### 4

//teacher solution   'не правильно работает
// import _ from 'lodash';
// const { _ } = require('lodash');

// // BEGIN
// const playT = (roundsCount, rollDie) => {
//   const bar = '#';
//   const numbers = _.times(roundsCount, rollDie);// помоему сдесь партак или кокретно в rollDie
//   const sides = _.range(1, 7);

//   const lines = sides.map((side) => {
//     const count = numbers.filter((number) => number === side).length;
//     const displayCount = count !== 0 ? ` ${count}` : '';
//     return `${side}|${bar.repeat(count)}${displayCount}`;
//   });
//   const str = lines.join('\n');

//   console.log(str);
// };
// // END

// playT(100, rollDie);
// => 1|
//    2|## 2
//    3|# 1
//    4|## 2
//    5|#### 4
//    6|#### 4