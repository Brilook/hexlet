/*
https://ru.hexlet.io/challenges/js_functions_ip_converter/instance

Реализуйте и экспортируйте функции ipToInt и intToIp,
которые преобразовывают представление IP-адреса из десятичного формата с точками в 32-битное число в десятичной форме и обратно.
Функция ipToInt принимает на вход строку и должна возвращать число.
А функция intToIp наоборот: принимает на вход число, а возвращает строку.
*/

const ipToInt = (IP) => IP.split('.').reverse().reduce((acc, item, index) => {
    acc += item * Math.pow(256, index);
    return acc;
}, 0);


const intToIp = (num) => {
    const res = [];
    let number = num;
    for (let i = 3; i >= 0; i--) {
        const remaind = number % Math.pow(256, i);
        const quotient = number / Math.pow(256, i);
        res.push(Math.floor(quotient));
        number = remaind;
    }
    return res.join('.');
}

ipToInt('128.32.10.1'); // 2149583361
// ipToInt('0.0.0.0'); // 0
// ipToInt('255.255.255.255'); // 4294967295

intToIp(2149583361); // '128.32.10.1'
// intToIp(0); // '0.0.0.0'
// intToIp(4294967295); // '255.255.255.255'


// teacher solution

 const {
    chunk
} = require("lodash");

const ipToIntT = (ip) => {
  const ipInHex = ip
    .split('.')
    .map((octet) => Number(octet).toString(16).padStart(2, 0))
    .join('');

  return parseInt(ipInHex, 16);
};

const intToIpT = (int) => {
  const ipInHex = int.toString(16).padStart(8, 0);

  return chunk(ipInHex, 2)
    .map((octet) => parseInt(octet.join(''), 16))
    .join('.');
};

ipToIntT('128.32.10.1');
intToIpT(2149583361);