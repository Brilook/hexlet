/*
https://ru.hexlet.io/challenges/js_functions_nrzi/instance

Реализуйте функцию, которая принимает строку в виде графического представления линейного сигнала и возвращает строку с бинарным кодом. 
*/
const nrzi = (signal) => {
    const unit1 = '|¯';
    const unit2 = '|_';
    let binary = [];
    if (signal.length > 1) {
      binary = (signal
        .split(unit1)
        .join('1'))
        .split(unit2)
        .join('1')
        .split('');
      binary.forEach((element, index) => {
        if (element !== '1') binary[index] = '0';
      });
    }
    return binary.join('');
  };

// techer solution
  const nrziT = (str) => str
  .split('')
  .map((e, i, arr) => {
    if (e === '|') return '';
    return arr[i - 1] === '|' ? 1 : 0;
  })
  .join('');






const signal1 = '_|¯|____|¯|__|¯¯¯';
nrzi1(signal1); // '011000110100'

const signal2 = '|¯|___|¯¯¯¯¯|___|¯|_|¯';
nrzi(signal2); // '110010000100111'

const signal3 = '¯|___|¯¯¯¯¯|___|¯|_|¯';
nrzi(signal3); // '010010000100111'

const signal4 = '';
nrzi(signal4); // ''

const signal5 = '|';
nrzi(signal5); // ''