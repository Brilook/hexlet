/*
https://ru.hexlet.io/courses/js-functions/lessons/signals/exercise_unit

Реализуйте и экспортируйте по умолчанию функцию, которая принимает на вход список емейлов, а возвращает количество емейлов,
расположенных на каждом бесплатном домене. Список бесплатных доменов хранится в константе freeEmailDomains.
*/
const freeEmailDomains = [
  'gmail.com',
  'yandex.ru',
  'hotmail.com',
];



const emails = [
  'info@gmail.com',
  'info@yandex.ru',
  'info@hotmail.com',
  'mk@host.com',
  'support@hexlet.io',
  'key@yandex.ru',
  'sergey@gmail.com',
  'vovan@gmail.com',
  'vovan@hotmail.com',
];

const getFreeDomainsCount1 = (emails, fmd = freeEmailDomains) => {
  const domains = emails.map(email => email.slice(email.indexOf('@') + 1));
  const freeEmails = domains.filter(item => fmd.some(elem => elem === item));

  return freeEmails.reduce((acc, domain) => {
    if (!acc.hasOwnProperty(domain)) {
      acc[domain] = 1;
    } else {
      acc[domain] += 1;
    }
    return acc;
  }, {});
};

// new version
const getFreeDomainsCount = (emails, fmd = freeEmailDomains) => emails.map(email => email.slice(email.indexOf('@') + 1))
  .filter(item => fmd.includes(item))
  .reduce((acc, domain) => {
    acc[domain] = (acc[domain] || 0) + 1;
    return acc;
  }, {});

const getFreeDomainsCount3 = (emails, fmd = freeEmailDomains) => {
  const domains = emails.map(email => email.slice(email.indexOf('@') + 1));
  fmd = new Set(fmd);
  return domains.reduce((acc, domain) => {
    if (fmd.has(domain)) {
      acc[domain] = (acc[domain] || 0) + 1;
    }
    return acc;
  }, {});
}


const getFreeDomainsCount2 = (emails, fmd = freeEmailDomains) => {
  const domains = emails.map(email => email.slice(email.indexOf('@') + 1));
  const freeDomainsCount = fmd.reduce((acc, domain) => {
    acc[domain] = 0;
    return acc;
  }, {});

  domains.forEach(domain => {
    if (freeDomainsCount[domain] >= 0) {
      freeDomainsCount[domain] += 1;
    }
  });
  return freeDomainsCount;
}




// teachers solution
const {
  get
} = require('lodash');

const getFreeDomainsCountT = (emails) => emails
  .map((email) => {
    const [, domain] = email.split('@');
    return domain;
  })
  .filter((domain) => freeEmailDomains.includes(domain))
  .reduce((acc, domain) => {
    const count = get(acc, domain, 0) + 1; // 'get' is function from lodash
    return {
      ...acc,
      [domain]: count
    };
  }, {});


getFreeDomainsCount3(emails);
// {
//   'gmail.com': 3,
//   'yandex.ru': 2,
//   'hotmail.com': 2,
// };