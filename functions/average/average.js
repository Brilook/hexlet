/*
https://ru.hexlet.io/courses/js-functions/lessons/rest-operator/exercise_unit

Реализуйте и экспортируйте по умолчанию функцию, которая возвращает среднее арифметическое всех переданных аргументов.
Если функции не передать ни одного аргумента, то она должна вернуть null.

average(0); // 0
average(0, 10); // 5
average(-3, 4, 2, 10); // 3.25
average(); // null
*/

const sum = (numbers) => {
    let result = 0;
    for (const num of numbers) {
      result += num;
    }
    return result;
  };

const average = (...numbers) => numbers.length ? sum(numbers) / numbers.length : null;

average(0); // 0
average(0, 10); // 5
average(-3, 4, 2, 10); // 3.25
average(); // null