import ParseError from '../OOP/parseJson/ParseError.js';
import { parseJson } from '../OOP/parseJson/parseJson';

test('test parse json', () => {
  const json = '{ "key": "value" }';
  expect(parseJson(json)).toEqual({ key: 'value' });
});

test('test parse invalid json', () => {
  const json = '{ key": "value" }';
  expect(() => parseJson(json)).toThrow(ParseError);
});