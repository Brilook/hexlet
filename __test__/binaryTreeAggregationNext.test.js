import Node from '../OOP/trials/binaryTreeAggregation/binaryTreeAggregationNext.js';

describe('Binary tree', () => {
  const tree = new Node(9,
    new Node(4,
      new Node(8),
      new Node(6,
        new Node(3),
        new Node(7))),
    new Node(17,
      null,
      new Node(22,
        null,
        new Node(20))));
    
  test('test getters', () => {
    expect(tree.getKey()).toBe(9);
    expect(tree.getLeftChild().getKey()).toBe(4);
    expect(tree.getRightChild().getKey()).toBe(17);
  });

  test('test _next', () => {
    expect(tree._next().getKey()).toBe(4);
    const node22 = tree.getRightChild().getRightChild();
    expect(node22._next().getKey()).toBe(20);
    const node8 = tree.getLeftChild().getLeftChild();
    expect(node8._next().getKey()).toBe(6);
    const node7 = tree.getLeftChild().getRightChild().getRightChild();
    expect(node7._next().getKey()).toBe(17);
    const node17 = node7._next();
    expect(node17._next()._next().getKey()).toBe(20);
    expect(node17._next()._next()._next()).toBe(null);
  });

  test('emty rightChild', () => {
    const tree = new Node(9,
      new Node(4,
        new Node(8),
        new Node(6,
          new Node(3),
          new Node(7))),
      new Node(17,
        new Node(22,
          null,
          new Node(20)),
          null));
    
    const node20 = tree.getRightChild().getLeftChild().getRightChild();
    expect(node20._next()).toBe(null);
  });

  test('equal keys', () => {
    const tree = new Node(9,
      new Node(4),
      new Node(4));

  const startNode = tree.getLeftChild();
  const nextNode = tree.getRightChild();
  
  expect(startNode._next().getKey()).toBe(4);
  expect(startNode._next()).toBe(nextNode);
  expect(startNode._next()).toEqual(nextNode);
  });

  test('test methods', () => {
    expect(tree.getCount()).toBe(9);
    expect(tree.getSum()).toBe(96);
    expect(tree.toArray()).toEqual([9, 4, 8, 6, 3, 7, 17, 22, 20]);
    expect(tree.toString()).toEqual('(9, 4, 8, 6, 3, 7, 17, 22, 20)');
  });

  test('test predicate methods', () => {
    expect(tree.some((key) => key < 4)).toBeTruthy();
    expect(tree.some((key) => key > 22)).toBeFalsy();
    expect(tree.every((key) => key <= 22)).toBeTruthy();
    expect(tree.every((key) => key < 22)).toBeFalsy();
    expect(tree.every(Number.isInteger)).toBeTruthy();
  });
});
