import each from '../OOP/each.js';

test('each', () => {
  const objects = [
    { name: 'Karl' },
    { name: 'Mia' },
  ];
  each(objects, function callback() {
    this.name = this.name.split('').reverse().join('');
  });

  const expected = [
    { name: 'lraK' },
    { name: 'aiM' },
  ];
  expect(objects).toEqual(expected);
});
