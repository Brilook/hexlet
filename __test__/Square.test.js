import Square from '../OOP/trials/Square/Square.js';

test('GetSide', () => {
  const square = new Square(4);
  const actual = square.getSide();
  expect(actual).toBe(4);
});
