import { getMutualFriends, makeUser } from '../OOP/getMutualFriends.js';

test('without friends', () => {
  const user1 = makeUser();
  const user2 = makeUser();
  expect(getMutualFriends(user1, user2)).toEqual([]);
});

test('with friends', () => {
  const expected = [makeUser({ id: 2 }), makeUser({ id: 8 })];
  const user1 = makeUser({
    friends: [makeUser({ id: 2 }), makeUser({ id: 8 }), makeUser({ id: 100 })],
  });
  const user2 = makeUser({
    friends: [makeUser({ id: 2 }), makeUser({ id: 7 }), makeUser({ id: 8 })],
  });
  const user3 = makeUser({
    friends: [makeUser({ id: 1 }), makeUser({ id: 7 }), makeUser({ id: 8 })],
  });
  const mutualFriends = getMutualFriends(user1, user2);
  
  expect(JSON.stringify(mutualFriends)).toEqual(JSON.stringify(expected));

  const mutualFriends1 = getMutualFriends(user1, user2, user3);
  const expected1 = [makeUser({ id: 8 })];

  expect(JSON.stringify(mutualFriends1)).toEqual(JSON.stringify(expected1));
});