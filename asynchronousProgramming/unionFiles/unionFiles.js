'use strict';
/*
https://ru.hexlet.io/courses/js-asynchronous-programming/lessons/event-loop/exercise_unit


Это задание напрямую не связано с теорией урока, но позволяет еще больше прокачаться в работе с асинхронным кодом.

В библиотеке async есть функция waterfall(),
которая позволяет строить цепочки асинхронных функций без необходимости вкладывать их друг в друга.
Подробнее о том как она работает, посмотрите в документации. Попробуйте решить данное упражнение с применением этой функции.

file.js
Реализуйте и экспортируйте асинхронную функцию unionFiles(), которую мы рассматривали в предыдущих уроках.
*/
import waterfall from 'async/waterfall';
import fs from 'fs';


export const unionFiles = (inputPath1, inputPath2, outputPath, callback) => {
  waterfall([
    (cbfn) => fs.readFile(inputPath1, cbfn),
    (data1, cbfn) => fs.readFile(inputPath2, (err, data2) => cbfn(err, data1, data2)),
    (data1, data2, cbfn) => fs.writeFile(outputPath, `${data1}${data2}`, cbfn)
   ], callback);
  };