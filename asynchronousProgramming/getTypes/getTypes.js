'use strict';
/*
https://ru.hexlet.io/courses/js-asynchronous-programming/lessons/chain-of-promises/exercise_unit

Реализуйте и экспортируйте асинхронную функцию getTypes(),
которая анализирует список переданных путей и возвращает массив (в промисе),
с описанием того, что находится по каждому из путей.

Эта функция должна отрабатывать успешно в любом случае.
Если во время выполнения асинхронной операции возникла ошибка,
то значением для этого пути будет null.
Для простоты считаем, что в эту функцию всегда передается как минимум один путь для обработки
(иначе придется задействовать механизм, который проходится в курсах далее).

Примеры
import { getTypes } from './file.js';

getTypes(['/etc', '/etc/hosts', '/undefined']).then(console.log);
// ['directory', 'file', null]
*/



import { promises as fs } from 'fs';

const getAffiliation = statPromise => statPromise
  .then((stat) => (stat.isDirectory() ? 'directory' : 'file'))
  .catch(() => null);

export const getTypes = (paths) => {
  const promises = paths.reduce((acc, path) => {
    const affiliation = getAffiliation(fs.stat(path));
    acc.push(affiliation);
    return acc;
  }, []);
  return Promise.all(promises);
};


// sequential solution

const getTypeName = (stat) => (stat.isDirectory() ? 'directory' : 'file');

export const getTypesSequential = (filesPath) => {
  // функция получает путь и аккумулятор из reduce, выполняет попытку получить stat,
  // добавляет в аккумулятор строку или null и возвращает обновлённый аккумулятор
  const processPath = (filepath, result) => fs.stat(filepath)
    .then((data) => [...result, getTypeName(data)])
    .catch(() => [...result, null]);

  const resultPromise = filesPath.reduce(
    // promise - это аккумулятор, обёрнутый в промис, поэтому на нём вызывается then
    // result - предыдущее значение аккумулятора
    (promise, filepath) => promise.then((result) => processPath(filepath, result)),
    Promise.resolve([]),
  );
  return resultPromise;
};
