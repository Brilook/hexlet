'use strict';
/*
https://ru.hexlet.io/courses/js-asynchronous-programming/lessons/asynchronous-flow/exercise_unit

Реализуйте и экспортируйте асинхронную функцию compareFileSizes(),
которая сравнивает размеры двух файлов.
Если первый больше второго, то она возвращает единицу, если размеры равны, то возвращает ноль, иначе — -1.

import { compareFileSizes } from './info.js';

compareFileSizes('filepath1', 'filepath2', (_err, result) => console.log(result));
Подсказка
Для реализации этого задания, нужно воспользоваться функцией fs.stat, которая использовалась в примерах теории
Math.sign
*/
import fs from 'fs';

export const compareFileSizes = (file1, file2, callback) => {
  fs.stat(file1, (err1, { size: size1 }) => {
    fs.stat(file2, (err2, { size: size2 }) => {
      callback(null, Math.sign(size1 - size2));
    });
  });
};
